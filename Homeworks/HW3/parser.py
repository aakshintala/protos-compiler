import ply.yacc as yacc
#import sys
import IR
from lexer import tokens
precedence = (('left','OR'),
		('left', 'AND'),
		('nonassoc', 'EQ','NEQ','GT','LT','GTEQ','LTEQ'),
		('left', 'ADD','SUB'),
		('left', 'MUL','DIV','MOD'),
		('right', 'NOT'),
		('right', 'UMINUS'))

block_map = {}
global_id =0

def set_temp():
        global global_id
        global_id = 0

def get_temp():
        global global_id
        global_id = global_id + 1
        return '_t'+str(global_id-1)

def getitem(iter):
	x = ''
	if(isinstance(iter, list)):
		for another in iter:
			x = x + getitem(another)
	else:
		x = x + str(iter)
	return x

bno = 0
def get_new_block():
	global bno
	bno = bno + 1
	return "_b"+str(bno)

cur_bno = ''
def get_cur_block():
	global cur_bno
	return str(cur_bno)

def set_cur_block(x):
	global cur_bno
	cur_bno = x
					
class NODE:
	def __init__(self,op,items):
		#print op, items
		self.items = items
		self.op = op

	def __str__(self):
		x = str(self.op) + " "
		for iter in self.items:
			x = x + getitem(iter)
		return x

	def defined(self):
		if(self.op == '='):
			if(self.items[0].op != 'id'):
				print "left hand side is not id.. error"
				exit(0)
			return [self.items[0].items[0]]

		if(self.op == 'id' or self.op == 'intconst'):
			return []

	def wellformed(self, defined):

		if(self.op == '='):
			if(not self.items[1].wellformed(defined)):
				return False
			else:
				if(self.items[0].items[0] not in defined):
					defined.append(self.items[0].items[0])
				return True

		if(self.op == 'ife'):
			if(not self.items[0].wellformed(defined)):
				return False

			local_defined1 = defined[:]
			local_defined2 = defined[:]
			if(not self.items[1].wellformed(local_defined1) or not self.items[2].wellformed(local_defined2)):
				return False
		
			for iter1 in local_defined1:
				if iter1 in local_defined2:
					if iter1 not in defined:
						defined.append(iter1)
			return True		
		
		if(self.op == 'if' or self.op == 'while'):
			local_defined = defined[:]
			if(not self.items[0].wellformed(defined) or not self.items[1].wellformed(local_defined)):
				return False
			else:
				return True

		if(self.op == 'intconst'):
			return True

		if(self.op == 'id'):
			if(self.items[0] not in defined):
				print "var: ",str(self.items[0]), " not defined"
				return False
			else:
				return True

		for element in self.items:
			if(not element.wellformed(defined)):
				return False
		return True
	
	def gencode(self):

		if(self.op == 'intconst'):
			temp = get_temp()
			return [IR.IR_STMT('loadi', temp, self.items[0], 1, None, None, 1)]

		if(self.op == 'id'):
			#temp = get_temp()
			#return [IR.IR_STMT('move', temp, self.items[0], 0, None, None, 4)]
			return []

		if(self.op == 'print'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			opcode = 'print'
			icode_list = self.items[0].gencode()
			if(len(icode_list) == 0):
				# an id.
				op1 = self.items[0].items[0]
			else:
				op1 = icode_list[len(icode_list)-1].dest
			op1type = 0
			optype = 2
			x = IR.IR_STMT(opcode, dest, op1, op1type, op2, op2type, optype)
			icode_list.append(x)
			return icode_list

		if(self.op == '='):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			dest = self.items[0].items[0]
			opcode = '='
			icode_list = self.items[1].gencode()
			if(len(icode_list) == 0):
				op1 = self.items[1].items[0]
			else:
				if(icode_list[len(icode_list)-1].dest == None):
					print "DESTINATION IS NONE.. SEE THIS CASE.. "
					exit(0)
				else:
					op1 = icode_list[len(icode_list)-1].dest
			op1type = 0
			optype = 4
			x = IR.IR_STMT(opcode, dest, op1, op1type, op2, op2type, optype)
			icode_list.append(x)
			return icode_list

		if(self.op == 'input'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			opcode = 'input'
			dest = get_temp()
			optype = 3
			return [IR.IR_STMT(opcode, dest, op1, op1type, op2, op2type, optype)]

		if(self.op == 'uminus' or self.op == 'not'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			opcode = self.op
			list_icode = self.items[0].gencode()
			if(len(list_icode) == 0):
				op1 = self.items[0].items[0]
			else:
				op1 = list_icode[len(list_icode)-1].dest
			op1type = 0
			dest = get_temp()
			optype = 0
			x = IR.IR_STMT(opcode, dest, op1, op1type, op2, op2type, optype)
			list_icode.append(x)
			return list_icode

		if(self.op == '+' or self.op == '-' or self.op == '*' or self.op == '/' or self.op == '%' or self.op == '==' or self.op == '!=' or self.op == '<' or self.op == '>' or self.op == '>=' or self.op == '<='):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			dest = get_temp()
			list_icode1 = self.items[1].gencode()
			if(len(list_icode1) == 0):
				op2 = self.items[1].items[0]
			else:
				op2 = list_icode1[len(list_icode1)-1].dest
			op2type = 0
			list_icode2 = self.items[0].gencode()
			if(len(list_icode2) == 0):
				op1 = self.items[0].items[0]
			else:
				op1 = list_icode2[len(list_icode2)-1].dest

			op1type = 0
			opcode = self.op
			optype = 0 #arithmetic

			list_icode1 = list_icode1 + list_icode2
			x = IR.IR_STMT(opcode, dest, op1, op1type, op2, op2type, optype)
			list_icode1.append(x)
			return list_icode1

		if(self.op == 'stmtseq'):
			list_icode = []
			for element in self.items:
				list_icode = list_icode + element.gencode()
			return list_icode		
			
		if(self.op == '&&'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			list_icode = self.items[0].gencode()
			if(len(list_icode) == 0):
				op1 = self.items[0].items[0]
			else:
				op1 = list_icode[len(list_icode)-1].dest
	
			op1type = 0

			dest = get_temp()
			list_icode.append(IR.IR_STMT('move', dest, op1, op1type, None, None, 4))
	
			new_bno = get_new_block()
			if(block_map.has_key(new_bno)):
				print "Couldn't find a new block name.. "
				exit(0)
			block_map[new_bno] = self.items[1].gencode()
			if(len(block_map[new_bno]) == 0):
				block_map[new_bno] = [IR.IR_STMT('move', dest, self.items[1][0], 0, None, None, 4)]
			else:
				block_map[new_bno].append(IR.IR_STMT('move', dest, block_map[new_bno][len(block_map[new_bno])-1].dest, 0, None, None, 4))
			opcode = 'if'
			optype = 8 

			x = IR.IR_STMT(opcode, new_bno, op1, op1type, op2, op2type, optype)
			list_icode.append(x)
			list_icode.append(IR.IR_STMT('move', dest, dest, 0, None, None, 4))
			return list_icode
				
		if(self.op == '||'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			list_icode = self.items[0].gencode()
			if(len(list_icode) == 0):
				op1 = self.items[0].items[0]
			else:
				op1 = list_icode[len(list_icode)-1].dest
	
			op1type = 0
	
			dest = get_temp()

			new_bno = get_new_block()
			if(block_map.has_key(new_bno)):
				print "Couldn't find a new block name.. "
				exit(0)
			block_map[new_bno] = self.items[1].gencode()
			if(len(block_map[new_bno]) == 0):
				block_map[new_bno] = [IR.IR_STMT('move', dest, self.items[1][0], 0, None, None, 4)]
			else:
				block_map[new_bno].append(IR.IR_STMT('move', dest, block_map[new_bno][len(block_map[new_bno])-1].dest, 0, None, None, 4))

			op2 = new_bno
			op2type = 3 
			opcode = 'ife'
			optype = 7

			new_bno1 = get_new_block()
			if(block_map.has_key(new_bno1)):
				print "Couldn't find a new block name.. "
				exit(0)
			block_map[new_bno1] = [IR.IR_STMT('move', dest, op1, 0, None, None, 4)]

			x = IR.IR_STMT(opcode, new_bno1, op1, op1type, op2, op2type, optype)
			list_icode.append(x)
			list_icode.append(IR.IR_STMT('move', dest, dest, 0, None, None, 4))
			return list_icode

					
		if(self.op == 'ife'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			list_icode = self.items[0].gencode()
			if(len(list_icode) == 0):
				op1 = self.items[0].items[0]
			else:
				op1 = list_icode[len(list_icode)-1].dest

			op1type = 0
			op2 = None
			opcode = self.op
			optype = 7

			#hack: put else label in op2
			new_bno = get_new_block()
			if(block_map.has_key(new_bno)):
				print "Couldn't find a new block name.. "
				exit(0)

			block_map[new_bno] = self.items[1].gencode()
			#print new_bno, "   ", block_map[new_bno]
			dest = new_bno

			new_bno1 = get_new_block()
			if(block_map.has_key(new_bno1)):
				print "Couldn't find a new block name.. "
				exit(0)

			block_map[new_bno1] = self.items[2].gencode()
			#print new_bno1, "   ", block_map[new_bno1]
			op2 = new_bno1
			op2type = 3
			 
			x = IR.IR_STMT(opcode, dest, op1, op1type, op2, op2type, optype)
			list_icode.append(x)
			return list_icode
								
		if(self.op == 'if'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			list_icode = self.items[0].gencode()
			if(len(list_icode) == 0):
				op1 = self.items[0].items[0]
			else:
				op1 = list_icode[len(list_icode)-1].dest
			op2 = None
			opcode = self.op
			optype = 8
			op1type = 0

			new_bno = get_new_block()
			if(block_map.has_key(new_bno)):
				print "Couldn't find a new block name.. "
				exit(0)

			block_map[new_bno] = self.items[1].gencode()
			#print new_bno, "   ", block_map[new_bno]
			dest = new_bno

			x = IR.IR_STMT(opcode, dest, op1, op1type, op2, op2type, optype)
			list_icode.append(x)
			return list_icode

		if(self.op == 'while'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			new_bno = get_new_block()
			if(block_map.has_key(new_bno)):
				print "Couldn't find a new block name.. "
				exit(0)

			block_map[new_bno] = self.items[0].gencode()
			if(len(block_map[new_bno]) == 0):
				op1 = self.items[0].items[0]
			else:
				op1 = block_map[new_bno][len(block_map[new_bno]) - 1].dest
			op1type = 0
			list_icode = []
			op2 = new_bno
			op2type = 3
			opcode = self.op
			optype = 9

			new_bno1 = get_new_block()
			if(block_map.has_key(new_bno1)):
				print "Couldn't find a new block name.. "
				exit(0)

			block_map[new_bno1] = self.items[1].gencode()
			if(len(block_map[new_bno1]) == 0):
				dest = self.items[1].items[0]
			else:
				dest = block_map[new_bno1][len(block_map[new_bno1]) - 1].dest
			print dest
			block_map[new_bno].append(IR.IR_STMT(opcode, new_bno1, op1, op1type, op2, op2type, optype))

			list_icode.append(IR.IR_STMT('jmp',new_bno,None,None,None,None,10))
			return list_icode
		

def p_pgm(p):
	'Pgm : StmtSeq'
	p[0] = p[1]

def p_sseq_stmt(p):
	'StmtSeq : Stmt'
	p[0] = NODE('stmtseq',[p[1]])

def p_sseq_stmt_sseq(p):
	'StmtSeq : Stmt StmtSeq'
	p[0] = NODE('stmtseq',[p[1],p[2]])

def p_stmt(p):
	'''Stmt : Assign
		| Print
		| Block
		| If
		| While '''
	p[0] = p[1]

def p_assign(p):
	'Assign : ID ASSIGN Rhs SCOLON'
	p[0] = NODE('=',[NODE('id',[p[1]]),p[3]])

def p_print(p):
	'Print : PRINT LOPEN AE ROPEN SCOLON'
	p[0] = NODE('print',[p[3]])

def p_block(p):
	'Block : LCURL StmtSeq RCURL'
	p[0] = p[2]

def p_if_no_else(p):
	'If : IF AE THEN Stmt'
	print "inside if no else"
	p[0] = NODE('if', [p[2],p[4]])

def p_if_else(p):
	'If : IF AE THEN Stmt ELSE Stmt'
	print "if else"
	p[0] = NODE('ife', [p[2],p[4],p[6]])

def p_while(p):
	'While : WHILE AE DO Stmt'
	p[0] = NODE('while', [p[2],p[4]])

def p_rhs_inp(p):
	'Rhs : INPUT LOPEN ROPEN'
	p[0] = NODE('input',[])

def p_rhs_ae(p):
	'Rhs : AE'
	p[0] = p[1]

def p_ae_bin(p):
	'''AE : AE ADD AE
	      | AE SUB AE
	      | AE MUL AE
	      | AE DIV AE
	      | AE MOD AE
	      | AE AND AE
	      | AE OR AE
	      | AE EQ AE
	      | AE NEQ AE
	      | AE LT AE
	      | AE GT AE
	      | AE GTEQ AE
	      | AE LTEQ AE'''
	print "inside binary"
	p[0] = NODE(p[2], [p[1],p[3]])
	
	
def p_ae_un(p):
	'''AE : SUB AE %prec UMINUS
	      | NOT AE'''
	print "inside unary"
	if(p[1] == '-'):
		p[0] = NODE('uminus', [p[2]])
	else:
		p[0] = NODE('not', [p[2]])
		

def p_ae_open(p):
	'AE : LOPEN AE ROPEN'
	print "inside open"
	p[0] = p[2]

def p_ae_int(p):
	'AE : INTCONST'
	p[0] = NODE('intconst',[p[1]])

def p_ae_id(p):
	'AE : ID'
	p[0] = NODE('id', [p[1]])

def p_error(p):
	if(p == None):
		print "Syntax error: Reached end of file"
	else:
		print "Syntax error at Line: ", p.lineno, "Before Token: ", p.value
	exit(0)

def print_sequence(blk_map,block_no):
	for items in blk_map[block_no]:
		print block_no, "  ", items
		if(items.dest and not str(items.dest).isdigit() and str(items.dest)[:2] == '_b'):
			print_sequence(blk_map,str(items.dest))
		if(items.src1 and not str(items.src1).isdigit() and str(items.src1)[:2] == '_b'):
			print_sequence(blk_map,str(items.src1))
		if(items.operatortype != 9 and items.src2 and not str(items.src2).isdigit() and str(items.src2)[:2] == '_b'):
			print_sequence(blk_map,str(items.src2))


def parse(s):
	parser = yacc.yacc()
	result = parser.parse(s)

	if(result.wellformed([])):
		new_block = get_new_block()
		if(block_map.has_key(new_block)):
			print "No new block label found.. "
			exit(0)
		block_map[new_block] = result.gencode()
		print_sequence(block_map, new_block)
		return block_map, new_block
	else:
		print "Not a well-formed program."
		exit(0)
