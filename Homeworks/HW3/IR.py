#class REGIMM():
#	value
#	vtype = Variable: 0 Immediate: 1 , Label: 3
		
#class IR_STMT():
#	opr = Type of Operator: * + - / % uminus input print loadi move
#	dest = Destination
#	src1 = REGIMM(src1,src1type) or None
#	src2 = REGIMM(src2,src2type) or None
#	operatortype = Arithmetic: 0, Loadi: 1, Print: 2, Input: 3 , Move: 4, Load: 5, Store: 6, Ife: 7, If: 8, While: 9, Jump: 10, Loop_End: 11
#	spill = False/True
		
class REGIMM():
	def __init__(self,value,vtype):
		self.value = value
		self.vtype = vtype
		# Variable: 0 Immediate: 1
	def __str__(self):
		return str(self.value)
	def __repr__(self):
		return str(self.value)
		
		
class IR_STMT():
	def __init__(self,opr,dest,src1,src1type,src2,src2type,opttype):
		self.opr = opr
		self.dest = dest
		if(src1 != None):
			self.src1 = REGIMM(src1,src1type)
		else:
			self.src1 = None
		if(src2 != None):
			self.src2 = REGIMM(src2,src2type)
		else:
			self.src2 = None
		self.operatortype = opttype
		self.spill = False
		
	def __str__(self):
		return "\"" + str(self.opr) + ',' + str(self.dest) + ',' + str(self.src1) + ',' + str(self.src2) + ',' + str(self.operatortype) + ',' + str(self.spill) + "\""
	
	def __repr__(self):
		return "\"" + str(self.opr) + ',' + str(self.dest) + ',' + str(self.src1) + ',' + str(self.src2) + ',' + str(self.operatortype) + ',' + str(self.spill) + "\""

class insandouts():
	def __init__(self):
		self.ins = []
		self.outs = []
		self.uses = []
		self.defs = []
		
inoutmaps = {}
		
def liveness(blk_map, blkid, lastouts):
	global inoutmaps
	inoutmaps[blkid] = insandouts()
	if(blk_map.has_key(blkid)):
		irlist = blk_map[blkid]
	else:
		print "irlist doesn't exist for", blkid
		exit()
	#print blkid, irlist
	#print
	if(not len(irlist)):
		return inoutmaps
	for iter in range(0,len(irlist)):
		inoutmaps[blkid].ins.append([])
		inoutmaps[blkid].outs.append([])
		temp = []
		if(irlist[iter].operatortype == 0):
			if(irlist[iter].src1.vtype == 0):
				temp.append(irlist[iter].src1.value)
			if(irlist[iter].opr != 'uminus' and irlist[iter].opr != 'not' and irlist[iter].src2.vtype == 0):
				temp.append(irlist[iter].src2.value)
		elif(irlist[iter].operatortype == 2 or irlist[iter].operatortype == 4 or irlist[iter].operatortype == 6 or irlist[iter].operatortype == 7 or irlist[iter].operatortype == 8 or irlist[iter].operatortype == 9):
			if(irlist[iter].src1.vtype == 0):
				temp.append(irlist[iter].src1.value)
		inoutmaps[blkid].uses.append(temp)
		if(irlist[iter].operatortype == 0 or irlist[iter].operatortype == 1 or irlist[iter].operatortype == 3 or irlist[iter].operatortype == 4 or irlist[iter].operatortype == 5):
			inoutmaps[blkid].defs.append([irlist[iter].dest])
		else:
			inoutmaps[blkid].defs.append([ ])
	done = False
	while(True):
		tempin = inoutmaps[blkid].ins
		tempout = inoutmaps[blkid].outs
		iter = len(irlist)-1
		if(irlist[iter].operatortype == 7): #"ife,_b17,_t30,_b18,7,False"
			#print "in ife loop", blkid
			inoutmaps = liveness(blk_map,str(irlist[iter].dest),lastouts)
			inoutmaps = liveness(blk_map,str(irlist[iter].src2),lastouts)
			inoutmaps[blkid].outs[iter] = list(set(inoutmaps[str(irlist[iter].dest)].ins[0]).union(inoutmaps[str(irlist[iter].src2)].ins[0]))
			inoutmaps[blkid].ins[iter] = list(set(inoutmaps[blkid].uses[iter]).union(set(inoutmaps[blkid].outs[iter]) - set(inoutmaps[blkid].defs[iter])))
			
		elif(irlist[iter].operatortype == 8): #"if,_b16,_t27,None,8,False"
			inoutmaps = liveness(blk_map,str(irlist[iter].dest),lastouts)
			inoutmaps[blkid].outs[iter] = list(set(inoutmaps[str(irlist[iter].dest)].ins[0]))
			inoutmaps[blkid].ins[iter] = list(set(inoutmaps[blkid].uses[iter]).union(set(inoutmaps[blkid].outs[iter]) - set(inoutmaps[blkid].defs[iter])))
			#print "in if loop"
			
		elif(irlist[iter].operatortype == 9): #"while,_b22,_b21,None,9,False"
			loopflag = True
			tempout1 = lastouts
			inoutmaps = liveness(blk_map,str(irlist[iter].dest),tempout1)
			inoutmaps[blkid].outs[iter] = list(set(inoutmaps[str(irlist[iter].dest)].ins[0]))
			inoutmaps[blkid].ins[iter] = list(set(inoutmaps[blkid].uses[iter]).union(set(inoutmaps[blkid].outs[iter]) - set(inoutmaps[blkid].defs[iter])))
			tempout1 = inoutmaps[blkid].ins[iter]
			while(loopflag):
				tempin2while = inoutmaps[blkid].ins[iter]
				tempout2while = inoutmaps[blkid].outs[iter]
				tempininner = inoutmaps[str(irlist[iter].dest)].ins
				tempoutinner = inoutmaps[str(irlist[iter].dest)].outs
				inoutmaps = liveness(blk_map,str(irlist[iter].dest),tempout1)
				inoutmaps[blkid].outs[iter] = list(set(inoutmaps[str(irlist[iter].dest)].ins[0]))
				inoutmaps[blkid].ins[iter] = list(set(inoutmaps[blkid].uses[iter]).union(set(inoutmaps[blkid].outs[iter]) - set(inoutmaps[blkid].defs[iter])))
				tempout1 = inoutmaps[blkid].ins[iter]

				if((tempin2while == inoutmaps[blkid].ins[iter])):
					if(tempout2while == inoutmaps[blkid].outs[iter]):
						if(tempininner == inoutmaps[str(irlist[iter].dest)].ins): 
							if(tempoutinner == inoutmaps[str(irlist[iter].dest)].outs):
								loopflag = False
			print "broke out of loop"
		
#		elif():
#		  pass
			
		elif(irlist[iter].operatortype == 10): #"jmp,_b21,None,None,10,False"
			inoutmaps = liveness(blk_map,str(irlist[iter].dest),lastouts)
			inoutmaps[blkid].outs[iter] = list(set(inoutmaps[str(irlist[iter].dest)].ins[0]))
			inoutmaps[blkid].ins[iter] = list(set(inoutmaps[blkid].uses[iter]).union(set(inoutmaps[blkid].outs[iter]) - set(inoutmaps[blkid].defs[iter])))
			
		else:
			inoutmaps[blkid].outs[iter] = lastouts
			inoutmaps[blkid].ins[iter] = list(set(inoutmaps[blkid].uses[iter]).union(set(inoutmaps[blkid].outs[iter]) - set(inoutmaps[blkid].defs[iter])))
		iter -= 1
		while(iter >= 0):
			if(irlist[iter].operatortype == 7): #ife
				inoutmaps = liveness(blk_map,str(irlist[iter].dest),list(set(inoutmaps[blkid].ins[iter+1])))
				inoutmaps = liveness(blk_map,str(irlist[iter].src2),list(set(inoutmaps[blkid].ins[iter+1])))
				inoutmaps[blkid].outs[iter] = list(set(inoutmaps[str(irlist[iter].dest)].ins[0]).union(inoutmaps[str(irlist[iter].src2)].ins[0]))
				inoutmaps[blkid].ins[iter] = list(set(inoutmaps[blkid].uses[iter]).union(set(inoutmaps[blkid].outs[iter]) - set(inoutmaps[blkid].defs[iter])))
				#print "in ife loop"
			elif(irlist[iter].operatortype == 8): #if
				inoutmaps = liveness(blk_map,str(irlist[iter].dest),list(set(inoutmaps[blkid].ins[iter+1])))
				inoutmaps[blkid].outs[iter] = list(set(inoutmaps[str(irlist[iter].dest)].ins[0]).union(inoutmaps[blkid].ins[iter+1]) - set(inoutmaps[str(irlist[iter].dest)].ins[0]).intersection(inoutmaps[blkid].ins[iter+1]))
				#inoutmaps[blkid].outs[iter] = list(set(inoutmaps[str(irlist[iter].dest)].ins[0]))
				inoutmaps[blkid].ins[iter] = list(set(inoutmaps[blkid].uses[iter]).union(set(inoutmaps[blkid].outs[iter]) - set(inoutmaps[blkid].defs[iter])))
				#print "in if loop"
			elif(irlist[iter].operatortype == 9): #while
				loopflag = True
				tempout1 = lastouts
				inoutmaps = liveness(blk_map,str(irlist[iter].dest),tempout1)
				inoutmaps[blkid].outs[iter] = list(set(inoutmaps[str(irlist[iter].dest)].ins[0]).union(inoutmaps[blkid].ins[iter+1]) - set(inoutmaps[str(irlist[iter].dest)].ins[0]).intersection(inoutmaps[blkid].ins[iter+1]))
				#inoutmaps[blkid].outs[iter] = list(set(inoutmaps[str(irlist[iter].dest)].ins[0]))
				inoutmaps[blkid].ins[iter] = list(set(inoutmaps[blkid].uses[iter]).union(set(inoutmaps[blkid].outs[iter]) - set(inoutmaps[blkid].defs[iter])))
				tempout1 = inoutmaps[blkid].ins[iter]
				while(loopflag):
					tempin2while = inoutmaps[blkid].ins[iter]
					tempout2while = inoutmaps[blkid].outs[iter]
					tempininner = inoutmaps[str(irlist[iter].dest)].ins
					tempoutinner = inoutmaps[str(irlist[iter].dest)].outs
					inoutmaps = liveness(blk_map,str(irlist[iter].dest),tempout1)
					inoutmaps[blkid].outs[iter] = list(set(inoutmaps[str(irlist[iter].dest)].ins[0]).union(inoutmaps[blkid].ins[iter+1]) - set(inoutmaps[str(irlist[iter].dest)].ins[0]).intersection(inoutmaps[blkid].ins[iter+1]))
					#inoutmaps[blkid].outs[iter] = list(set(inoutmaps[str(irlist[iter].dest)].ins[0]))
					inoutmaps[blkid].ins[iter] = list(set(inoutmaps[blkid].uses[iter]).union(set(inoutmaps[blkid].outs[iter]) - set(inoutmaps[blkid].defs[iter])))
					tempout1 = inoutmaps[blkid].ins[iter]

					if((tempin2while == inoutmaps[blkid].ins[iter])):
						if(tempout2while == inoutmaps[blkid].outs[iter]):
							if(tempininner == inoutmaps[str(irlist[iter].dest)].ins): 
								if(tempoutinner == inoutmaps[str(irlist[iter].dest)].outs):
									loopflag = False				

			elif(irlist[iter].operatortype == 10): #"jmp,_b21,None,None,10,False"
				inoutmaps = liveness(blk_map,str(irlist[iter].dest),lastouts)
				inoutmaps[blkid].outs[iter] = list(set(inoutmaps[str(irlist[iter].dest)].ins[0]))
				inoutmaps[blkid].ins[iter] = list(set(inoutmaps[blkid].uses[iter]).union(set(inoutmaps[blkid].outs[iter]) - set(inoutmaps[blkid].defs[iter])))
			else:
				inoutmaps[blkid].outs[iter] = list(set(inoutmaps[blkid].ins[iter+1]))
				inoutmaps[blkid].ins[iter] = list(set(inoutmaps[blkid].uses[iter]).union(set(inoutmaps[blkid].outs[iter]) - set(inoutmaps[blkid].defs[iter])))
			iter -= 1
		#print "\n in liveness", blkid, inoutmaps[blkid].ins, inoutmaps[blkid].outs
		done = True
		for iter in range(0,len(inoutmaps[blkid].ins)):
			if (inoutmaps[blkid].ins[iter] != tempin[iter] or inoutmaps[blkid].outs[iter] != tempout[iter]):
				print blkid, iter, inoutmaps[blkid].ins[iter], "marker1", tempin[iter]
				print inoutmaps[blkid].outs[iter],"marker2", tempout[iter], "didn't converge going again"
				print
				done = False
		if(done == True):
			break
		
	return inoutmaps


