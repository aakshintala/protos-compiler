#!/usr/bin/python

import IR, parser as Parser, reg_alloc as RegisterAlloc, sys, MIPSLowerISel as Lower 

ipfile = open(sys.argv[1],'r')
program = ""
for line in ipfile:
	program += str(line)
blk_map, first_blkid = Parser.parse(program)
#Parser.print_sequence(blk_map ,first_blkid)
insoutsmaps = IR.liveness(blk_map, first_blkid, [])
print
for iter in insoutsmaps:
	print iter, insoutsmaps[iter].ins , insoutsmaps[iter].outs
	print
cnode, blk_map = RegisterAlloc.kcolor(insoutsmaps,21,blk_map,first_blkid)
print cnode
#Parser.print_sequence(blk_map ,first_blkid)
machinecode = Lower.MipsLowerISel(blk_map, first_blkid, cnode)

opfile = open(sys.argv[1].rstrip('proto')+"asm",'w')
for line in machinecode:
	opfile.write(line)
opfile.write("\tli $v0, 10\n\tsyscall\n")

