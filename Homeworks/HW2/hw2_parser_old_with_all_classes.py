import cStringIO, tokenize

#defined = {}
#undefined = []

def generate_tokens(line):
        line = line.strip()
        src = cStringIO.StringIO(line).readline
        return list(tokenize.generate_tokens(src))

class PRODOP:
	def __init__(self, left, right, token):
		self.left = left
		self.right = right
		if(token[1] == '*'):
			self.prodop_type = 0
		if(token[1] == '/'):
			self.prodop_type = 1
		if(token[1] == '%'):
			self.prodop_type = 2
	def prod_str(self, prod_type):
		if(prod_type == 0):
			return '*'
		if(prod_type == 1):
			return '/'
		if(prod_type == 2):
			return '%'
	def __str__(self):
		return str(self.left)+self.prod_str(self.prodop_type)+str(self.right)

class SUMOP:

	def __init__(self, left, right, token):
		if(token[1] == '+'):
			self.sumop_type = 0
		else:
			self.sumop_type = 1 

		self.left = left
		self.right = right

	def sum_str(self, sum_type):
		if(sum_type == 0):
			return '+'
		if(sum_type == 1):
			return '-'
	def __str__(self):
		return str(self.left)+self.sum_str(self.sumop_type)+str(self.right)

def search_prodop(tokens):
	brace_start = 0
	for iter in range(len(tokens)):
		if(tokens[iter][1] == '('):
			brace_start = brace_start + 1
		if(tokens[iter][1] == ')'):
			if(brace_start == 0):
				print "not proper braces"
			brace_start = brace_start - 1 
		if(brace_start == 0 and (tokens[iter][1] == '*' or tokens[iter][1] == '/' or tokens[iter][1] == '%')):
			return iter
	return -1

class F:
	def __init__(self, tokens):
		if(tokens[0][1] == '-'):
			self.f_type = 0
			self.item = F(tokens[1:])
		else:
			if(tokens[0][1] == '('):
				if(tokens[len(tokens)-1][1] != ')'):
					print "wrong F"
				else:
					self.f_type = 1
					self.item = AE(tokens[1:len(tokens)-1])
			else:
				if(tokens[0][0] == tokenize.NUMBER):
					self.f_type = 2
					self.item = tokens[0][1]
				else:
					if(tokens[0][0] == tokenize.NAME):
						self.f_type = 3
						#if(defined.has_key(tokens[0][1])):
						#	if(defined[tokens[0][1]] == 0):
						#		self.item = tokens[0][1]
						#	else:
						#		self.item = tokens[0][1]+str(defined[tokens[0][1]])
						#else:
						#	print tokens[0][1]," variable is not defined"
						self.item = tokens[0][1]
							
					else:
						print "wrong F"
	def __str__(self):
		if(self.f_type == 0):
			return '-'+str(self.item)
		if(self.f_type == 1):
			return '('+str(self.item)+')'
		return str(self.item)

class T:
	def __init__(self, tokens):
		prodop_index = search_prodop(tokens)
 		if(prodop_index != -1):
			self.t_type = 0
			self.item = PRODOP(F(tokens[:prodop_index]), T(tokens[prodop_index+1:]), tokens[prodop_index])
		else:
			self.t_type = 1
			self.item = F(tokens)
	def __str__(self):
		return str(self.item)

class AE:
	def __init__(self, tokens):
		sumop_index = search_sumop(tokens)
		if(sumop_index != -1):
			self.ae_type = 0
			self.item = SUMOP(T(tokens[:sumop_index]), AE(tokens[sumop_index+1:]), tokens[sumop_index])
		else:
			self.ae_type = 1
			self.item = T(tokens)
	def __str__(self):
		return str(self.item)
	
def search_sumop(tokens):
	brace_start = 0
	for iter in range(len(tokens)):
		if(tokens[iter][1] == '('):
			brace_start = brace_start + 1 
		if(tokens[iter][1] == ')'):
			if(brace_start == 0):
				print "not proper braces"
			brace_start = brace_start - 1 
		if(tokens[iter][1] == '+'):
			if(brace_start == 0):
				return iter
		if(tokens[iter][1] == '-'):
			if(iter != 0 and brace_start == 0 and tokens[iter-1][1] != '*' and tokens[iter-1][1] != '/' and tokens[iter-1][1] != '%'):
				return iter
	return -1
class PRINT:
	def __init__(self, tokens):
		self.item = AE(tokens[2:len(tokens)-2])
	def __str__(self):
		return "print("+str(self.item)+")"
class INPUT:
	def __init__(self, tokens):
		self.item = "input()"
	def __str__(self):
		return self.item
class RHS:
	def __init__(self, tokens):
		if(self.check_input(tokens)):
			self.item = INPUT(tokens)
			self.rhs_type = 0
		else:
			self.rhs_type = 1
			self.item = AE(tokens)
	
	def check_input(self, tokens):
		if(tokens[0][1] == 'input'):
			return True
		else:
			return False
	def __str__(self):
		return str(self.item)

class ASSIGN:
	def __init__(self, tokens):
		self.left = tokens[0][1]
		self.right = RHS(tokens[2:len(tokens)-1])	
		#if(defined.has_key(tokens[0][1])):
		#	defined[tokens[0][1]] += 1
		#	self.left = tokens[0][1]+str(defined[tokens[0][1]])
		#else:
		#	defined[tokens[0][1]] = 0

	def __str__(self):
		return str(self.left)+"="+str(self.right)

class Stmt:
	def __init__(self, tokens):
		self.syntax = False
		tokens = tokens[:-1]
		if(self.valid(tokens)):
			if(self.check_print(tokens)):
				self.stmt_type = 0
				self.item = PRINT(tokens)
			else:
				if(self.check_assign(tokens)):
					self.stmt_type = 1
					self.item = ASSIGN(tokens)
				else:
					print "statement not valid"
			self.syntax = True
		else:
			print "statement not valid"

	def valid(self, tokens):
		if(tokens[len(tokens)-1][1] != ';'):
			print "not valid statement"
                        return False
		else:
			return True
	def check_print(self, tokens):
		if(tokens[0][1] == "print" and tokens[1][1] == '(' and tokens[len(tokens)-2][1] == ')'):
			return True
		else:
			return False
	def check_assign(self, tokens):
		if(tokens[0][1] != "print" and tokens[1][1] == '=' and tokens[len(tokens)-1][1] == ';'):
			return True
		else:
			return False
	def __str__(self):
		return str(self.item)

class PGM:
	def __init__(self, pgm):
		self.pgm = pgm.strip()
		self.nstmts = 0
		self.stmts = []
		self.defined = []
		self.undefined = []
	def parse(self):
		if(self.pgm[-1] != ';'):
			print "not a valid program"
		start = 0
		while(self.pgm[start:].find(';') != -1):
			stmt = self.pgm[start:start+self.pgm[start:].find(';')+1]
			tokens = generate_tokens(stmt)
			self.stmts.append(Stmt(tokens))
			self.nstmts = self.nstmts+1
			start = start+self.pgm[start:].find(';') + 1
	def __str__(self):
		start = 0
		res = "" 
		while(start < self.nstmts):
			res = res + str(self.stmts[start])
			start = start + 1
		return res
	def wellformed(self):
		start = 0
		while(start < self.nstmts):
			if(self.stmts[start].stmt_type == 1):
				stmt = str(self.stmts[start])
				tokens = generate_tokens(stmt)
				iter = 2
				while(iter < len(tokens)-1):
					if(tokens[iter][0] == tokenize.NAME):
						if(tokens[iter][1] not in self.defined):
							return False
					iter = iter + 1
				if(tokens[0][1] not in self.defined):
					self.defined.append(tokens[0][1])
			start = start+1
		return True


x = PGM("x=5*(-2+3);x=3;")
x.parse()
print x
print x.wellformed()
print "next"
x = PGM("x=input();print(1);x=y;")
x.parse()
print "parsed"
print x
print x.wellformed()
tokens = generate_tokens("x=1*(-2+3);")
print Stmt(tokens)
tokens = generate_tokens("x=yy*(-2+3);")
print Stmt(tokens)
tokens = generate_tokens("x=1+2;")
print Stmt(tokens)
tokens = generate_tokens("x=1+-2;")
print Stmt(tokens)
tokens = generate_tokens("x=(a+(b-c))+d;")
print Stmt(tokens)
tokens = generate_tokens("x=(-(b-c))+d;")
print Stmt(tokens)
