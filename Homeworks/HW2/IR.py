#class REGIMM():
#	value
#	vtype = Variable: 0 Immediate: 1		
		
#class IR_STMT():
#	opr = Type of Operator: * + - / % uminus input print loadi move
#	dest = Destination
#	src1 = REGIMM(src1,src1type) or None
#	src2 = REGIMM(src2,src2type) or None
#	operatortype = Arithmetic: 0, Loadi: 1, Print: 2, Input: 3 , Move: 4
#	spill = False/True
		
class REGIMM():
	def __init__(self,value,vtype):
		self.value = value
		self.vtype = vtype
		# Variable: 0 Immediate: 1
	def __str__(self):
		return str(self.value)
		
		
class IR_STMT():
	def __init__(self,opr,dest,src1,src1type,src2,src2type,opttype):
		self.opr = opr
		self.dest = dest
		if(src1 != None):
			self.src1 = REGIMM(src1,src1type)
		else:
			self.src1 = None
		if(src2 != None):
			self.src2 = REGIMM(src2,src2type)
		else:
			self.src2 = None
		self.operatortype = opttype
		self.spill = False
		# Arithmetic: 0, Loadi: 1, Print: 2, Input: 3
		
	def __str__(self):
		return "\"" + str(self.opr) + ',' + str(self.dest) + ',' + str(self.src1) + ',' + str(self.src2) + ',' + str(self.operatortype) + ',' + str(self.spill) + "\""
	
	def __repr__(self):
		return "\"" + str(self.opr) + ',' + str(self.dest) + ',' + str(self.src1) + ',' + str(self.src2) + ',' + str(self.operatortype) + ',' + str(self.spill) + "\""

def liveness(irlist): 
	ins = []
	outs = []
	uses = []
	defs = []
	dead = []
	if(not len(irlist)):
		return ins, outs, dead
	for iter in range(0,len(irlist)):
		ins.append([])
		outs.append([])
		temp = []
		if(irlist[iter].operatortype == 0):
			if(irlist[iter].src1.vtype == 0):
				temp.append(irlist[iter].src1.value)
			if(irlist[iter].opr != 'uminus' and irlist[iter].src2.vtype == 0):
				temp.append(irlist[iter].src2.value)
		elif(irlist[iter].operatortype == 2 or irlist[iter].operatortype == 4 or irlist[iter].operatortype == 6):
			if(irlist[iter].src1.vtype == 0):
				temp.append(irlist[iter].src1.value)
		uses.append(temp)
		if(irlist[iter].operatortype == 0 or irlist[iter].operatortype == 1 or irlist[iter].operatortype == 3 or irlist[iter].operatortype == 4 or irlist[iter].operatortype == 5):
			defs.append([irlist[iter].dest])
		else:
			defs.append([ ])

	done = False
	while(True):
		tempin = ins
		tempout = outs
		iter = len(irlist)-1
		outs[iter] = []
		ins[iter] = list(set(uses[iter]).union(set(outs[iter]) - set(defs[iter])))
		for item in defs[iter]:
			dead.append(item)
		iter -= 1
		while(iter >= 0):
			outs[iter] = list(set(ins[iter+1]))
			ins[iter] = list(set(uses[iter]).union(set(outs[iter]) - set(defs[iter])))
			deadnodes = list(set(defs[iter]) - set(outs[iter]))
			for item in deadnodes:
				dead.append(item)
			iter -= 1
		done = True
		for iter in range(0,len(ins)):
			if (ins[iter] != tempin[iter] or outs[iter] != tempout[iter]):
				done = False
		if(done == True):
			break

	return ins, outs , dead

def dcralive(irlist):
	ins, outs, dead = liveness(irlist)
	while(True):
		toremove = []
		for iter in range(0,len(irlist)):
			for iter2 in range(0,len(dead)):
				if irlist[iter].dest == dead[iter2]:
					toremove.append(irlist[iter])
		for iter in toremove:
			irlist.remove(iter)
		ins, outs, dead = liveness(irlist)
		if len(dead) == 0:
			break
	return ins, outs




