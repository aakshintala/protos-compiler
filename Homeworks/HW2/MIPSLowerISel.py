#class REGIMM():
#	value
#	vtype = Variable: 0 Immediate: 1 Memory: 10	
		
#class IR_STMT():
#	opr = Type of Operator: * + - / % uminus input print loadi move
#	dest = Destination
#	src1 = REGIMM(src1,src1type) or None
#	src2 = REGIMM(src2,src2type) or None
#	operatortype = Arithmetic: 0, Loadi: 1, Print: 2, Input: 3 , Move: 4
#	spill = False/True

Reg_Dict = {0:""}

def FetchReg(var,cnode):
	if(not cnode.has_key(var)):
		return 
	color = cnode[var]
	if color <= 9:
		return "$t" + str(color)
	elif color > 9 and color < 18:
		return "$s" + str(color-10)
	elif color > 17 and color < 21:
		return "$a" + str(color - 17)
	else:
		print "Problem in register Allocation. Exiting"
		exit()


def LowerArithOp(IRInst, cnode):
	regdest = FetchReg(IRInst.dest,cnode)
	if IRInst.src1.vtype != 0:
		print "Variable SRC1 value not denoted right. Error in IR. Exiting.", IRInst
		exit()
	
	if IRInst.opr != 'uminus' and IRInst.src2.vtype !=0:
		print "Variable SRC2 value not denoted right. Error in IR. Exiting.", IRInst
		exit()
	
	regsrc1 = FetchReg(IRInst.src1.value,cnode)
	
	if IRInst.opr == '*':
		regsrc2 = FetchReg(IRInst.src2.value,cnode)
		MCInst = "\tmul " + regdest + "," + regsrc1 + "," + regsrc2 + "\n"
	elif IRInst.opr == '+':
		regsrc2 = FetchReg(IRInst.src2.value,cnode)
		MCInst = "\tadd " + regdest + "," + regsrc1 + "," + regsrc2 + "\n"
	elif IRInst.opr == '-':
		regsrc2 = FetchReg(IRInst.src2.value,cnode)
		MCInst = "\tsub " + regdest + "," + regsrc1 + "," + regsrc2 + "\n"
	elif IRInst.opr == '/':
		regsrc2 = FetchReg(IRInst.src2.value,cnode)
		MCInst = "\tdiv " + regdest + "," + regsrc1 + "," + regsrc2 + "\n"
	elif IRInst.opr == '%':
		regsrc2 = FetchReg(IRInst.src2.value,cnode)
		MCInst = "\trem " + regdest + "," + regsrc1 + "," + regsrc2 + "\n"
	elif IRInst.opr == 'uminus':
		MCInst = "\tneg " + regdest + "," + regsrc1 + "\n"
	else:
		print "Operator Type Mismatch. Not an Arithmetic Operator. Exiting.", IRInst
		exit()
	return MCInst

def LowerMove(IRInst, cnode):
	regdest = FetchReg(IRInst.dest,cnode)
	if IRInst.src1.vtype != 0:
		print "Move requires a variable value in src. Error in IR. Exiting.", IRInst
		exit()
	regsrc1 = FetchReg(IRInst.src1.value,cnode)
	MCInst = "\tmove "+ regdest + "," + regsrc1 + "\n"	
	return MCInst

def LowerLoadi(IRInst, cnode):
	reg = FetchReg(IRInst.dest,cnode)
	if IRInst.src1.vtype != 1:
		print "Immediate value not denoted right. Error in IR. Exiting.", IRInst
		exit()
	MCInst = "\tli "+ reg + "," + IRInst.src1.value + "\n"	
	return MCInst
	
def LowerInput(IRInst, cnode):
	MCInst = "\n\tli $v0,5\n"
	MCInst += "\tsyscall\n"
	reg = FetchReg(IRInst.dest,cnode)
	MCInst += "\tmove "+ reg + ",$v0" + "\n"	
	return MCInst

def LowerPrint(IRInst, cnode):
	MCInst = "\n\tli $v0,1\n"
	reg = FetchReg(IRInst.src1.value,cnode)
	MCInst += "\tmove " + "$a0," + reg + "\n"	
	MCInst += "\tsyscall\n"
	MCInst += "\tli $v0,4\n\tla $a0,newline\n\tsyscall\n"
	return MCInst
	
def LowerLoad(IRInst, cnode): #mem_load,c8,M[c],None,5,False
	reg = FetchReg(IRInst.dest,cnode)
	if IRInst.src1.vtype != 10:
		print "Memory value not denoted right. Error in IR. Exiting.", IRInst
		exit()
	MCInst = "\n\tlw "+ reg + "," + IRInst.src1.value + "\n"	
	return MCInst

def LowerStore(IRInst, cnode): #store,M[y],y10,0,None,6,False
	if IRInst.src1.vtype != 0:
		print "Register value not denoted right. Error in IR. Exiting." , IRInst
		exit()
	reg = FetchReg(IRInst.src1.value,cnode)
	MCInst = "\n\tsw "+ reg + "," + IRInst.dest + "\n"	
	return MCInst

def MipsLowerISel(irlist, cnode):
	insts = []
	memnodes = []
	insts.append("\t.data\nnewline:\t.asciiz\t\"\\n\"\n")
	for iter in irlist:
		if iter.operatortype == 6:
			if(not iter.dest in memnodes):
				insts.append(str(iter.dest)+":\t.word\t"+"0\n")
				memnodes.append(iter.dest)
	print memnodes
	toremove = []
	for iter in range(0,len(irlist)-1): #"-,t6,x10,0,t7,0,False", "=,y1,t6,0,None,4,False"
		if irlist[iter].operatortype == 4:
			if irlist[iter-1].operatortype == 0:
				irlist[iter-1].dest = irlist[iter].dest
				toremove.append(irlist[iter])
	
	for iter in toremove:
		irlist.remove(iter)
	
	insts.append("\n\t.text\nmain:\n")
	print irlist
	for iter in irlist:
		if iter.operatortype == 0:
			insts.append(LowerArithOp(iter,cnode))
		elif iter.operatortype == 1:
			insts.append(LowerLoadi(iter,cnode))
		elif iter.operatortype == 2:
			insts.append(LowerPrint(iter,cnode))
		elif iter.operatortype == 3:
			insts.append(LowerInput(iter,cnode))
		elif iter.operatortype == 4:
			insts.append(LowerMove(iter,cnode))
		elif iter.operatortype == 5:
			insts.append(LowerLoad(iter,cnode))
		elif iter.operatortype == 6:
			insts.append(LowerStore(iter,cnode))
		else:
			print "Invalid Operation Type. Exiting"
			exit()
	insts.append("\tli $v0, 10\n\tsyscall")	
	return insts
