#!/usr/bin/python

import IR, hw2_parser as Parser, MIPSLowerISel as Lower, reg_alloc as RegisterAlloc, sys

ipfile = open(sys.argv[1],'r')
program = ""
for line in ipfile:
	program += str(line)
#print program
x = Parser.PGM(program)
x.parse()
q = x.gencode()
print q
ins, outs = IR.dcralive(q)
print "ins: ", ins
print "outs: ", outs
cnode, q = RegisterAlloc.kcolor(ins,3,q)
print cnode
#print q
machinecode = Lower.MipsLowerISel(q,cnode)

opfile = open(sys.argv[1].rstrip('proto')+"asm",'w')
for line in machinecode:
	opfile.write(line)

