import cStringIO, tokenize
import IR

global_id =0 
def set_temp():
	global global_id
	global_id = 0
def get_temp():
	global global_id
	global_id = global_id + 1
	return '_t'+str(global_id-1)
	
def generate_tokens(line):
        line = line.strip()
        src = cStringIO.StringIO(line).readline
        return list(tokenize.generate_tokens(src))

class PRODOP:
	def __init__(self, left, right, token):
		self.left = left
		self.right = right
		if(token[1] == '*'):
			self.prodop_type = 0
		if(token[1] == '/'):
			self.prodop_type = 1
		if(token[1] == '%'):
			self.prodop_type = 2

	def prod_str(self, prod_type):
		if(prod_type == 0):
			return '*'
		if(prod_type == 1):
			return '/'
		if(prod_type == 2):
			return '%'

	def __str__(self):
		return str(self.left)+self.prod_str(self.prodop_type)+str(self.right)

	def gencode(self):
		opcode = dest = op1 = op1type = op2 = op2type = optype = None
		
		dest = get_temp()
		list_icode1 = self.right.gencode()
		if(len(list_icode1) == 0):
			op2 = self.right.item
		else:
			op2 = list_icode1[len(list_icode1)-1].dest
		op2type = 0

		list_icode2 = self.left.gencode()
		if(len(list_icode2) == 0):
			op1 = self.left.item
		else:
			op1 = list_icode2[len(list_icode2)-1].dest
		op1type = 0

		if(self.prodop_type == 0):
			opcode = '*'
		else:
			if(self.prodop_type == 1):
				opcode = '/'
			else:
				opcode = '%'
		
		optype = 0 #arithmetic

		list_icode1 = list_icode1 + list_icode2
		x = IR.IR_STMT(opcode, dest, op1, op1type, op2, op2type, optype)
		list_icode1.append(x)
		return list_icode1

	def get_used(self):
		return self.left.get_used()+self.right.get_used()

class SUMOP:

	def __init__(self, left, right, token):
		if(token[1] == '+'):
			self.sumop_type = 0
		else:
			self.sumop_type = 1 

		self.left = left
		self.right = right

	def sum_str(self, sum_type):
		if(sum_type == 0):
			return '+'
		if(sum_type == 1):
			return '-'

	def __str__(self):
		return str(self.left)+self.sum_str(self.sumop_type)+str(self.right)

	def gencode(self):
		opcode = dest = op1 = op1type = op2 = op2type = optype = None
		dest = get_temp()
		list_icode1 = self.right.gencode()
		if(len(list_icode1) == 0):
			op2 = self.right.item
		else:
			op2 = list_icode1[len(list_icode1)-1].dest
		op2type = 0

		list_icode2 = self.left.gencode()
		if(len(list_icode2) == 0):
			op1 = self.left.item
		else:
			op1 = list_icode2[len(list_icode2)-1].dest
		op1type = 0

		if(self.sumop_type == 0):
			opcode = '+'
		else:
			opcode = '-'
		optype = 0 #arithmetic

		list_icode1 = list_icode1 + list_icode2
		x = IR.IR_STMT(opcode, dest, op1, op1type, op2, op2type, optype)
		list_icode1.append(x)
		return list_icode1

	def get_used(self):
		return self.left.get_used()+self.right.get_used()

def search_prodop(tokens):
	brace_start = 0
	for iter in range(len(tokens)):
		if(tokens[iter][1] == '('):
			brace_start = brace_start + 1
		if(tokens[iter][1] == ')'):
			if(brace_start == 0):
				print "Line: ", tokens[iter][4], "=> No proper braces"  
				exit(0)	
			brace_start = brace_start - 1 
		if(brace_start == 0 and (tokens[iter][1] == '*' or tokens[iter][1] == '/' or tokens[iter][1] == '%')):
			return iter
	return -1

class UMINUS:
	def __init__(self, tokens):
		self.item = get_f(tokens)
	def __str__(self):
		return '-'+str(self.item)
	def gencode(self):
		opcode = dest = op1 = op1type = op2 = op2type = optype = None
		opcode = 'uminus'
		
		list_icode = self.item.gencode()
		if(len(list_icode) == 0):
			op1 = self.item.item
		else:
			op1 = list_icode[len(list_icode)-1].dest

		op1type = 0
		dest = get_temp()
		optype = 0
		x = IR.IR_STMT(opcode, dest, op1, op1type, op2, op2type, optype)
		list_icode.append(x)
		return list_icode

	def get_used(self):
		return self.item.get_used()

class INTCONST:
	def __init__(self, literal):
		self.item = literal
	def __str__(self):
		return self.item
	def gencode(self):
		temp = get_temp()
		return [IR.IR_STMT('loadi', temp, self.item, 1, None, None, 1)]

	def get_used(self):
		return []

class VAR:
	def __init__(self, literal):
		self.item = literal
	def __str__(self):
		return self.item
	def gencode(self):
		return []
	def get_used(self):
		return [self.item]

def get_f(tokens):
	if(tokens[0][1] == '-'):
		return UMINUS(tokens[1:])
	else:
		if(tokens[0][1] == '('):
			if(tokens[len(tokens)-1][1] != ')'):
				print "Line: ", tokens[0][4], "=> No closing brace"  
				exit(0)
			else:
				return get_ae(tokens[1:len(tokens)-1])
		else:
			if(tokens[0][0] == tokenize.NUMBER):
				if(len(tokens) > 1):
					print "Line: ", tokens[0][4], "=> No ; after ", tokens[0][1]  
					exit(0)
				return INTCONST(tokens[0][1])
			else:
				if(tokens[0][0] == tokenize.NAME):
					if(len(tokens) > 1):
						print "Line: ", tokens[0][4], "=> No ; after ", tokens[0][1]  
						exit(0)
					return VAR(tokens[0][1])
				else:
					print "Line: ", tokens[0][4], "=> Invalid symbol ", tokens[0][1]  
					exit(0)

def get_t(tokens):
	prodop_index = search_prodop(tokens)
	if(prodop_index != -1):
		return PRODOP(get_f(tokens[:prodop_index]), get_t(tokens[prodop_index+1:]), tokens[prodop_index])
	else:
		return get_f(tokens)

def get_ae(tokens):
	sumop_index = search_sumop(tokens)
	if(sumop_index != -1):
		return SUMOP(get_t(tokens[:sumop_index]), get_ae(tokens[sumop_index+1:]), tokens[sumop_index])
	else:
		return get_t(tokens)
	
def search_sumop(tokens):
	brace_start = 0
	for iter in range(len(tokens)):
		if(tokens[iter][1] == '('):
			brace_start = brace_start + 1 
		if(tokens[iter][1] == ')'):
			if(brace_start == 0):
				print "Line: ", tokens[iter][4], "=> No open brace for ", tokens[0][1]  
				exit(0)
			brace_start = brace_start - 1 
		if(tokens[iter][1] == '+'):
			if(brace_start == 0):
				return iter
		if(tokens[iter][1] == '-'):
			if(iter != 0 and brace_start == 0 and tokens[iter-1][1] != '*' and tokens[iter-1][1] != '/' and tokens[iter-1][1] != '%' and tokens[iter-1][1] != '+' and tokens[iter-1][1] != '-'):
				return iter
	return -1

class PRINT:
	def __init__(self, tokens):
		self.item = get_ae(tokens[2:len(tokens)-2])

	def __str__(self):
		return "print("+str(self.item)+")"

	def gencode(self):
		opcode = dest = op1 = op1type = op2 = op2type = optype = None
		opcode = 'print'
		icode_list = self.item.gencode()
		if(len(icode_list) == 0):
			op1 = self.item.item
		else:
			op1 = icode_list[len(icode_list)-1].dest
		op1type = 0

		optype = 2
		x = IR.IR_STMT(opcode, dest, op1, op1type, op2, op2type, optype)
		icode_list.append(x)
		return icode_list

	def get_used(self):
		return self.item.get_used()

	def get_defined(self):
		return []
	
class INPUT:
	def __init__(self, tokens):
		self.item = "input()"

	def __str__(self):
		return self.item

	def gencode(self):
		opcode = dest = op1 = op1type = op2 = op2type = optype = None
		opcode = 'input'
		dest = get_temp()
		optype = 3
		return [IR.IR_STMT(opcode, dest, op1, op1type, op2, op2type, optype)]

	def get_used(self):
		return []

def get_rhs(tokens):
	if(check_input(tokens)):
		return INPUT(tokens)
	else:
		return get_ae(tokens)

def check_input(tokens):
	if(tokens[0][1] == 'input'):
		return True
	else:
		return False

class ASSIGN:
	def __init__(self, tokens):
		self.left = tokens[0][1]
		self.right = get_rhs(tokens[2:len(tokens)-1])	

	def gencode(self):
		opcode = dest = op1 = op1type = op2 = op2type = optype = None
		dest = self.left
		opcode = '='
		icode_list = self.right.gencode()
		if(len(icode_list) == 0):
			op1 = self.right.item
		else:
			if(icode_list[len(icode_list)-1].dest == None):
				print "DESTINATION IS NONE.. SEE THIS CASE.. "
				exit(0)
			else:
				op1 = icode_list[len(icode_list)-1].dest
		op1type = 0
		optype = 4
		x = IR.IR_STMT(opcode, dest, op1, op1type, op2, op2type, optype)
		icode_list.append(x)
		return icode_list

	def __str__(self):
		return str(self.left)+"="+str(self.right)

	def get_used(self):
		return self.right.get_used()

	def get_defined(self):
		return [self.left]

def get_stmt(tokens):
	tokens = tokens[:-1]
	if(valid(tokens)):
		if(check_print(tokens)):
			return PRINT(tokens)
		else:
			if(check_assign(tokens)):
				return ASSIGN(tokens)
			else:
				print "Line: ", tokens[0][4], "=> Not a supported statement"  
				exit(0)
	else:
		print "Line: ", tokens[0][4], "=> Not ending with a semicolon(;)"  
		exit(0)

def valid(tokens):
	if(tokens[len(tokens)-1][1] != ';'):
		return False
	else:
		return True

def check_print(tokens):
	if(tokens[0][1] == "print" and tokens[1][1] == '(' and tokens[len(tokens)-2][1] == ')'):
		return True
	else:
		return False

def check_assign(tokens):
	if(tokens[0][1] != "print" and tokens[1][1] == '=' and tokens[len(tokens)-1][1] == ';'):
		return True
	else:
		return False

class PGM:
	def __init__(self, pgm):
		self.pgm = pgm.strip()
		self.nstmts = 0
		self.stmts = []
		self.defined = []
		self.undefined = []
		self.icode = []
	def parse(self):
		if(self.pgm[-1] != ';'):
			print self.pgm, "=> Inalid program. Please end the Program with a ;"
			exit(0)
		start = 0
		while(self.pgm[start:].find(';') != -1):
			stmt = self.pgm[start:start+self.pgm[start:].find(';')+1]
			tokens = generate_tokens(stmt)
			self.stmts.append(get_stmt(tokens))
			self.nstmts = self.nstmts+1
			start = start+self.pgm[start:].find(';') + 1
	def __str__(self):
		start = 0
		res = "" 
		while(start < self.nstmts):
			res = res + str(self.stmts[start])
			start = start + 1
		return res
	def wellformed(self):
		start = 0
		while(start < self.nstmts):
			used = list(set(self.stmts[start].get_used()))
			for key in used:
				if(key not in self.defined):
					print "Not wellformed ", key, "=> Not defined."  
					return False
			created = list(set(self.stmts[start].get_defined()))
			for key in created:
				if key not in self.defined:
					self.defined.append(key)	
			start = start+1
		return True

	def gencode(self):
		if(not self.wellformed()):
			print "Not a well Formed Program. Please correct."
			exit(0)
		start = 0
		while (start < self.nstmts):
			self.icode += self.stmts[start].gencode()
			start += 1
		print self.icode		
		return self.icode
				
#x = PGM("x=5*(-2+3);x=3")
#x.parse()
#print x
#print x.wellformed()
#print x.gencode()	
#x = PGM("x=input();print(1);x=y;")
#x.parse()
#print x
#print x.wellformed()
#print x.gencode()	
#x = PGM("x=1*(-2+3);")
#x.parse()
#print x
#print x.wellformed()
#print x.gencode()	
#x = PGM("x=yy*(-2+3);")
#x.parse()
#print x
#print x.wellformed()
#x = PGM("x=1+2;")
#x.parse()
#print x
#print x.wellformed()
#x = PGM("x=1+-2;")
#x.parse()
#print x
#print x.wellformed()
#x = PGM("x=(a+(b-c))+d;")
#x.parse()
#print x
#print x.wellformed()
#x = PGM("x=(-(b-c))+d;")
#x.parse()
#print x
#print x.wellformed()
