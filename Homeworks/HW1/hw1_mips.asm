	.data
msg:	.asciiz "Please enter a value for N\n"
ex:	.asciiz "The largest value of k for which pow(2,k) is a divisor of N is "
ex2:	.asciiz	".\n"
	
	.text
main:
	li $v0,4
	la $a0, msg	# print the message as a prompt
	syscall
	li $v0,5
	syscall		# read in the value of N.
	move $t0,$v0	# value of N is now in t0
	li $t1, 1		# t1 is now used to store i(2 powers)
	li $t2, 0		# t2 is now x(remainder when divided)
	li $t3, 0		# t3 is now y(value of k)
	li $t4, 2		# multiplication factor
	
lup:	mul $t1, $t1, $t4
	div $t2, $t0, $t1
	beqz $t2, out
	add $t3, $t3, $t2
	b lup

out:	li $v0,4
	la $a0, ex	# print the message to give the answer
	syscall
	li $v0, 1
	move $a0, $t3
	syscall
	li $v0,4
	la $a0, ex2	# print the period and newline
	syscall
	
end:	li $v0, 10
	syscall	
