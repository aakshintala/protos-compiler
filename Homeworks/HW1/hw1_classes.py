
class PBF:
    pass
	
class OR(PBF):
    
    def __init__(self, f1, f2):
        self.lchild = f1
        self.rchild = f2

    def __str__(self):
        return str(self.lchild) + " " + str(self.rchild) + " |"

    def isNNF(self):
	return self.lchild.isNNF() and self.rchild.isNNF()

    def toNNF(self):
	return OR(self.lchild.toNNF(), self.rchild.toNNF())

    def change(self):
		return AND(NOT(self.lchild.toNNF()), NOT(self.rchild.toNNF()));
	
class AND(PBF):

    def __init__(self, f1, f2):
        self.lchild = f1
        self.rchild = f2

    def __str__(self):
        return str(self.lchild) + " " + str(self.rchild) + " &"

    def isNNF(self):
	return self.lchild.isNNF() and self.rchild.isNNF()	

    def toNNF(self):
	return AND(self.lchild.toNNF(), self.rchild.toNNF())

    def change(self):
		return OR(NOT(self.lchild.toNNF()), NOT(self.rchild.toNNF()));	

class NOT(PBF):

    def __init__(self, f):
        self.child = f

    def __str__(self):
        return str(self.child) + " !"

    def isNNF(self):
	if ((str(self.child)[-1] == '!') or (str(self.child)[-1] == '|') or(str(self.child)[-1] == '&')):
		return False
	else:
		return True

    def toNNF(self):
	if ((str(self.child)[-1] == '!') or (str(self.child)[-1] == '|') or(str(self.child)[-1] == '&')):
		return self.child.change();
	else:
		return NOT(self.child)

    def change(self):
		return self.child.toNNF()	

class PROP(PBF):

    def __init__(self, p):
        self.prop = p

    def __str__(self):
        return self.prop

    def isNNF(self):
	return True

    def toNNF(self):
	return PROP(self.prop)
	
def toNNF(expr):
	return expr.toNNF();

def parse(str):
	str1 = str.split()
	if(len(str1) == 0):
		return "Invalid String, No expression to evaluate."
	iter = 0
	operand = []
	operatorcount = 0
	operandcount = 0
	while(iter < len(str1)):
		if(str1[iter] == '|'):
			operatorcount += 1
			operand.append(OR(operand.pop(), operand.pop()))
		elif(str1[iter] == '!'):
			#operatorcount += 1
			if(operandcount == 0):
				return "Invalid String, Operator and Operand count mismatch"
			operand.append(NOT(operand.pop()))
		elif(str1[iter] == '&'):
			operatorcount += 1
			operand.append(AND(operand.pop(), operand.pop()))
		elif(str1[iter].isalpha()):
			operandcount += 1
			operand.append(PROP(str1[iter]))
		else:
			return "Invalid String, Bogus characters detected."
		iter = iter + 1
	if(operandcount > 1 and operatorcount == 0):
		return "Invalid String, Operator and Operand count mismatch"
	obj = operand.pop()
	obj = obj.toNNF()
	return obj
	
print "print AND(PROP('X'), NOT(OR(PROP('Y'), PROP('Z')))) :", AND(PROP('X'), NOT(OR(PROP('Y'), PROP('Z'))))  
print "print AND(PROP('X'), NOT(OR(PROP('Y'), PROP('Z')))).isNNF()  :", AND(PROP('X'), NOT(OR(PROP('Y'), PROP('Z')))).isNNF()    
print "print AND(OR(PROP('X'), NOT(PROP('Y'))), PROP('Z')).isNNF()  :", AND(OR(PROP('X'), NOT(PROP('Y'))), PROP('Z')).isNNF()    
print "print NOT(NOT(PROP('Y'))).isNNF() :", NOT(NOT(PROP('Y'))).isNNF()    
print "print NOT(NOT(NOT(PROP('Y')))).toNNF() :", NOT(NOT(NOT(PROP('Y')))).toNNF()    
print "print PROP('Y').isNNF() :", PROP('Y').isNNF()    
print "print AND(PROP('X'), NOT(OR(PROP('Y'), PROP('Z')))).toNNF()  :", AND(PROP('X'), NOT(OR(PROP('Y'), PROP('Z')))).toNNF()
print "print toNNF(AND(PROP('X'), NOT(OR(PROP('Y'), PROP('Z')))))  :", toNNF(AND(PROP('X'), NOT(OR(PROP('Y'), PROP('Z')))))
print "print parse(\"x ! y ! | a ! b ! | |\") :", parse("x ! y ! | a ! b ! | |")
print "print parse(\"x y | ! z &\") :", parse("x y | ! z &")
print "print parse(\" \") :", parse(" ")
print "print parse(\"x y\") :", parse("x y")
print "print parse(\"!\") :", parse("!")
print "print parse(\"x\") :", parse("x")
