def dup(inp):
	if len(inp) != len(set(inp)):
		return True 
	else:
		return False

''' test cases '''
print "Question 1"
lt = [1,2,2,3]
print lt
print dup(lt)
lt = [1,2,3]
print lt
print dup(lt)
lt = ['a','b','a']
print lt
print dup(lt)
lt = ['a','b']
print lt
print dup(lt)
lt = ['','']
print lt
print dup(lt)
lt = ['']
print lt
print dup(lt)

''' Doesnt take care of spaces or empty string '''

def def_use(inp):
	
	defined=[]
	undefined=[]

	for iter in inp:
		if ((not isinstance(iter, list)) or (len(iter) < 2) or (not isinstance(iter[1], list)) or (isinstance(iter[0], list))):
			print "Not a proper format"
		else:
			for rhs in iter[1]:
				if (rhs not in defined) and (rhs not in undefined) and (rhs):
					undefined.append(rhs)
			if (iter[0] not in undefined) and (iter[0] not in defined):
				defined.append(iter[0])
	return undefined

print "Question 2:"
print def_use([['x',[' ']], ['y',['x']],['z',['x','y']]])
print def_use([['x',[]], ['y',['x']],['z',['x','y']]])
print def_use([ ["x", []], ["y", ["y", "x"]], ["z", ["x","w"]], ["x", ["x","z"]] ])

def ssa(inp):
	
	for iter in inp:
		if (not isinstance(iter, list)) or (len(iter) < 2) or (not isinstance(iter[1], list)) or (isinstance(iter[0], list)):
			print "Not a proper format"
			return

	defin = {}
	undefined = []
	for iter in inp:
		for rhs in range(0, len(iter[1])):
			if (not defin[iter[1][rhs]]):
				if (iter[1][rhs] not in undefined):
					undefined.append(iter[1][rhs])
			else:
				if(defin[iter[1][rhs]] != 0):
					iter[1][rhs] = iter[1][rhs] + str(defin[iter[1][rhs]])
	
		if (defin.has_key(iter[0])):
			defin[iter[0]] += 1
			iter[0] = iter[0] + str(defin[iter[0]])
		else:
			defin[iter[0]] = 0
	
			

print "Question 3:"
lst = [['x',[]],['y',['x']],['x',['x','y']],['z',['x','y']],['y',['z','x']]]
print lst
ssa(lst)
print lst	
