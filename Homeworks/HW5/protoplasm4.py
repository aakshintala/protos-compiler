#!/usr/bin/python

import IR, parser as Parser, reg_alloc as RegisterAlloc, sys, MIPSLowerISel as Lower 

ipfile = open(sys.argv[1],'r')
program = ""
for line in ipfile:
	program += str(line)
blk_map, first_blkid, func_map = Parser.parse(program)
# IR.optimise(blk_map, first_blkid)
Parser.print_sequence(blk_map, first_blkid, [])
insoutsmaps = IR.dofuncliveness(blk_map, func_map)
print
for iter in insoutsmaps:
	print iter, insoutsmaps[iter].ins
	print
	print iter, insoutsmaps[iter].outs
cnode, blk_map = RegisterAlloc.kcolor(insoutsmaps,21,blk_map,first_blkid)
print cnode
#Parser.print_sequence(blk_map ,first_blkid,[])
# machinecode = Lower.MipsLowerISel(blk_map, first_blkid, cnode)
machinecode = Lower.MCLower(blk_map, func_map, cnode)
# print machinecode
opfile = open(sys.argv[1].rstrip('proto')+"asm",'w')
for line in machinecode:
	opfile.write(line)

