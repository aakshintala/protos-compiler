#!/bin/bash
iter=1

while [ $iter -lt 24 ]; do
	python protoplasm4.py test/test$iter.proto
	spim -file test/test$iter.asm
	let iter=iter+1
done

