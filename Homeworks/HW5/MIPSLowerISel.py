#class REGIMM():
#	value
#	vtype = Variable: 0 Immediate: 1 Memory: 10	
		
#class IR_STMT():
#	opr = Type of Operator: * + - / % uminus input print loadi move
#	dest = Destination
#	src1 = REGIMM(src1,src1type) or None
#	src2 = REGIMM(src2,src2type) or None
#	operatortype = Arithmetic: 0, Loadi: 1, Print: 2, Input: 3 , Move: 4
#	spill = False/True
stack = []

block = 0
def FetchBlock():
	global block
	block += 1
	return "block" + str(block)

def FetchReg(var,cnode):
	if(not cnode.has_key(var)):
		print "Dead Code Executed? Requesting register for uncolored node", var
#		exit()
		return 
	color = cnode[var]
	if color <= 9:
		return "$t" + str(color)
	elif color > 9 and color < 18:
		return "$s" + str(color-10)
	elif color > 17 and color < 22:
		return "$a" + str(color - 18)
	else:
		print "Problem in register Allocation. Exiting"
		exit()

def LowerArithOp(IRInst, cnode): #'==' '!=' '<' '>' '>=''<='
	regdest = FetchReg(IRInst.dest,cnode)
	if IRInst.src1.vtype != 0:
		print "Variable SRC1 value not denoted right. Error in IR. Exiting.", IRInst
		exit()
	
	if IRInst.opr != 'uminus' and IRInst.opr != 'not' and IRInst.src2.vtype !=0:
		print "Variable SRC2 value not denoted right. Error in IR. Exiting.", IRInst
		exit()
	
	regsrc1 = FetchReg(IRInst.src1.value,cnode)
	
	if IRInst.opr == '*':
		regsrc2 = FetchReg(IRInst.src2.value,cnode)
		MCInst = "\tmul " + regdest + "," + regsrc1 + "," + regsrc2 + "\n"
	elif IRInst.opr == '+':
		regsrc2 = FetchReg(IRInst.src2.value,cnode)
		MCInst = "\tadd " + regdest + "," + regsrc1 + "," + regsrc2 + "\n"
	elif IRInst.opr == '-':
		regsrc2 = FetchReg(IRInst.src2.value,cnode)
		MCInst = "\tsub " + regdest + "," + regsrc1 + "," + regsrc2 + "\n"
	elif IRInst.opr == '/':
		regsrc2 = FetchReg(IRInst.src2.value,cnode)
		MCInst = "\tdiv " + regdest + "," + regsrc1 + "," + regsrc2 + "\n"
	elif IRInst.opr == '%':
		regsrc2 = FetchReg(IRInst.src2.value,cnode)
		MCInst = "\trem " + regdest + "," + regsrc1 + "," + regsrc2 + "\n"
	elif IRInst.opr == 'uminus':
		MCInst = "\tneg " + regdest + "," + regsrc1 + "\n"
	elif IRInst.opr == 'not':
		MCInst = "\tnot " + regdest + "," + regsrc1 + "\n"
	elif IRInst.opr == '==':
		regsrc2 = FetchReg(IRInst.src2.value,cnode)
		MCInst = "\tseq " + regdest + "," + regsrc1 + "," + regsrc2 + "\n"
	elif IRInst.opr == '!=':
		regsrc2 = FetchReg(IRInst.src2.value,cnode)
		MCInst = "\tsne " + regdest + "," + regsrc1 + "," + regsrc2 + "\n"
	elif IRInst.opr == '<' :
		regsrc2 = FetchReg(IRInst.src2.value,cnode)
		MCInst = "\tslt " + regdest + "," + regsrc1 + "," + regsrc2 + "\n"
	elif IRInst.opr == '>' :
		regsrc2 = FetchReg(IRInst.src2.value,cnode)
		MCInst = "\tsgt " + regdest + "," + regsrc1 + "," + regsrc2 + "\n"
	elif IRInst.opr == '>=':
		regsrc2 = FetchReg(IRInst.src2.value,cnode)
		MCInst = "\tsge " + regdest + "," + regsrc1 + "," + regsrc2 + "\n"
	elif IRInst.opr == '<=':
		regsrc2 = FetchReg(IRInst.src2.value,cnode)
		MCInst = "\tsle " + regdest + "," + regsrc1 + "," + regsrc2 + "\n"
	else:
		print "Operator Type Mismatch. Not an Arithmetic Operator. Exiting.", IRInst
		exit()
	return MCInst

def LowerMove(IRInst, cnode):
	regdest = FetchReg(IRInst.dest,cnode)
	if IRInst.src1.vtype != 0:
		print "Move requires a variable value in src. Error in IR. Exiting.", IRInst
		exit()
	regsrc1 = FetchReg(IRInst.src1.value,cnode)
	if(regdest == None or regsrc1 == None):
	  return "";
	MCInst = "\tmove "+ regdest + "," + regsrc1 + "\n"	
	return MCInst

def LowerLoadi(IRInst, cnode):
	reg = FetchReg(IRInst.dest,cnode)
	if IRInst.src1.vtype != 1:
		print "Immediate value not denoted right. Error in IR. Exiting.", IRInst
		exit()
	MCInst = "\tli "+ reg + "," + str(IRInst.src1.value) + "\n"	
	return MCInst
	
def LowerInput(IRInst, cnode):
	MCInst = "\n\tli $v0,5\n"
	MCInst += "\tsyscall\n"
	reg = FetchReg(IRInst.dest,cnode)
	MCInst += "\tmove "+ reg + ",$v0" + "\n"	
	return MCInst

def LowerPrint(IRInst, cnode):
	MCInst = "\n\tli $v0,1\n"
	reg = FetchReg(IRInst.src1.value,cnode)
	MCInst += "\tmove " + "$a0," + reg + "\n"	
	MCInst += "\tsyscall\n"
	MCInst += "\tli $v0,4\n\tla $a0,newline\n\tsyscall\n"
	return MCInst
	
def LowerLoad(IRInst, cnode): #mem_load,c8,M[c],None,5,False
	reg = FetchReg(IRInst.dest,cnode)
	if IRInst.src1.vtype != 10:
		print "Memory value not denoted right. Error in IR. Exiting.", IRInst
		exit()
	MCInst = "\n\tlw "+ reg + "," + IRInst.src1.value + "\n"	
	return MCInst

def LowerStore(IRInst, cnode): #store,M[y],y10,0,None,6,False
	if IRInst.src1.vtype != 0:
		print "Register value not denoted right. Error in IR. Exiting." , IRInst
		exit()
	reg = FetchReg(IRInst.src1.value,cnode)
	MCInst = "\n\tsw "+ reg + "," + IRInst.dest + "\n"	
	return MCInst

def LowerIfEqual(IRInst,blk_map,cnode): #"ife,_b5,_t8,_b6,7,False"
	if IRInst.src1.vtype != 0:
		print "Register value not denoted right. Error in IR. Exiting." , IRInst
		exit()
	reg = FetchReg(IRInst.src1.value,cnode)
	MCInst = "\n\tbgtz "+ reg + "," + IRInst.dest + "\n" + IRInst.src2.value + ":\n"
	templist = MipsLowerISel(blk_map, IRInst.src2.value, cnode)
	for iter in templist:
		MCInst += iter
	returnblock = FetchBlock()
	MCInst += "\n\tb "+ returnblock + "\n"
	MCInst += IRInst.dest + ":\n"
	templist = MipsLowerISel(blk_map, IRInst.dest, cnode)
	for iter in templist:
		MCInst += iter
	MCInst += returnblock + ":\n"
	return MCInst
	
def LowerIf(IRInst,blk_map,cnode): #"if,_b2,_t0,None,8,False"
	if IRInst.src1.vtype != 0:
		print "Register value not denoted right. Error in IR. Exiting." , IRInst
		exit()
	reg = FetchReg(IRInst.src1.value,cnode)
	returnblock = FetchBlock()
	MCInst = "\n\tbeqz "+ reg + "," + returnblock + "\n" + IRInst.dest + ":\n"
	templist = MipsLowerISel(blk_map, IRInst.dest, cnode)
	for iter in templist:
		MCInst += iter
	MCInst += returnblock + ":\n"
	return MCInst
	
def LowerWhileCond(IRInst,blk_map,cnode): #"if,_b2,_t0,None,8,False"
	global stack
	if IRInst.src1.vtype != 0:
		print "Register value not denoted right. Error in IR. Exiting." , IRInst
		exit()
	reg = FetchReg(IRInst.src1.value,cnode)
	#returnblock = FetchBlock()
	MCInst = "\n\tbeqz "+ reg + "," + stack.pop() + "\n" + IRInst.dest + ":\n"
	templist = MipsLowerISel(blk_map, IRInst.dest, cnode)
	for iter in templist:
		MCInst += iter
	#MCInst += returnblock + ":\n"
	return MCInst

def LowerWhileStart(IRInst,blk_map,cnode):
	global stack
	returnblock = FetchBlock()
	stack.append(returnblock)
	MCInst = "\n" + IRInst.dest + ":\n"
	templist = MipsLowerISel(blk_map, IRInst.dest, cnode)
	for iter in templist:
		MCInst += iter
	MCInst += "\n\tb "+ IRInst.dest 
	MCInst += "\n" + returnblock + ":\n"
	return MCInst
	
def LowerJmp(IRInst,blk_map,cnode): #"jmp,_b16,None,None,10,False"
	returnblock = FetchBlock()
	MCInst = "\n\tb "+ IRInst.dest + "\n" + IRInst.dest + ":\n"
	templist = MipsLowerISel(blk_map, IRInst.dest, cnode)
	for iter in templist:
		MCInst += iter
	MCInst += returnblock + ":\n"
	return MCInst
	
def LowerAlloc(IRInst,cnode):			#  "alloc,x,_t2,None,60,False"
	MCInst = "\n\tli $v0,9\n"
	reg = FetchReg(IRInst.src1.value,cnode)
	MCInst += "\tadd " + reg + "," + reg + ",1\n"
	MCInst += "\tmul " + reg + "," + reg + ",4\n"
	MCInst += "\tmove " + "$a0," + reg + "\n"	
	MCInst += "\tsyscall\n"
	regdest = FetchReg(IRInst.dest,cnode)
	MCInst += "\tmove " +	regdest + ",$v0" + "\n"
	MCInst += "\tdiv " + reg + "," + reg + ",4\n"
	MCInst += "\tsub " + reg + "," + reg + ",1\n"
	MCInst += "\tsw "+ reg + ", (" +	regdest + ")\n"
	return MCInst
	
def LowerAddress(IRInst,cnode):		#  "aload,_t4,x,_t5,55,False"
	regsrc1 = FetchReg(IRInst.src1.value,cnode)
	regsrc2 = FetchReg(IRInst.src2.value,cnode)
	regdest = FetchReg(IRInst.dest,cnode)
	MCInst = "\tadd " + regdest + "," + regsrc2 + ",1\n"
	MCInst += "\tmul " + regdest + "," + regdest + ",4\n"
	MCInst += "\tadd " + regdest + "," + regsrc1 + "," + regdest + "\n"
	return MCInst

def LowerArrayStore(IRInst,cnode):	#	"store,x,2,_t3,_t5,61,False"
	regsrc1 = FetchReg(IRInst.src1.value,cnode)
	regdest = FetchReg(IRInst.dest,cnode)
	MCInst = "\tsw " + regsrc1 + ", (" + regdest + ")" + "\n"
	return MCInst

def LowerDeref(IRInst, cnode): #"deref,_t8,0,_t5,None,90,False"
	regsrc1 = FetchReg(IRInst.src1.value,cnode)
	regdest = FetchReg(IRInst.dest,cnode)
	MCInst = "\tlw " + regdest + ", (" + regsrc1 + ")" + "\n"
	return MCInst

def LowerCallStart(IRInst, cnode): #"call_start,None,None,None,6,101,False"
	MCInst = "\taddiu $sp, $sp, -40\n"
	for i in range(0,9):
		MCInst += "\tsw $t" + str(i) + "," + str(i*4)  + "($sp)" + "\n"
	return MCInst

def LowerCallEnd(IRInst, cnode): #"call_end,_t0,0,_b2,None,102,False"
	MCInst = "\n\tjal "+ IRInst.src1.value + "\n" 
	for i in range(0,9):
		MCInst += "\tlw $t" + str(i) + "," + str(i*4)  + "($sp)" + "\n"
	MCInst += "\taddiu $sp, $sp, 40\n"
	if(IRInst.dest != None):
		regdest = FetchReg(IRInst.dest,cnode)
		MCInst += "\tmove " + regdest + ",$v0"  + "\n"
	return MCInst

def LowerArgPush(IRInst, cnode): #"push,None,None,_t3,None,103,False"
	regsrc1 = FetchReg(IRInst.src1.value,cnode)
	MCInst = "\taddiu $sp, $sp, -4\n"
	MCInst += "\tsw " + regsrc1 + ", ($sp)" + "\n"
	return MCInst

def LowerArgPop(IRInst,cnode):	#"pop,None,None,x,None,104,False"
	regsrc1 = FetchReg(IRInst.src1.value,cnode)
	MCInst = "\tlw " + regsrc1 + ", ($sp)" + "\n"
	MCInst += "\taddiu $sp, $sp, 4\n"
	return MCInst

def LowerFuncStart(IRInst,cnode): #"func_start,None,None,None,None,105,False"
	MCInst = "\taddiu $sp, $sp, -48\n"
	for i in range(0,7):
		MCInst += "\tsw $s" + str(i) + "," + str(i*4)  + "($sp)" + "\n"
	for i in range(0,3):
		MCInst += "\tsw $a" + str(i) + "," + str((i+8)*4)  + "($sp)" + "\n"
	MCInst += "\taddiu $sp, $sp, -4\n"
	MCInst += "\tsw $ra,($sp)\n"
	return MCInst
	
def LowerReturn(IRInst,cnode): #"ret,_t0,0,_t5,None,110,False"
	MCInst = "\tlw $ra,($sp)\n"
	MCInst += "\taddiu $sp, $sp, 4\n"
	for i in range(0,7):
		MCInst += "\tlw $s" + str(i) + "," + str(i*4)  + "($sp)" + "\n"
	for i in range(0,3):
		MCInst += "\tlw $a" + str(i) + "," + str((i+8)*4)  + "($sp)" + "\n"
	MCInst += "\taddiu $sp, $sp, 48\n"
	if(IRInst.src1 != None):
		regsrc1 = FetchReg(IRInst.src1.value,cnode)
		MCInst += "\tmove " + "$v0," + regsrc1 + "\n"
	MCInst += "\tjr $ra\n"
	return MCInst

# _b1    "<=,_t14,0,_t14,y,0,False"
# _b1    "exit,None,0,_t14,None,81,False"
def LowerBoundCheck(IRInst,cnode):
	if IRInst.src1.vtype != 0:
		print "Register value not denoted right. Error in IR. Exiting." , IRInst
		exit()
	reg = FetchReg(IRInst.src1.value,cnode)
	MCInst = "\n\tbgtz "+ reg + ", boundfail" + "\n"
	return MCInst

def Init(blk_map, blkid, cnode):
	insts = []
	memnodes = []
	insts.append("\t.data\nnewline:\t.asciiz\t\"\\n\"\n")
	insts.append("boundmsg:\t.asciiz\t\"Array Index out of Bounds.\\n\"\n")
	for key in blk_map:
		irlist = blk_map[key]
		for iter in irlist:
			if iter.operatortype == 6:
				if(not iter.dest in memnodes):
					insts.append(str(iter.dest)+":\t.word\t"+"0\n")
					memnodes.append(iter.dest)
		# toremove = []
		# for iter in range(0,len(irlist)-1): #"-,t6,x10,0,t7,0,False", "=,y1,t6,0,None,4,False"
		# 	if irlist[iter].operatortype == 4:
		# 		if irlist[iter-1].operatortype == 0:
		# 			if irlist[iter-1].dest == irlist[iter].src1.value:
		# 				irlist[iter-1].dest = irlist[iter].dest
		# 				toremove.append(irlist[iter])
	
		# for iter in toremove:
		# 	irlist.remove(iter)
	
	insts.append("\n\t.text\nmain:\n")
	return insts

def MCLower(blk_map, func_map, cnode):
	insts = []
	insts += Init(blk_map, func_map['main'], cnode)
	insts += MipsLowerISel(blk_map, func_map['main'], cnode)
	insts.append("exit:\tli $v0, 10\n\tsyscall\n\n")
	insts.append("boundfail:\n\tli $v0,4\n\tla $a0,boundmsg\n\tsyscall\n\tb exit\n")
	for k,v in func_map.iteritems():
		if(k == 'main'):
			pass
		else:
			insts.append("\n\n"+v+":\n")
			insts += MipsLowerISel(blk_map, v, cnode)
	return insts

def MipsLowerISel(blk_map, blkid, cnode):
	insts = []
	irlist = blk_map[blkid]	
	for iter in irlist:
		if iter.operatortype == 0:
			insts.append(LowerArithOp(iter,cnode))
		elif iter.operatortype == 1:
			insts.append(LowerLoadi(iter,cnode))
		elif iter.operatortype == 2:
			insts.append(LowerPrint(iter,cnode))
		elif iter.operatortype == 3:
			insts.append(LowerInput(iter,cnode))
		elif iter.operatortype == 4:
			insts.append(LowerMove(iter,cnode))
		elif iter.operatortype == 5:
			insts.append(LowerLoad(iter,cnode))
		elif iter.operatortype == 6:
			insts.append(LowerStore(iter,cnode))
		elif iter.operatortype == 7:
			insts.append(LowerIfEqual(iter,blk_map,cnode))
		elif iter.operatortype == 8:
			insts.append(LowerIf(iter,blk_map,cnode))
		elif iter.operatortype == 9:
			insts.append(LowerWhileStart(iter,blk_map,cnode))
		elif iter.operatortype == 10:
			insts.append(LowerJmp(iter,blk_map,cnode))
		elif iter.operatortype == 11:
			pass
		elif iter.operatortype == 12:
			insts.append(LowerWhileCond(iter,blk_map,cnode))
#		elif iter.operatortype == 52:
#			insts.append(LowerMove(iter,cnode))
		elif iter.operatortype == 55:
			insts.append(LowerAddress(iter,cnode))
		elif iter.operatortype == 60:
			insts.append(LowerAlloc(iter,cnode))
		elif iter.operatortype == 61:
			insts.append(LowerArrayStore(iter,cnode))
		elif iter.operatortype == 81:
			insts.append(LowerBoundCheck(iter,cnode))
		elif iter.operatortype == 90:
			insts.append(LowerDeref(iter,cnode))
		elif iter.operatortype == 101:
			insts.append(LowerCallStart(iter,cnode))
		elif iter.operatortype == 102:
			insts.append(LowerCallEnd(iter,cnode))
		elif iter.operatortype == 103:
			insts.append(LowerArgPush(iter,cnode))
		elif iter.operatortype == 104:
			insts.append(LowerArgPop(iter,cnode))
		elif iter.operatortype == 105:
			insts.append(LowerFuncStart(iter,cnode))
		elif iter.operatortype == 110:
			insts.append(LowerReturn(iter,cnode))
		else:
			print "Invalid Operation Type. Exiting", iter.operatortype
			exit()	
	return insts
