#!/bin/bash
iter=1

while [ $iter -lt 70 ]; do
	python protoplasm5.py test/t$iter.proto
	spim -file test/t$iter.asm
	let iter=iter+1
done

