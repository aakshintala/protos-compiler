import ply.lex as lex

keywords = { 'extends':'EXTENDS', 'this':'THIS', 'super':'SUPER', 'class':'CLASS', 'return':'RETURN', 'void':'VOID','new':'NEW','for':'FOR','int':'INT','bool':'BOOL','true':'TRUE','false':'FALSE','input':'INPUT','print':'PRINT','if':'IF','then':'THEN','else':'ELSE','while':'WHILE','do':'DO'}
# Tokens
tokens = [ 'DOT','DEC','INC','LSQ','RSQ','COMA','INTCONST', 'ID', 'ASSIGN', 'SCOLON', 'LOPEN', 'ROPEN', 'LCURL', 'RCURL', 'ADD', 'SUB', 'MUL', 'DIV', 'MOD', 'AND', 'OR', 'EQ', 'NEQ', 'LT', 'LTEQ', 'GT', 'GTEQ', 'NOT' ] + list(keywords.values())

t_ignore = " \t"
t_ASSIGN = r'\='
t_SCOLON = r'\;'
t_LOPEN = r'\('
t_ROPEN = r'\)'
t_LCURL = r'\{'
t_RCURL = r'\}'
t_ADD = r'\+'
t_SUB = r'\-' 
t_MUL = r'\*'
t_DIV = r'\/'
t_MOD = r'\%'
t_AND = r'\&\&'
t_OR = r'\|\|'
t_EQ = r'\=\='
t_NEQ = r'!='
t_LT = r'<'
t_LTEQ = r'<='
t_GT = r'>'
t_GTEQ = r'>='
t_NOT = r'!'
t_INC = r"\+\+"
t_DEC = r'\-\-'
t_LSQ = r'\['
t_RSQ = r'\]'
t_COMA = r'\,'
t_DOT = r'\.'

def t_comment(t):
	r'//.*'
	pass

def t_newline(t):
    r'\n+'
    t.lexer.lineno += t.value.count("\n")

def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)

def t_INTCONST(t):
    r'\d+'
    t.value = int(t.value)
    return t

def t_ID(t):
	r'[a-zA-Z][a-zA-Z0-9]*'
	if(not keywords.has_key(t.value)):
		t.type = 'ID'
	else:
		t.type = keywords[t.value]
	return t


# Build the lexer
lexer = lex.lex()

#input ='''a = input();
#b = a || !2;
#c = a + b;
#d = 2 --3 ;
#print(d - 4 * a);'''

# Give the lexer some input
#lexer.input(input)

# Tokenize
#for tok in lexer:
#    print tok.type,tok.value
