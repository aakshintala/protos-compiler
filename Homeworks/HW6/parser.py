import ply.yacc as yacc
#import sys
import IR
import copy
from lexer import tokens
precedence = (('left','OR'),
		('left', 'AND'),
		('nonassoc', 'EQ', 'NEQ'),
		('nonassoc', 'GT','LT','GTEQ','LTEQ'),
		('left', 'ADD','SUB'),
		('left', 'MUL','DIV','MOD'),
		('right', 'NOT'),
		('right', 'UMINUS'))

block_map = {}
global_id =0
#s_table = {}
class_def = {}
fun_def = {}
sym_table = {}
last_func_type = ''
class_def_order = {}
fun_blk_map = {}
last_class_def_name = ''
parent_classes = {}
global_table = []
curr_level = 0
funcall_called = 0

def set_temp():
        global global_id
        global_id = 0

def get_temp():
        global global_id
        global_id = global_id + 1
        return '_t'+str(global_id-1)

#return_temp = get_temp()

def convert_addr(icode_list):
	ptr = icode_list[len(icode_list)-1].dest
	ptrtype = icode_list[len(icode_list)-1].desttype
	if(ptrtype == 2):
		op1 = get_temp()
		icode_list.append(IR.IR_STMT('deref',op1, 0, ptr, ptrtype, None, None, 90))

def getitem(iter):
	x = ''
	if(isinstance(iter, list)):
		for another in iter:
			x = x + getitem(another)
	else:
		x = x + str(iter)
	return x

bno = 0
def get_new_block():
	global bno
	bno = bno + 1
	return "_b"+str(bno)

cur_bno = ''
def get_cur_block():
	global cur_bno
	return str(cur_bno)

def set_cur_block(x):
	global cur_bno
	cur_bno = x

main_blk = get_new_block()
this_var = get_temp()

class TYPE:
	def __init__(self, ptype, atype, dim):
		self.ptype = ptype
		self.atype = atype
		self.dim = dim
						
class NODE:
	def __init__(self,op,items):
		#print op, items
		self.items = items
		self.op = op
		self.sym_table = {}
		self.dtype = None

	def __str__(self):
		x = str(self.op) + " "
		for iter in self.items:
			x = x + getitem(iter)
		return x

	def wellformed(self, declared, defined, lhs):

		if(self.op == 'vardecl'):
			return self.items[1].wellformed(declared, defined, 2)

		if(self.op == 'fundecl'):
			if(self.items[1].items[0] not in declared):
				declared.append(self.items[1].items[0])
			if(self.items[1].items[0] not in defined):
				defined.append(self.items[1].items[0])
			
			local_defined = defined[:]		
			local_declared = declared[:]
		
			local_declared.append('return')	
			if(not self.items[3].wellformed(local_declared, local_defined, 2)):
				return False
			if(not self.items[4].wellformed(local_declared, local_defined, 0)):
				return False

			if('return' not in local_defined):
				return False	
			return True

		if(self.op == 'funcall'):
			if(not self.items[0].wellformed(declared, defined, 0)):
				return False
			if(not self.items[1].wellformed(declared, defined, 0)):
				return False
			return True
	
		if(self.op == 'newobj'):
			return self.items[0].wellformed(declared, defined, 0)

		if(self.op == 'newarray'):
			return self.items[1].wellformed(declared, defined, 0)
	
		if(self.op == 'extends'):
			return self.items[0].wellformed(declared, defined, 0)

		if(self.op == 'memberdeclstar'):
			for element in self.items:
				if(element.op != 'vardecl'):
					if(not element.wellformed(declared, defined, 0)):
						return False
			return True
	
		if(self.op == 'classdecl'):

			if(self.items[1].op != 'epsilon'):
				self.items[1].wellformed(declared, defined, 0)

			if(self.items[0].items[0] not in declared):
				declared.append(self.items[0].items[0])
			if(self.items[0].items[0] not in defined):
				defined.append(self.items[0].items[0])
			
			if(not self.items[2].wellformed(declared, defined, 0)):
				return False
			return True

		if(self.op == 'formals'):
			if(self.items[1].items[0] not in declared):
				declared.append(self.items[1].items[0])
			if(self.items[1].items[0] not in defined):
				defined.append(self.items[1].items[0])
			if(len(self.items) == 4):
				return self.items[3].wellformed(declared, defined, 2)
			else:
				return True

		if(self.op == '='):
			if(not self.items[1].wellformed(declared, defined, 0)):
				return False
			else:
				if(not self.items[0].wellformed(declared, defined, 1)):
					return False
				return True

		if(self.op == 'ife'):
			if(not self.items[0].wellformed(declared, defined, 0)):
				return False

			local_defined1 = defined[:]
			local_defined2 = defined[:]
			local_declared1 = declared[:]
			local_declared2 = declared[:]

			if(not self.items[1].wellformed(local_declared1, local_defined1,0) or not self.items[2].wellformed(local_declared2, local_defined2,0)):
				return False
		
			for iter1 in local_defined1:
				if iter1 in local_defined2:
					if (iter1 not in defined) and (iter1 in declared):
						defined.append(iter1)
			return True		
		
		if(self.op == 'if' or self.op == 'while'):
			local_defined = defined[:]
			local_declared = declared[:]
			if(not self.items[0].wellformed(declared, defined, 0) or not self.items[1].wellformed(local_declared, local_defined, 0)):
				return False
			else:
				return True

		if(self.op == 'dowhile'):
			local_defined = defined[:]
			local_declared = declared[:]
			if(not self.items[0].wellformed(local_declared, local_defined, 0) or not self.items[1].wellformed(declared, defined, 0)):
				return False
			for iter1 in local_defined:
				if (iter1 not in defined) and (iter1 in declared):
					defined.append(iter1)
			return True		

		if(self.op == 'Pgm_blk'):

			local_defined = defined[:]
			local_declared = declared[:]
			for element in self.items:
				if(not element.wellformed(local_declared, local_defined, 0)):
					return False
			#print local_defined, local_declared
			for iter1 in local_defined:
				if (iter1 not in defined) and (iter1 in declared):
					defined.append(iter1)
			return True

		if(self.op == 'for'):
			if(not self.items[0].wellformed(declared, defined, 0)):
				return False
			if(not self.items[1].wellformed(declared, defined, 0)):
				return False
			if(not self.items[2].wellformed(declared, defined, 0)):
				return False
	
			local_defined = defined[:]
			local_declared = declared[:]
			if(not self.items[3].wellformed(local_declared, local_defined, 0)):
				return False
			else:
				return True

		if(self.op == 'objaccess'):
			return self.items[0].wellformed(declared, defined, 0)
			
		if(self.op == 'intconst' or self.op == 'input' or self.op == 'bool' or self.op == 'void' or self.op == 'int' or self.op == 'this' or self.op == 'super'):
			return True

		if(self.op == 'id'):
			if(lhs == 2):
				if(self.items[0] not in declared):
					declared.append(self.items[0])
				return True

			if(lhs == 1):
				if(self.items[0] not in declared):
					print str(self.items[0]), "not declared" 
					return False
				else:
					if(self.items[0] not in defined):
						defined.append(self.items[0])
				return True
			else:
				if(self.items[0] not in declared):
					print str(self.items[0]), "not declared" 
					return False
				if(self.items[0] not in defined):
					print str(self.items[0]), "not defined" 
					return False
				else:
					return True
				
		if(self.op == 'vardecl'):
			if(not self.items[1].wellformed(declared, defined, 0)):
				return False
			return True

		if(self.op == 'varlist'):
			if(not self.items[0].wellformed(declared, defined, 2)):
				return False
			if(len(self.items) == 3):
				return self.items[2].wellformed(declared, defined, 2)
			return True

		if(self.op == 'array'):
			if(not self.items[1].wellformed(declared, defined, 0)):
				return False
			if(not self.items[0].wellformed(declared, defined, 0)):
				return False
			return True

		if(self.op == 'return'):
			if(not self.items[0].wellformed(declared, defined, 0)):
				return False
			defined.append('return')
			return True

		for element in self.items:
			if(not element.wellformed(declared, defined, 0)):
				return False
		return True

	def settype(self, dtype):
		if(self.op == 'varlist'):
			if(self.items[1].op == 'epsilon'):
				self.items[0].settype(dtype)
			else:
				temp = self.items[1]
				dim = 0;
				while(temp.op != 'epsilon'):
					dim = dim + 1
					temp = temp.items[0]
				dtype_local = copy.deepcopy(dtype)
				dtype_local.ptype = 'array'
				dtype_local.dim = dim
				self.items[0].settype(dtype_local)

			if(len(self.items) == 3):
				self.items[2].settype(dtype)
			return

		if(self.op == 'id'):
			if(not sym_table.has_key(self.items[0])):
				sym_table[self.items[0]] = [dtype, 0]
			else:
				if(sym_table[self.items[0]][0].ptype != dtype.ptype):
					print "new declaration doesnt match with older declaration"
					exit()
				else:
					sym_table[self.items[0]][1] = sym_table[self.items[0]][1] + 1
					self.items[0] = self.items[0] + str(sym_table[self.items[0]][1])
					sym_table[self.items[0]] = [dtype, 0]
			return

	def set_idlist(self, memtype, memlist):
		if(self.op == 'varlist'):
			
			if(self.items[1].op == 'epsilon'):
				memlist.append([memtype, self.items[0].items[0]])
			else:
				temp = self.items[1]
				dim = 0;
				while(temp.op != 'epsilon'):
					dim = dim + 1
					temp = temp.items[0]
				memtype_local = copy.deepcopy(memtype)
				memtype_local.ptype = 'array'
				memtype_local.dim = dim
				memlist.append([memtype_local, self.items[0].items[0]])

			if(len(self.items) == 3):
				self.items[2].set_idlist(memtype, memlist)
			return
		else:
			print "some wrong control flow. see this.."
			exit()
			
	def get_memberlist(self, memberlist):

		if(self.op == 'memberdeclstar'):
			for element in self.items:
				element.get_memberlist(memberlist)
			return

		if(self.op == 'vardecl'):
			memtype = self.items[0].make_type()
			self.items[1].set_idlist(memtype, memberlist)
			return

	def get_funclist(self):
		if(self.op == 'memberdeclstar'):
			for element in self.items:
				element.get_funclist()
			return

		if(self.op == 'fundecl'):
			dtype = self.get_type()
			if(dtype.ptype == 'Er_type'):
				print 'function type not type correct'
				exit()
                        return 

	def get_typelist(self, typelist):
		if(self.op == 'formals'):
			dtype = self.items[0].make_type()
			if(self.items[2].op != 'epsilon'):
				temp = self.items[2]
				dim = 0;
				while(temp.op != 'epsilon'):
					dim = dim + 1
					temp = temp.items[0]
				dtype.ptype = 'array'
				dtype.dim = dim
			
			typelist.append([dtype, self.items[1].items[0]])

			if(len(self.items) == 4):
				self.items[3].get_typelist(typelist)

		else:
			if(self.op == 'epsilon'):
				#typelist.append(TYPE('void', 'void', 0))
				#print 'adding epsilon type in the list'
				return
			else:
				print "wrong call to get_typelist.."
				exit()

	def make_type(self):
		if(self.op == 'int' or self.op == 'bool' or self.op == 'void'):
			return TYPE(self.op, self.op, 0)
		if(self.op == 'id'):
			if(not class_def.has_key(self.items[0])):
				print 'class not defined'
				exit()
			else:
				return TYPE(self.items[0], self.items[0], 0)

	def get_type(self):

		global sym_table
		global last_func_type
		global last_class_def_name
		global parent_classes

		if(self.op == 'input'):
			return TYPE('int', 'int', 0)

		if(self.op == 'id'):
			if(not sym_table.has_key(self.items[0])):
				print self.items[0] + ' doesnt exist'
				exit()
			else:
				local_copy = copy.deepcopy(sym_table[self.items[0]][0])
				self.dtype = sym_table[self.items[0]][0]
				if(sym_table[self.items[0]][1]):
					self.items[0] = self.items[0] + str(sym_table[self.items[0]][1])
				return local_copy

		if(self.op == 'varlist'):
			print 'get_type called on varlist'
			exit()

		if(self.op == 'this'):
			if(last_class_def_name == ''):
				print 'No class scope'
				exit()
			else:
				return TYPE(last_class_def_name, last_class_def_name, 0)

		if(self.op == 'super'):
			if(last_class_def_name == ''):
				print 'No class scope'
				exit()
			else:
				if(len(parent_classes[last_class_def_name]) == 0):
					print 'No parent class'
					exit()
				else:
					pclass = parent_classes[last_class_def_name][0]
					return TYPE(pclass, pclass, 0)

		if(self.op == 'declseq'):
			for element in self.items:
				x = element.get_type()
				if(x.ptype == 'Er_type'):
					print 'error in declaration'
					exit()
			return TYPE('Success_type', 'Success_type', 0)

		if(self.op == 'epsilon'):
			return TYPE('void', 'void', 0);

		if(self.op == 'vardecl'):
			x = self.items[0].make_type()
			self.items[1].settype(x)
			return TYPE('Success_type', 'Success_type', 0)

		if(self.op == 'formals'):
			dtype = self.items[0].make_type()
			dim = 0;
			if(self.items[2].op != 'epsilon'):
				temp = self.items[2]
				while(temp.op != 'epsilon'):
					dim = dim + 1
					temp = temp.items[0]

			if(dim != 0):
				dtype.ptype = 'array'
				dtype.dim = dim

			self.items[1].settype(dtype)

			if(len(self.items) == 4):
				if (self.items[3].get_type().ptype == 'Er_type'):
					print 'Type error in formals'
					exit()
			return TYPE('Success_type', 'Success_type', 0)

		if(self.op == 'fundecl'):
			if(fun_def.has_key(self.items[1].items[0])):
				print "function already declared"
				exit()
		
			typelist = []
			self.items[3].get_typelist(typelist)

			dim = 0;
			if(self.items[2].op != 'epsilon'):
				temp = self.items[2]
				while(temp.op != 'epsilon'):
					dim = dim + 1
					temp = temp.items[0]

			dtype = self.items[0].make_type()
			if(dim != 0):
				dtype.ptype = 'array'
				dtype.dim = dim

			fun_name = ''
			if(last_class_def_name != ''):
				fun_name = last_class_def_name + '_' + self.items[1].items[0]
			else:
				fun_name = self.items[1].items[0]

			for e in typelist:
				fun_name = fun_name + '_' + e[0].ptype + '_' + e[0].atype + '_' + str(e[0].dim)

			fun_def[fun_name] = [dtype, typelist]
			self.fname = fun_name
			last_func_type = copy.deepcopy(dtype)	
				
			self.sym_table = copy.deepcopy(sym_table)
			
			if(self.items[3].get_type() == 'Er_type'):
				print "something wrong with Formals. we are declaring the arguments of a function"
				exit()
			ret_type = self.items[4].get_type()
			if(ret_type.ptype == 'Er_type'):
				print 'Type error in function block'
				exit()
			self.sym_table_local = copy.deepcopy(sym_table)	
			sym_table = copy.deepcopy(self.sym_table)
			return TYPE('Success_type', 'Success_type', 0)

		if(self.op == 'classdecl'):
			if(class_def.has_key(self.items[0].items[0])):
				print "Class already declared"
				exit()
			
			last_class_def_name = self.items[0].items[0]
			member_list = []

			class_def[self.items[0].items[0]] = {}
			class_def_order[self.items[0].items[0]] = [] 
			parent_classes[self.items[0].items[0]] = []

			self.items[2].get_memberlist(member_list)

			for element in member_list:
				class_def[self.items[0].items[0]][element[1]] = element[0]
				class_def_order[self.items[0].items[0]].append(element[1])

			cparents = []
			if(self.items[1].op != 'epsilon'):
				parent_classes[self.items[0].items[0]].append(self.items[1].items[0].items[0])
				cparents = copy.deepcopy(parent_classes[self.items[1].items[0].items[0]])
				parent_classes[self.items[0].items[0]] = parent_classes[self.items[0].items[0]] + cparents

			self.items[2].get_funclist()

			last_class_def_name = ''	
 			return TYPE('Success_type', 'Success_type', 0)
		
		if(self.op == '='):
			type1 = self.items[0].get_type()
			type2 = self.items[1].get_type()

			if(type1.ptype == 'Er_type' or type2.ptype == 'Er_type' or type1.dim != type2.dim):
				print 'type Error near ='
				exit()
			else:
				if(type1.ptype != type2.ptype):
					if(not parent_classes.has_key(type2.ptype) or type1.ptype not in parent_classes[type2.ptype]):
						print 'type Error near ='
						exit()
						
				if(type1.atype != type2.atype):
					if(not parent_classes.has_key(type2.atype) or type1.atype not in parent_classes[type2.atype]):
						print 'type Error near ='
						exit()

				self.dtype = type1
				return type1
		
		if(self.op == 'preinc' or self.op == 'postinc' or self.op == 'predec' or self.op == 'postdec'):
			type1 = self.items[0].get_type()
			if(type1.ptype != 'int'):
				print 'pre/post inc/dec doesnt have int type'
				exit()
			else:
				self.dtype = type1
				return type1

		if(self.op == 'bool'):
			self.dtype = TYPE('bool', 'bool', 0)
			return TYPE('bool', 'bool', 0)

		if(self.op == 'intconst'):
			self.dtype = TYPE('int', 'int', 0)
			return TYPE('int', 'int', 0)

		if(self.op == 'void'):
			self.dtype = TYPE('void', 'void', 0)
			return TYPE('void', 'void', 0)

		if(self.op == 'objaccess'):
			ctype = self.items[0].get_type()
			if(ctype.ptype == 'Er_type' or not class_def.has_key(ctype.ptype)):
				print 'object doesnt exist'
				exit()	
			# class name cannot be 'Er_type' or 'array'
			for element in class_def[ctype.ptype].keys():
				if(element == self.items[1].items[0]):
					self.dtype = class_def[ctype.ptype][element]
					return class_def[ctype.ptype][element]

			for pclass in parent_classes[ctype.ptype]:
				for element in class_def[pclass].keys():
					if(element == self.items[1].items[0]):
						self.dtype = class_def[pclass][element]
						return class_def[pclass][element]
					
			print 'Member ', self.items[1].items[0], 'doesnt exist in class'
			exit()	
	
		if(self.op == 'array'):
			idxtype = self.items[1].get_type()
			if(idxtype.ptype != 'int'):
				print 'array index is not integer'
				exit()

			artype = self.items[0].get_type()
			if(artype.ptype != 'array'):
				print 'variable is not array type'
				exit()
			else:
				artype_local = copy.deepcopy(artype)
				artype_local.dim = artype_local.dim - 1
				if(artype_local.dim == 0):
					artype_local.ptype = artype_local.atype
				self.dtype = artype_local
				return artype_local

		if(self.op == 'newobj'):
			if(class_def.has_key(self.items[0].items[0])):
				x = self.items[0].make_type()
				self.dtype = x
				return x
			else:
				print 'class in new() doesnt exist'
				exit()
		
		if(self.op == 'print'):
			argtype = self.items[0].get_type()
			if(argtype.ptype != 'int'):
				print 'print argument is not integer'
				exit()
			else:
				self.dtype =  TYPE('void', 'void', 0)
				return TYPE('void', 'void', 0)

		if(self.op == 'return'):
			result_type = TYPE('Success_type', 'Success_type', 0)
			if(self.items[0].op == 'epsilon'):
				result_type = TYPE('void', 'void', 0)
			else:
				x = self.items[0].get_type()
				if(x.ptype == 'Er_type'):
					print 'return expression is not type-correct'
					exit()
				result_type = copy.deepcopy(x)

			if(result_type.ptype != last_func_type.ptype or result_type.ptype != last_func_type.ptype or result_type.ptype != last_func_type.ptype):
				print 'return type doesnt match with function definition'
				exit()
			else:
				self.dtype = result_type
				return result_type

		if(self.op == 'stmtseq'):
			sttype = TYPE('void', 'void', 0)
			for element in self.items:
				sttype = element.get_type()
				if(sttype.ptype == 'Er_type'):
					print 'statement is not type correct'
					exit()
			return sttype
	
		if(self.op == 'memberdeclstar'):
			for element in self.items:
				if(element.get_type().ptype == 'Er_type'):
					print 'var declaration not type correct'
					exit()
			return TYPE('Success_type', 'Success_type', 0)

		if(self.op == 'blkext'):
			for element in self.items:
				if(element.get_type().ptype == 'Er_type'):
					print 'block is not type correct'
					exit()
			return TYPE('Success_type', 'Success_type', 0)
			
		if(self.op == 'Pgm_blk'):
			self.sym_table = copy.deepcopy(sym_table)
			for element in self.items:
				if(element.get_type().ptype == 'Er_type'):
					print 'program block is not type correct'
					exit()
			self.sym_table_local = copy.deepcopy(sym_table)
			sym_table = copy.deepcopy(self.sym_table)

			return TYPE('Success_type', 'Success_type', 0)

		if(self.op == 'ife'):
			x = self.items[0].get_type()
			if(x.ptype == 'Er_type' or x.ptype != 'bool'):
				print 'either expression in if/else is not boolean or not type correct'
				exit()

			self.sym_table = copy.deepcopy(sym_table)
			x = self.items[1].get_type()
			if(x.ptype == 'Er_type'):
				print 'if block statement is not type correct'
				exit()
			local_sym_table1 = copy.deepcopy(sym_table)
			sym_table = copy.deepcopy(self.sym_table)
			x = self.items[2].get_type()
			if(x.ptype == 'Er_type'):
				print 'else block statement is not type correct'
				exit()
			local_sym_table2 = copy.deepcopy(sym_table)
			sym_table = copy.deepcopy(self.sym_table)
			return TYPE('Success_type', 'Success_type', 0)
								
		if(self.op == 'if'):
			x = self.items[0].get_type()
			if(x.ptype == 'Er_type' or x.ptype != 'bool'):
				print 'either expression in if is not boolean or not type correct'
				exit()

			self.sym_table = copy.deepcopy(sym_table)
			x = self.items[1].get_type()
			if(x.ptype == 'Er_type'):
				print 'if block statement is not type correct'
				exit()
			local_sym_table1 = copy.deepcopy(sym_table)
			sym_table = copy.deepcopy(self.sym_table)
			return TYPE('Success_type', 'Success_type', 0)

		if(self.op == 'while'):
			x = self.items[0].get_type()
			if(x.ptype == 'Er_type' or x.ptype != 'bool'):
				print 'either expression in while is not boolean or not type correct'
				exit()

			self.sym_table = copy.deepcopy(sym_table)
			x = self.items[1].get_type()
			if(x.ptype == 'Er_type'):
				print 'while block statement is not type correct'
				exit()
			local_sym_table1 = copy.deepcopy(sym_table)
			sym_table = copy.deepcopy(self.sym_table)
			return TYPE('Success_type', 'Success_type', 0)

		if(self.op == 'dowhile'):
			self.sym_table = copy.deepcopy(sym_table)
			x = self.items[0].get_type()
			if(x.ptype == 'Er_type'):
				print 'do/while block statement is not type correct'
				exit()

			sym_table = copy.deepcopy(self.sym_table)
			x = self.items[1].get_type()
			if(x.ptype == 'Er_type' or x.ptype != 'bool'):
				print 'either expression in do/while is not boolean or not type correct'
				exit()
			return TYPE('Success_type', 'Success_type', 0)

		if(self.op == 'for'):
			x = self.items[0].get_type()
			if(x.ptype == 'Er_type'):
				print 'for expression 1 is not type correct'
				exit()

			x = self.items[1].get_type()
			if(x.ptype != 'bool'):
				print 'for expression 2 is not boolean'
				exit()

			x = self.items[2].get_type()
			if(x.ptype == 'Er_type'):
				print 'for expression 3 is not type correct'
				exit()

			self.sym_table = copy.deepcopy(sym_table)
			x = self.items[3].get_type()
			if(x.ptype == 'Er_type'):
				print 'for block is not type correct'
				exit()

			sym_table = copy.deepcopy(self.sym_table)
			return TYPE('Success_type', 'Success_type', 0)
			

		if(self.op == '+' or self.op == '-' or self.op == '*' or self.op == '/' or self.op == '%'):
			type1 = self.items[0].get_type()
			type2 = self.items[0].get_type()
			if(type1.ptype != 'int' or type2.ptype != 'int'):
				print 'airthematic binary operator doesnt have both integers'
				exit()
			else:
				self.dtype = type1
				return type1

		if(self.op == '==' or self.op == '!='):
			type1 = self.items[0].get_type()
			type2 = self.items[0].get_type()
			if((type1.ptype == 'int' and type2.ptype == 'int') or (type1.ptype == 'bool' and type2.ptype == 'bool')):
				self.dtype = TYPE('bool','bool', 0)
				return TYPE('bool','bool', 0)
			else:
				print '==/!= dont have same type on both the sides'
				exit()

		if(self.op == '<' or self.op == '>' or self.op == '>=' or self.op == '<='):
			type1 = self.items[0].get_type()
			type2 = self.items[0].get_type()
			if(type1.ptype != 'int' or type2.ptype != 'int'):
				print 'comparison operators doent have integers on both the sides'
				exit()
			else:
				self.dtype = TYPE('bool','bool', 0)
				return TYPE('bool','bool', 0)
		
		if(self.op == 'uminus'):
			type1 = self.items[0].get_type()
			if(type1.ptype != 'int'):
				print 'unary minus doesnt have integer argument'
				exit()
			else:
				self.dtype = TYPE('int','int', 0)
				return TYPE('int','int', 0)
	
		if(self.op == 'not'):
			type1 = self.items[0].get_type()
			if(type1.ptype != 'bool'):
				print 'not operator doesnt have boolean operator'
				exit()
			else:
				self.dtype = TYPE('bool','bool', 0)
				return TYPE('bool','bool', 0)

		if(self.op == '&&' or self.op == '||'):
			type1 = self.items[0].get_type()
			type2 = self.items[0].get_type()
			if(type1.ptype != 'bool' or type2.ptype != 'bool'):
				print '&& / || dont have boolean on both the sides'
				exit()
			else:
				self.dtype = TYPE('bool','bool', 0)	
				return TYPE('bool','bool', 0)
	
		if(self.op == 'funcall'):
		
			fun_name = ''	
			
			type_list = []
			self.items[1].get_typeargs(type_list)

			tname = ''
			for e in type_list:
				if(tname != ''):
					tname = tname + '_' + e.ptype + '_' + e.atype + '_' + str(e.dim)
				else:
					tname = e.ptype + '_' + e.atype + '_' + str(e.dim)
			
			type1 = []
			if(self.items[0].op == 'id'):
				if(tname != ''):
					tname = self.items[0].items[0] + '_' + tname
				else:
					tname = self.items[0].items[0]

				if(not fun_def.has_key(tname)):
					print 'function doesnt exist'
					exit()
				fun_name = tname
			else:
				# get the function name
				fname = self.items[0].items[1].items[0]
				type1 = self.items[0].items[0].get_type()
				if(tname != ''):
					tname = fname + '_' + tname
				else:
					tname = fname
					
				fun_name = type1.ptype + '_' + tname

			not_found = 0
			if(not fun_def.has_key(fun_name)):
				not_found = 1
			else:
				self.fname = fun_name
				return fun_def[fun_name][0]

			if(self.items[0].op == 'id'):
				print 'Didnt find the function definition', self.items[0].items[0]
				exit()
	
			# search through the parent classes
			for pclass in parent_classes[type1.ptype]:
				fun_name = pclass + '_' + tname 

				if(fun_def.has_key(fun_name)):
					self.fname = fun_name
					return fun_def[fun_name][0]
			print 'Didnt find the function definition', tname
			exit()
			
		if(self.op == 'newarray'):
			base_type = self.items[0].make_type()
			x = self.items[1].get_type()
			if(x.ptype == 'Er_type' or x.ptype != 'int'):
				print 'dimension in new [] is not int or not type correct'
				exit()
			
			dim = 1;
			if(self.items[2].op != 'epsilon'):
				temp = self.items[2]
				while(temp.op != 'epsilon'):
					dim = dim + 1
					temp = temp.items[0]
		
			base_type.ptype = 'array'
			base_type.dim = dim
			self.dtype = base_type
			return base_type	

	def get_typeargs(self, type_args):
		if(self.op == 'epsilon'):
			return
		if(self.op == 'args'):
			x = self.items[0].get_type()
			if(x.ptype == 'Er_type'):
				print 'arguments passed in function call are not type correct'
				exit()
			else:
				type_args.append(x)
			if(len(self.items) == 2):
				self.items[1].get_typeargs(type_args)
		else:
			print 'unknown control path. debug.'
			exit()
			
	def get_declared(self, local_declared):
		if(self.op == 'declseq' or self.op == 'Decl' or self.op == 'varlist'):
			for element in self.items:
				element.get_declared(local_declared)
		if(self.op == 'id'):
			# local_declared.append(self.items[0].items[0])
			local_declared.append(self.items[0])

	def gencode(self):
		global sym_table
		global global_table
		global curr_level
		global funcall_called
		global last_class_def_name

		if(self.op == 'intconst'):
			temp = get_temp()
			return [IR.IR_STMT('loadi', temp, 0, self.items[0], 1, None, None, 1)]

		if(self.op == 'id'):
#			temp = get_temp()
			#if(sym_table[self.items[0]][1] == 0):
			temp = self.items[0]
			#else:
			#	print 'this flow shouldnt execute now.'
			#	exit()
			#	temp = self.items[0] + str(sym_table[self.items[0]][1])
			return [IR.IR_STMT('move', temp, 0, temp, 0, None, None, 4)]

		if(self.op == 'epsilon'):
			return []

		if(self.op == 'vardecl'):
			if(curr_level == 0):
				declaredvars = []
				self.items[1].get_declared(declaredvars)
				for element in declaredvars:
					global_table.append(element)
			return []

		if(self.op == 'declseq' or self.op == 'blkext'):
			list_icode = []
			for element in self.items:
				list_icode = list_icode + element.gencode()
			return list_icode

		if(self.op == 'args'):
			list_icode = []
			for element in self.items:
				list_icode = list_icode + element.gencode()
				convert_addr(list_icode)
				if(list_icode[len(list_icode)-1].dest != None):
					x = IR.IR_STMT('push', None, None, list_icode[len(list_icode)-1].dest, 0, None, None, 103)
					list_icode.append(x)
			return list_icode
		
		if(self.op == 'funcall'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			list_icode = []

			x = ''
			if(self.items[0].op != 'id'):
				# A class function
				funcall_called = 1
				list_icode = self.items[0].gencode()
				x = IR.IR_STMT('push', None, None, list_icode[len(list_icode)-1].dest, 0, None, None, 103)
				funcall_called = 0

			fun_name = self.fname
			list_icode.append(IR.IR_STMT('call_start', None, None, None, None, len(fun_def[fun_name][1]), 10, 101))

			if(self.items[0].op != 'id'):
				list_icode.append(x)

			list_icode += self.items[1].gencode()
			op1 = fun_blk_map[fun_name]
			op1type = 0

			return_temp = None
			return_type = None
			
			if(fun_def[self.fname][0].ptype != 'void'):
				return_temp = get_temp()
				return_type = 0
			#if(fun_def[self.items[0].items[0]][0].ptype != 'int' and fun_def[self.items[0].items[0]][0].ptype != 'bool' and fun_def[self.items[0].items[0]][0].ptype != 'void'):
			#	return_type = 2
			list_icode.append(IR.IR_STMT('call_end', return_temp, return_type, op1, op1type, None, None, 102))
			return list_icode
			
		if(self.op == 'fundecl'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			new_bno = ''
			fun_name = self.fname
			if(fun_name == 'main'):
				new_bno = main_blk
			else:
				new_bno = get_new_block()
			if(block_map.has_key(new_bno)):
				print "Couldn't find a new block name.. "
				exit(0)
			fun_blk_map[fun_name] = str(new_bno)

			#self.sym_table_gencode = copy.deepcopy(sym_table)
			#sym_table = copy.deepcopy(self.sym_table_local)
			curr_level = curr_level + 1
			list_icode = []
			typelist = fun_def[fun_name][1]
			for element in reversed(typelist):
				list_icode.append(IR.IR_STMT('pop', None, None, element[1], 0, None, None, 104))
			if(last_class_def_name != ''):
				list_icode.append(IR.IR_STMT('pop', None, None, this_var, 0, None, None, 104))
				
			list_icode.append(IR.IR_STMT('func_start', None, None, None, None, None, None, 105))

			#hack!! beware!! This pointer hack!! GO FIGURE!			
			list_icode.append(IR.IR_STMT('move', this_var, 0, this_var, 0, None, None, 4))
			#hack stops here
			
			list_icode = list_icode + self.items[4].gencode()	
			block_map[new_bno] = list_icode
			#sym_table = copy.deepcopy(self.sym_table_gencode)
			curr_level = curr_level - 1
			return []

		if(self.op == 'return'):
			list_icode = self.items[0].gencode()
			return_type = 0
			if(len(list_icode) != 0):
				convert_addr(list_icode)
				x = IR.IR_STMT('ret', None, None, list_icode[len(list_icode)-1].dest, 0, None, None, 110)
				list_icode.append(x)
			else:
				list_icode.append(IR.IR_STMT('ret', None, None, None, None, None, None, 110))
			return list_icode
			
		if(self.op == 'Pgm_blk'):
			#new_bno = get_new_block()
			#if(block_map.has_key(new_bno)):
			#	print "Couldn't find a new block name.. "
			#	exit(0)
			#block_map[new_bno] = self.items[1].gencode()
			#return [IR.IR_STMT('new block', new_bno, None, None, None, None, 50)]
			
			#self.s_table = copy.deepcopy(s_table)
			#local_declared = []
			#self.items[0].get_declared(local_declared)
			#for element in local_declared:
			#	if(s_table.has_key(element)):
			#		print 'this renaming should not happen now'
			#		exit()
			#		s_table[element] = s_table[element] + 1 
			#	else:
			#		s_table[element] = 0
			#print "in pgm_blk", local_declared, s_table
			curr_level = curr_level + 1
			list_icode = self.items[0].gencode()
			#s_table = copy.deepcopy(self.s_table)
			curr_level = curr_level - 1
			return list_icode

		if(self.op == 'stmtseq'):
			list_icode = []
			for element in self.items:
				list_icode = list_icode + element.gencode()
			return list_icode
				
		if(self.op == 'classdecl'):
			last_class_def_name = self.items[0].items[0]
			curr_level = curr_level + 1
			list_icode = self.items[2].gencode()
			curr_level = curr_level - 1
			last_class_def_name = ''
			return []

		if(self.op == 'memberdeclstar'):
			list_icode = []
			for element in self.items:
				list_icode = element.gencode()
			return []

		if(self.op == 'print'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			opcode = 'print'
			icode_list = self.items[0].gencode()
			if(len(icode_list) == 0):
				print "SEE THIS CASE.." # an id.
				op1 = self.items[0].items[0]
			else:
				convert_addr(icode_list)
				op1 = icode_list[len(icode_list)-1].dest
				op1type = 0
			optype = 2
			x = IR.IR_STMT(opcode, dest, 0, op1, op1type, op2, op2type, optype)
			icode_list.append(x)
			return icode_list

		if(self.op == '='):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
                        if(self.items[0].op == 'id'):
				#if(sym_table[self.items[0].items[0]][1] == 0):
				dest = self.items[0].items[0]
				#else:
				#	print 'this flow should not be reached'
				#	exit()
                                #	dest = self.items[0].items[0] + str(sym_table[self.items[0].items[0]][1])
#                                if(self.items[1].op == 'new'):
#                                        opcode = 'alloc'
#                                        list_icode = self.items[1].gencode()
#				
#					convert_addr(list_icode)	
#                                        op1 = list_icode[len(list_icode)-1].dest
#                                        op1type = 0
#                                        list_icode.append(IR.IR_STMT(opcode, dest, 2, op1, op1type, None, None, 60));
#                                else:
				list_icode = self.items[1].gencode()
				opcode = '='
				convert_addr(list_icode)	
				op1 = list_icode[len(list_icode)-1].dest
				op1type = 0
				list_icode.append(IR.IR_STMT(opcode, dest, 0, op1, op1type, None, None, 4));
                        else:
#                                if(self.items[1].op == 'new'):
#                                        opcode = 'alloc'
#                                        list_icode = self.items[1].gencode()
#					
#					convert_addr(list_icode)	
#                                        op1 = list_icode[len(list_icode)-1].dest
#                                        op1type = 0
#                                	list_icode = list_icode + self.items[0].gencode()
#                                	dest = list_icode[len(list_icode)-1].dest
#					desttype = list_icode[len(list_icode)-1].desttype
#          
#					if(desttype != 2):
#						print "Something wrong with desttype"
#
#					temp_var = get_temp()
#					list_icode.append(IR.IR_STMT(opcode, temp_var, 2, op1, op1type, None, None, 60));
#					opcode = 'store'
#					list_icode.append(IR.IR_STMT(opcode, dest, desttype, temp_var, 2, None, None, 61));
#				else:
				list_icode = self.items[1].gencode()
				convert_addr(list_icode)  
				op1 = list_icode[len(list_icode)-1].dest
				op1type = 0
				list_icode = list_icode + self.items[0].gencode()
				dest = list_icode[len(list_icode)-1].dest
				desttype = list_icode[len(list_icode)-1].desttype
				#if(desttype != 2):
				#	print "Something wrong with desttype"
				opcode = 'store'
				list_icode.append(IR.IR_STMT(opcode, dest, desttype, op1, op1type, None, None, 61));

			return list_icode

		if(self.op == 'this' or self.op == 'super'):
			temp = get_temp()
			return [IR.IR_STMT('move', temp, 0, this_var, 0, None, None, 4)]
 
		if(self.op == 'objaccess'):
                        opcode = dest = op1 = op1type = op2 = op2type = optype = None
                        list_icode = []
                        dest = get_temp()
                        if(self.items[0].op == 'id'):
				op1 = self.items[0].items[0]
                        	op1type = 2 
				if(funcall_called):
					list_icode.append(IR.IR_STMT('move', dest, 0, op1, 0, None, None, 4))
					return list_icode

				ctype_list = class_def_order[self.items[0].dtype.ptype]
				
				found = 0
				offset = 0

				cur_class = 0
 				par_class = 0

				for iter in range(0, len(ctype_list)):
					if(ctype_list[iter] == self.items[1].items[0]):
						offset = iter
						cur_class = 1 
						found = 1

				if(found == 0):
					#search in the parent classes
					piter = 0
					pciter = 0
					while(found == 0 and piter < len(parent_classes[self.items[0].dtype.ptype])):
						pctype_list = class_def_order[parent_classes[self.items[0].dtype.ptype][piter]]
						while(found == 0 and pciter < len(pctype_list)):
							if(pctype_list[pciter] == self.items[1].items[0]):
								found = 1
							pciter += 1

						if(found == 1):
							par_class = piter
							offset = (pciter - 1)
						piter += 1

				# Adust the offset now according to the layout.
				start_offset = 0
				if(cur_class != 0):
					for each_class in parent_classes[self.items[0].dtype.ptype]:
						start_offset += len(class_def_order[each_class])
					start_offset += offset
				else:
					for each_class in parent_classes[self.items[0].dtype.ptype][par_class+1:]:
						start_offset += len(class_def_order[each_class])
					start_offset += offset
							
				temp = get_temp()
				list_icode.append(IR.IR_STMT('loadi', temp, 0, start_offset, 1, None, None, 1))
                                op2 = temp
				op2type = 0
				#list_icode.append(IR.IR_STMT('oload', dest, 2, op1, op1type, op2, op2type, 100))
				list_icode.append(IR.IR_STMT('oload', dest, 2, op1, op1type, op2, op2type, 55))
			else:
                                list_icode = self.items[0].gencode()
				#print self.items[0].op
				convert_addr(list_icode)  
                                op1 = list_icode[len(list_icode)-1].dest
                                op1type = list_icode[len(list_icode)-1].desttype
				
				if(funcall_called):
					return list_icode

				cname = ''

				if(op1type != 0):
					print "Something wrong with desttype"
				# op1 has the addr
				#print self.items[0]
				found = 0
				offset = 0

				cur_class = 0
 				par_class = 0
				
				if(self.items[0].op == 'this'):
					ctype_list = class_def_order[last_class_def_name]
					cname = last_class_def_name
				else:
					if(self.items[0].op == 'super'):
						ctype_list = class_def_order[parent_classes[last_class_def_name][0]]
						#offset = len(class_def_order[last_class_def_name])
						cname = parent_classes[last_class_def_name][0]
					else:
						ctype_list = class_def_order[self.items[0].dtype.ptype]
						cname = self.items[0].dtype.ptype
					
				for iter in range(0, len(ctype_list)):
					if(ctype_list[iter] == self.items[1].items[0]):
							found = 1
							cur_class = 1
							offset = iter
				
				if(found == 0):
					#search in the parent classes
					piter = 0
					pciter = 0
					while(found == 0 and piter < len(parent_classes[cname])):
						pctype_list = class_def_order[parent_classes[cname][piter]]
						while(found == 0 and pciter < len(pctype_list)):
							if(pctype_list[pciter] == self.items[1].items[0]):
								found = 1
							pciter += 1

						if(found == 1):
							par_class = piter
							offset = (pciter - 1)
						piter += 1
			
				# Adust the offset now according to the layout.
				start_offset = 0
				if(cur_class != 0):
					for each_class in parent_classes[cname]:
						start_offset += len(class_def_order[each_class])
					start_offset += offset
				else:
					for each_class in parent_classes[cname][par_class + 1:]:
						start_offset += len(class_def_order[each_class])
					start_offset += offset
							
				op2 = get_temp()
				list_icode.append(IR.IR_STMT('loadi', op2, 0, start_offset, 1, None, None, 1))
				op2type = 0
				list_icode.append(IR.IR_STMT('oload', dest, 2, op1, op1type, op2, op2type, 55))
			return list_icode	
				 	
		if(self.op == 'array'):
                        opcode = dest = op1 = op1type = op2 = op2type = optype = None
                        list_icode = []
                        dest = get_temp()
                        if(self.items[0].op == 'id'):
				#if(sym_table[self.items[0].items[0]][1] == 0):
				op1 = self.items[0].items[0]
				#else:
				#	op1 = self.items[0].items[0] + str(sym_table[self.items[0].items[0]][1])
                                op1type = 2 
                                list_icode = self.items[1].gencode()
					
				convert_addr(list_icode)	
                                op2 = list_icode[len(list_icode)-1].dest;
                                op2type = 0
                                # load the size at op1 location
				#temp_size = get_temp()
				list_icode.append(IR.IR_STMT('deref',dest, 0, op1, op1type, None, None, 90))
				list_icode.append(IR.IR_STMT('<=', dest, 0, dest, 0, op2, 0, 0))
				list_icode.append(IR.IR_STMT('exit', None, 0, dest, 0, None, None, 81))
				list_icode.append(IR.IR_STMT('loadi', dest, 0, 0, 1, None, None, 1))
				list_icode.append(IR.IR_STMT('<', dest, 0, op2, 0, dest, 0, 0))
				list_icode.append(IR.IR_STMT('exit', None, 0, dest, 0, None, None, 81))
                                # load the addr in dest
                                list_icode.append(IR.IR_STMT('aload', dest, 2, op1, op1type, op2, op2type, 55))
                        else:
                                list_icode = self.items[0].gencode()
				convert_addr(list_icode)  
                                op1 = list_icode[len(list_icode)-1].dest
                                op1type = list_icode[len(list_icode)-1].desttype
 
				if(op1type != 0):
					print "Something wrong with desttype"
				# op1 has the addr
				list_icode = list_icode + self.items[1].gencode()
				convert_addr(list_icode)  
				op2 = list_icode[len(list_icode)-1].dest;
				op2type = 0
                                # load the size at op1 location
				#temp_size = get_temp()
				list_icode.append(IR.IR_STMT('deref',dest, 0, op1, op1type, None, None, 90))
				list_icode.append(IR.IR_STMT('<=', dest, 0, dest, 0, op2, 0, 0))
				list_icode.append(IR.IR_STMT('exit', None, 0, dest, 0, None, None, 81))
				list_icode.append(IR.IR_STMT('loadi', dest, 0, 0, 1, None, None, 1))
				list_icode.append(IR.IR_STMT('<', dest, 0, op2, 0, dest, 0, 0))
				list_icode.append(IR.IR_STMT('exit', None, 0, dest, 0, None, None, 81))
				# dest has the addr
				list_icode.append(IR.IR_STMT('aload', dest, 2, op1, op1type, op2, op2type, 55))
			return list_icode

		if(self.op == 'postinc'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			
			icode_list = self.items[0].gencode()
			if(len(icode_list) == 0):
				print "SEE THIS CASE.." # an id.
				op1 = self.items[0].items[0]
				op1type = 0
			else:
				op1 = icode_list[len(icode_list)-1].dest
				op1type = icode_list[len(icode_list)-1].desttype

			if(op1type != 2):
				temp = get_temp()
				icode_list.append(IR.IR_STMT('move', temp, 0, op1, op1type, None, None, 4))
				temp1 = get_temp()
				icode_list.append(IR.IR_STMT('loadi', temp1, 0, 1, 1, None, None, 1))
				opcode = '+'
				optype = 0 #arithmetic
				x = IR.IR_STMT(opcode, op1, 0, temp1, 0, op1, 0, optype)
				icode_list.append(x)
				icode_list.append(IR.IR_STMT('move', temp, 0, temp, 0, None, None, 4))
			else:
				temp = get_temp()
				icode_list.append(IR.IR_STMT('deref',temp, 0, op1, op1type, None, None, 90))
				temp1 = get_temp()
				icode_list.append(IR.IR_STMT('loadi', temp1, 0, 1, 1, None, None, 1))
				opcode = '+'
				optype = 0 #arithmetic
				x = IR.IR_STMT(opcode, temp1, 0, temp1, 0, temp, 0, optype)
				icode_list.append(x)
                                icode_list.append(IR.IR_STMT('store', op1, op1type, temp1, 0, None, None, 61));
				icode_list.append(IR.IR_STMT('move', temp, 0, temp, 0, None, None, 4))
				
			return icode_list
					
		if(self.op == 'preinc'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			
			icode_list = self.items[0].gencode()
			if(len(icode_list) == 0):
				print "SEE THIS CASE.." # an id.
				op1 = self.items[0].items[0]
				op1type = 0
			else:
				op1 = icode_list[len(icode_list)-1].dest
				op1type = icode_list[len(icode_list)-1].desttype

			if(op1type != 2):
				temp = get_temp()
				icode_list.append(IR.IR_STMT('loadi', temp, 0, 1, 1, None, None, 1))
				opcode = '+'
				optype = 0 #arithmetic
				x = IR.IR_STMT(opcode, op1, 0, temp, 0, op1, 0, optype)
				icode_list.append(x)
			else:
				temp = get_temp()
				icode_list.append(IR.IR_STMT('deref',temp, 0, op1, op1type, None, None, 90))
				temp1 = get_temp()
				icode_list.append(IR.IR_STMT('loadi', temp1, 0, 1, 1, None, None, 1))
				opcode = '+'
				optype = 0 #arithmetic
				x = IR.IR_STMT(opcode, temp, 0, temp1, 0, temp, 0, optype)
				icode_list.append(x)
                                icode_list.append(IR.IR_STMT('store', op1, op1type, temp, 0, None, None, 61));
				icode_list.append(IR.IR_STMT('move', temp, 0, temp, 0, None, None, 4))
			return icode_list
					
		if(self.op == 'postdec'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			
			icode_list = self.items[0].gencode()
			if(len(icode_list) == 0):
				print "SEE THIS CASE.." # an id.
				op1 = self.items[0].items[0]
				op1type = 0
			else:
				op1 = icode_list[len(icode_list)-1].dest
				op1type = icode_list[len(icode_list)-1].desttype

			if(op1type != 2):
				temp = get_temp()
				icode_list.append(IR.IR_STMT('move', temp, 0, op1, op1type, None, None, 4))
				temp1 = get_temp()
				icode_list.append(IR.IR_STMT('loadi', temp1, 0, 1, 1, None, None, 1))
				opcode = '-'
				optype = 0 #arithmetic
				x = IR.IR_STMT(opcode, op1, 0, op1, 0, temp1, 0, optype)
				icode_list.append(x)
				icode_list.append(IR.IR_STMT('move', temp, 0, temp, 0, None, None, 4))
			else:
				temp = get_temp()
				icode_list.append(IR.IR_STMT('deref',temp, 0, op1, op1type, None, None, 90))
				temp1 = get_temp()
				icode_list.append(IR.IR_STMT('loadi', temp1, 0, 1, 1, None, None, 1))
				opcode = '-'
				optype = 0 #arithmetic
				x = IR.IR_STMT(opcode, temp1, 0, temp, 0, temp1, 0, optype)
				icode_list.append(x)
                                icode_list.append(IR.IR_STMT('store', op1, op1type, temp1, 0, None, None, 61));
				icode_list.append(IR.IR_STMT('move', temp, 0, temp, 0, None, None, 4))

			return icode_list
					
		if(self.op == 'predec'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			
			icode_list = self.items[0].gencode()
			if(len(icode_list) == 0):
				print "SEE THIS CASE.." # an id.
				op1 = self.items[0].items[0]
				op1type = 0
			else:
				op1 = icode_list[len(icode_list)-1].dest
				op1type = icode_list[len(icode_list)-1].desttype

			if(op1type != 2):
				temp = get_temp()
				icode_list.append(IR.IR_STMT('loadi', temp, 0, 1, 1, None, None, 1))
				opcode = '-'
				optype = 0 #arithmetic
				x = IR.IR_STMT(opcode, op1, 0, op1, 0, temp, 0, optype)
				icode_list.append(x)
			else:
				temp = get_temp()
				icode_list.append(IR.IR_STMT('deref',temp, 0, op1, op1type, None, None, 90))
				temp1 = get_temp()
				icode_list.append(IR.IR_STMT('loadi', temp1, 0, 1, 1, None, None, 1))
				opcode = '-'
				optype = 0 #arithmetic
				x = IR.IR_STMT(opcode, temp, 0, temp, 0, temp1, 0, optype)
				icode_list.append(x)
                                icode_list.append(IR.IR_STMT('store', op1, op1type, temp, 0, None, None, 61));
				icode_list.append(IR.IR_STMT('move', temp, 0, temp, 0, None, None, 4))
			return icode_list
					
		if(self.op == 'input'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			opcode = 'input'
			dest = get_temp()
			optype = 3
			return [IR.IR_STMT(opcode, dest, 0, op1, op1type, op2, op2type, optype)]

		if(self.op == 'uminus' or self.op == 'not'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			opcode = self.op
			list_icode = self.items[0].gencode()
			if(len(list_icode) == 0):
				op1 = self.items[0].items[0]
			else:
				convert_addr(list_icode)
				op1 = list_icode[len(list_icode)-1].dest
			op1type = 0
			dest = get_temp()
			optype = 0
			x = IR.IR_STMT(opcode, dest, 0, op1, op1type, op2, op2type, optype)
			list_icode.append(x)
			return list_icode

		if(self.op == '+' or self.op == '-' or self.op == '*' or self.op == '/' or self.op == '%' or self.op == '==' or self.op == '!=' or self.op == '<' or self.op == '>' or self.op == '>=' or self.op == '<='):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			dest = get_temp()
			list_icode1 = self.items[1].gencode()
			if(len(list_icode1) == 0):
				op2 = self.items[1].items[0]
			else:
				convert_addr(list_icode1)
				op2 = list_icode1[len(list_icode1)-1].dest
			op2type = 0
			list_icode2 = self.items[0].gencode()
			if(len(list_icode2) == 0):
				op1 = self.items[0].items[0]
			else:
				convert_addr(list_icode2)
				op1 = list_icode2[len(list_icode2)-1].dest

			op1type = 0
			opcode = self.op
			optype = 0 #arithmetic

			list_icode1 = list_icode1 + list_icode2
			x = IR.IR_STMT(opcode, dest, 0, op1, op1type, op2, op2type, optype)
			list_icode1.append(x)
			return list_icode1

		if(self.op == 'bool'):
			if(self.items[0] == 'true'):	
				temp = get_temp()
				list_icode = [IR.IR_STMT('loadi', temp, 0, 1, 1, None, None, 1)]
			else:
				temp = get_temp()
				list_icode = [IR.IR_STMT('loadi', temp, 0, 0, 1, None, None, 1)]
			return list_icode

		if(self.op == 'newobj'):
			list_icode = []
			op1 = self.items[0].items[0]
			op1type = 0

			dim = len(class_def_order[op1]);
			for iter in parent_classes[op1]:
				dim += len(class_def_order[iter])

			if(dim == 0):
				op2 = None
				op2type = None
			else:
				op2 = dim	
				op2type = 1
			dest = get_temp()
			opcode = 'alloc'
			optype = 60
			temp = get_temp()
			list_icode.append(IR.IR_STMT('loadi', temp, 0, op2, 1, None, None, 1))
			x = IR.IR_STMT(opcode, dest, 0, temp, 0, op1, op1type, optype)
			list_icode.append(x)
			return list_icode

		if(self.op == 'newarray'):
			list_icode = self.items[1].gencode()
			convert_addr(list_icode)
			op1 = list_icode[len(list_icode)-1].dest
			op1type = 0

#			if(self.items[0].op == 'id'):
#				class_size = len(class_def[self.items[0].items[0]])
#				temp_var = get_temp()
#				list_icode.append(IR.IR_STMT('loadi', temp_var, 0, class_size, 1, None, None, 1))
#				list_icode.append(IR.IR_STMT('*', op1, 0, op1, 0, temp_var, 0, 0))
			
			temp = self.items[2]
			dim = 0;
			while(temp.op != 'epsilon'):
				dim = dim + 1
				temp = temp.items[0]

			if(dim == 0):
				op2 = None
				op2type = None
			else:
				op2 = dim	
				op2type = 1

			dest = get_temp()
			opcode = 'alloc'
			optype = 60
			x = IR.IR_STMT(opcode, dest, 0, op1, op1type, op2, op2type, optype)
			list_icode.append(x)
			return list_icode

		if(self.op == '&&'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			list_icode = self.items[0].gencode()
			if(len(list_icode) == 0):
				op1 = self.items[0].items[0]
			else:
				convert_addr(list_icode)
				op1 = list_icode[len(list_icode)-1].dest
	
			op1type = 0

			dest = get_temp()
			list_icode.append(IR.IR_STMT('move', dest, 0, op1, op1type, None, None, 4))
	
			new_bno = get_new_block()
			if(block_map.has_key(new_bno)):
				print "Couldn't find a new block name.. "
				exit(0)
			block_map[new_bno] = self.items[1].gencode()
			if(len(block_map[new_bno]) == 0):
				block_map[new_bno] = [IR.IR_STMT('move', dest, 0, self.items[1][0], 0, None, None, 4)]
			else:
				block_map[new_bno].append(IR.IR_STMT('move', dest, 0, block_map[new_bno][len(block_map[new_bno])-1].dest, 0, None, None, 4))
			opcode = 'if'
			optype = 8 

			x = IR.IR_STMT(opcode, new_bno, 0, op1, op1type, None, None, optype)
			list_icode.append(x)
			list_icode.append(IR.IR_STMT('move', dest, 0, dest, 0, None, None, 4))
			return list_icode
				
		if(self.op == '||'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			list_icode = self.items[0].gencode()
			if(len(list_icode) == 0):
				op1 = self.items[0].items[0]
			else:
				convert_addr(list_icode)
				op1 = list_icode[len(list_icode)-1].dest
	
			op1type = 0
	
			dest = get_temp()

			new_bno = get_new_block()
			if(block_map.has_key(new_bno)):
				print "Couldn't find a new block name.. "
				exit(0)
			block_map[new_bno] = self.items[1].gencode()
			if(len(block_map[new_bno]) == 0):
				block_map[new_bno] = [IR.IR_STMT('move', dest, 0, self.items[1][0], 0, None, None, 4)]
			else:
				block_map[new_bno].append(IR.IR_STMT('move', dest, 0, block_map[new_bno][len(block_map[new_bno])-1].dest, 0, None, None, 4))

			op2 = new_bno
			op2type = 3 
			opcode = 'ife'
			optype = 7

			new_bno1 = get_new_block()
			if(block_map.has_key(new_bno1)):
				print "Couldn't find a new block name.. "
				exit(0)
			block_map[new_bno1] = [IR.IR_STMT('move', dest, 0, op1, 0, None, None, 4)]

			x = IR.IR_STMT(opcode, new_bno1, 0, op1, op1type, op2, op2type, optype)
			list_icode.append(x)
			list_icode.append(IR.IR_STMT('move', dest, 0, dest, 0, None, None, 4))
			return list_icode

					
		if(self.op == 'ife'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			list_icode = self.items[0].gencode()
			if(len(list_icode) == 0):
				op1 = self.items[0].items[0]
			else:
				convert_addr(list_icode)
				op1 = list_icode[len(list_icode)-1].dest

			op1type = 0
			op2 = None
			opcode = self.op
			optype = 7

			#hack: put else label in op2
			new_bno = get_new_block()
			if(block_map.has_key(new_bno)):
				print "Couldn't find a new block name.. "
				exit(0)

			block_map[new_bno] = self.items[1].gencode()
			#print new_bno, "   ", block_map[new_bno]
			dest = new_bno

			new_bno1 = get_new_block()
			if(block_map.has_key(new_bno1)):
				print "Couldn't find a new block name.. "
				exit(0)

			block_map[new_bno1] = self.items[2].gencode()
			#print new_bno1, "   ", block_map[new_bno1]
			op2 = new_bno1
			op2type = 3
			 
			x = IR.IR_STMT(opcode, dest, 0, op1, op1type, op2, op2type, optype)
			list_icode.append(x)
			return list_icode
								
		if(self.op == 'if'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			list_icode = self.items[0].gencode()
			if(len(list_icode) == 0):
				op1 = self.items[0].items[0]
			else:
				convert_addr(list_icode)
				op1 = list_icode[len(list_icode)-1].dest
			op2 = None
			opcode = self.op
			optype = 8
			op1type = 0

			new_bno = get_new_block()
			if(block_map.has_key(new_bno)):
				print "Couldn't find a new block name.. "
				exit(0)

			block_map[new_bno] = self.items[1].gencode()
			#print new_bno, "   ", block_map[new_bno]
			dest = new_bno

			x = IR.IR_STMT(opcode, dest, 0, op1, op1type, op2, op2type, optype)
			list_icode.append(x)
			return list_icode

		if(self.op == 'while'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			new_bno = get_new_block()
			if(block_map.has_key(new_bno)):
				print "Couldn't find a new block name.. "
				exit(0)

			block_map[new_bno] = self.items[0].gencode()
			if(len(block_map[new_bno]) == 0):
				op1 = self.items[0].items[0]
			else:
				convert_addr(block_map[new_bno])
				op1 = block_map[new_bno][len(block_map[new_bno]) - 1].dest
			op1type = 0
			list_icode = []
			op2 = new_bno 
			op2type = 3
			opcode = self.op
			optype = 12

			new_bno1 = get_new_block()
			if(block_map.has_key(new_bno1)):
				print "Couldn't find a new block name.. "
				exit(0)

			block_map[new_bno1] = self.items[1].gencode()
			block_map[new_bno1].append(IR.IR_STMT('loop_end',new_bno, 0, None,None,None,None,11))
			#block_map[new_bno].append(IR.IR_STMT(opcode, new_bno1, op1, op1type, op2, op2type, optype))
			block_map[new_bno].append(IR.IR_STMT(opcode, new_bno1, 0, op1, op1type, None, None, optype))

			list_icode.append(IR.IR_STMT('whilejmp',new_bno, 0, None,None,None,None,9))
			return list_icode

		if(self.op == 'for'):
			list_icode = []
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			new_bno = get_new_block()
			if(block_map.has_key(new_bno)):
				print "Couldn't find a new block name.. "
				exit(0)
			list_icode = self.items[0].gencode()

			block_map[new_bno] = self.items[1].gencode()
			if(len(block_map[new_bno]) == 0):
				op1 = self.items[0].items[0]
			else:
				convert_addr(block_map[new_bno])
				op1 = block_map[new_bno][len(block_map[new_bno]) - 1].dest
			op1type = 0
			opcode = self.op
			optype = 12
			op2 = new_bno 	
			op2type = 3
			
			new_bno1 = get_new_block()
			if(block_map.has_key(new_bno1)):
				print "Couldn't find a new block name.. "
				exit(0)

			block_map[new_bno1] = self.items[3].gencode()
			block_map[new_bno1] = block_map[new_bno1] + self.items[2].gencode()

			block_map[new_bno].append(IR.IR_STMT(opcode, new_bno1, 0, op1, op1type, op2, op2type, optype))

			block_map[new_bno1].append(IR.IR_STMT('loop_end',new_bno, 0,None,None,None,None,11))
			list_icode.append(IR.IR_STMT('forjmp',new_bno,0,None,None,None,None,9))
			return list_icode

		if(self.op == 'dowhile'):
			list_icode = []
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			
			new_bno = get_new_block()
			if(block_map.has_key(new_bno)):
				print "Couldn't find a new block name.. "
				exit(0)
			
			list_icode.append(IR.IR_STMT('jmp',new_bno,0,None,None,None,None,10))
			block_map[new_bno] = self.items[0].gencode()
			
			new_bno1 = get_new_block()
			if(block_map.has_key(new_bno1)):
				print "Couldn't find a new block name.. "
				exit(0)
			
			block_map[new_bno].append(IR.IR_STMT('whilejmp',new_bno1,0,None,None,None,None,9))
			block_map[new_bno1] = self.items[1].gencode()
			
			new_bno2 = get_new_block()
			if(block_map.has_key(new_bno2)):
				print "Couldn't find a new block name.. "
				exit(0)
			
			block_map[new_bno2] = self.items[0].gencode()
			block_map[new_bno2].append(IR.IR_STMT('loop_end',new_bno1,0,None,None,None,None,11))
			
			if(len(block_map[new_bno1]) == 0):
				op1 = self.items[0].items[0]
			else:
				convert_addr(block_map[new_bno1])
				op1 = block_map[new_bno1][len(block_map[new_bno1]) - 1].dest
			op1type = 0
			dest = new_bno2
			opcode = self.op
			optype = 12 
			block_map[new_bno1].append(IR.IR_STMT(opcode, dest, 0,op1, op1type, new_bno1, 3, optype))
			return list_icode

def p_pgm(p):
	'Pgm : DeclSeq'
	p[0] = p[1]

def p_declseq_decl(p):
	'DeclSeq : Decl DeclSeq'
	p[0] = NODE('declseq', [p[1],p[2]])

def p_declseq_null(p):
	'DeclSeq : '
	p[0] = NODE('epsilon', [])

def p_decl(p):
	'''Decl : VarDecl
	        | FunDecl
	        | ClassDecl'''
	p[0] = p[1]

def p_vardecl(p):
	'VarDecl : Type VarList SCOLON'
	p[0] = NODE('vardecl', [p[1], p[2]])

def p_fundecl(p):
	'FunDecl : Type ID DimStar LOPEN FormalsOpt ROPEN Stmt'
	x = NODE('id', [p[2]])
	p[0] = NODE('fundecl', [p[1], x, p[3], p[5], p[7]])

def p_fundecl_formalopt(p):
	'FormalsOpt : Formals'
	p[0] = p[1]

def p_fundecl_formalopt_r(p):
	'FormalsOpt : '
	p[0] = NODE('epsilon',[])

def p_classdecl(p):
	'ClassDecl : CLASS ID ExtendsOpt LCURL MemberDeclstar RCURL'
	x = NODE('id', [p[2]])
	p[0] = NODE('classdecl', [x, p[3], p[5]])

def p_extendopt(p):
	'ExtendsOpt : Extends'
	p[0] = p[1]

def p_extendnull(p):
	'ExtendsOpt : '
	p[0] = NODE('epsilon', [])

def p_extends(p):
	'Extends : EXTENDS ID'
	p[0] = NODE('extends', [NODE('id', [p[2]])])

def p_MemberDeclstar(p):
	'MemberDeclstar : MemberDecl MemberDeclstar'
	p[0] = NODE('memberdeclstar', [p[1],p[2]])

def p_MemberDeclstar_n(p):
	'MemberDeclstar : '
	p[0] = NODE('epsilon', [])

def p_MemberDecl(p):
	'''MemberDecl : VarDecl
		      | FunDecl'''
	p[0] = p[1]
	
def p_type_int(p):
	'Type : INT'
	p[0] = NODE('int', [p[1]])

def p_type_bool(p):
	'Type : BOOL'
	p[0] = NODE('bool', [p[1]])

def p_type_id(p):
	'Type : ID'
	p[0] = NODE('id', [p[1]])

def p_type_void(p):
	'Type : VOID'
	p[0] = NODE('void', [p[1]])

def p_varlist_r(p):
	'VarList : ID DimStar COMA VarList'
	x = NODE('id', [p[1]])
	p[0] = NODE('varlist', [x, p[2], p[4]])

def p_varlist(p):
	'VarList : ID DimStar'
	x = NODE('id', [p[1]])
	p[0] = NODE('varlist', [x, p[2]])

def p_formals_r(p):
	'Formals : Type ID DimStar COMA Formals'
	x = NODE('id', [p[2]])
	p[0] = NODE('formals', [p[1], x, p[3], p[5]])

def p_formals(p):
	'Formals : Type ID DimStar'
	x = NODE('id', [p[2]])
	p[0] = NODE('formals', [p[1], x, p[3]])

def p_stmt_r(p):
	'Stmtseq : Stmt Stmtseq'
	p[0] = NODE('stmtseq', [p[1],p[2]])

def p_stmt_e(p):
	'Stmtseq : '
	p[0] = NODE('epsilon', [])

def p_stmt_se(p):
	'Stmt : SE SCOLON'
	p[0] = p[1]

def p_stmt_print(p):
	'Stmt : PRINT LOPEN AE ROPEN SCOLON'
	p[0] = NODE('print',[p[3]])

def p_stmt_blk(p):
	'Stmt : LCURL Blkext RCURL'
	p[0] = NODE('Pgm_blk',[p[2]])

def p_stmt_blkext(p):
	'Blkext : VarDecl Blkext'
	p[0] = NODE('blkext',[p[1],p[2]])

def p_stmt_blkext_stmt(p):
	'Blkext : Stmtseq'
	p[0] = p[1]

def p_stmt_ife(p):
	'Stmt : IF AE THEN Stmt ELSE Stmt'
	p[0] = NODE('ife', [p[2],p[4],p[6]])

def p_stmt_if(p):
	'Stmt : IF AE THEN Stmt'
	p[0] = NODE('if', [p[2],p[4]])

def p_stmt_while(p):
	'Stmt : WHILE AE DO Stmt'
	p[0] = NODE('while', [p[2],p[4]])

def p_stmt_dowhile(p):
	'Stmt : DO Stmt WHILE AE SCOLON'
	p[0] = NODE('dowhile', [p[2],p[4]])

def p_aeopt_ae(p):
	'AEOpt : AE'
	p[0] = p[1]

def p_aeopt_null(p):
	'AEOpt : '
	p[0] = NODE('epsilon',[])

def p_stmt_for(p):
	'Stmt : FOR LOPEN SEOpt SCOLON AEOpt SCOLON SEOpt ROPEN Stmt'
	p[0] = NODE('for', [p[3],p[5],p[7],p[9]])

def p_stmt_return(p):
	'Stmt : RETURN AEOpt SCOLON'
	p[0] = NODE('return', [p[2]])

def p_seopt_se(p):
	'SEOpt : SE'
	p[0] = p[1]

def p_seopt_null(p):
	'SEOpt : '
	p[0] = NODE('epsilon',[])

def p_se_assign(p):
	'SE : Lhs ASSIGN AE'
	p[0] = NODE('=', [p[1],p[3]])

def p_se_lhsp(p):
	'SE : Lhs INC'
	p[0] = NODE('postinc', [p[1]])
 
def p_se_lhsd(p):
	'SE : Lhs DEC'
	p[0] = NODE('postdec', [p[1]])

def p_se_lhsppre(p):
	'SE : INC Lhs'
	p[0] = NODE('preinc', [p[2]])
 
def p_se_lhsdpre(p):
	'SE : DEC Lhs'
	p[0] = NODE('predec', [p[2]])

def p_lhs_access(p):
	'''Lhs : FieldAccess
	       | ArrayAccess'''
	p[0] = p[1]

def p_ae_bin(p):
	'''AE : AE ADD AE
	      | AE SUB AE
	      | AE MUL AE
	      | AE DIV AE
	      | AE MOD AE
	      | AE AND AE
	      | AE OR AE
	      | AE EQ AE
	      | AE NEQ AE
	      | AE LT AE
	      | AE GT AE
	      | AE GTEQ AE
	      | AE LTEQ AE'''
	p[0] = NODE(p[2], [p[1],p[3]])
	
def p_ae_un(p):
	'''AE : SUB AE %prec UMINUS
	      | NOT AE'''
	if(p[1] == '-'):
		p[0] = NODE('uminus', [p[2]])
	else:
		p[0] = NODE('not', [p[2]])
		
def p_prim_open(p):
	'Primary : LOPEN AE ROPEN'
	p[0] = p[2]

def p_prim_int(p):
	'Primary : INTCONST'
	p[0] = NODE('intconst',[p[1]])

def p_prim_this(p):
	'''Primary : THIS
		   | SUPER'''
	p[0] = NODE(p[1],[p[1]])

def p_ae_se(p):
	'''AE : SE
	      | Primary
	      | NewArray'''
	p[0] = p[1]

def p_prim_input(p):
	'Primary : INPUT LOPEN ROPEN'
	p[0] = NODE('input', []);

def p_prim_tf(p):
	'''Primary : TRUE
		   | FALSE'''
	p[0] = NODE('bool',[p[1]])

def p_prim_obj(p):
	'''Primary : FieldAccess
		   | ArrayAccess
		   | FunctionCall
		   | NewObject'''
	p[0] = p[1]
 
def p_arrayccess(p):
	'ArrayAccess : Primary LSQ AE RSQ'
	p[0] = NODE('array',[p[1],p[3]])
 	
def p_fieldaccess(p):
	'FieldAccess : Primary DOT ID'
	x = NODE('id', [p[3]])
	p[0] = NODE('objaccess',[p[1], x])
 	
def p_fieldaccess_id(p):
	'FieldAccess : ID'
	p[0] = NODE('id', [p[1]])
 	
def p_funcall(p):
	'FunctionCall : FieldAccess LOPEN ArgOpt ROPEN'
	p[0] = NODE('funcall', [p[1], p[3]])
 	
def p_argopt(p):
	'ArgOpt : Args'
	p[0] = p[1]
 	
def p_argopt_null(p):
	'ArgOpt : '
	p[0] = NODE('epsilon',[])
 
def p_args_ae(p):
	'Args : AE COMA Args'
	p[0] = NODE('args',[p[1],p[3]])
	
def p_args_ae_(p):
	'Args : AE'
	p[0] = NODE('args',[p[1]])
	
def p_new_obj(p):
	'NewObject : NEW ID LOPEN ROPEN'
	x = NODE('id', [p[2]])
	p[0] = NODE('newobj', [x])

def p_stmt_funcall(p):
	'Stmt : FunctionCall SCOLON'
	p[0] = p[1]

def p_newar(p):
	'NewArray : NEW Type DimExpr DimStar'
	p[0] = 	NODE('newarray',[p[2],p[3],p[4]])

def p_dimexpr(p):
	'DimExpr : LSQ AE RSQ'
	p[0] = p[2]

def p_dimstar(p):
	'DimStar : LSQ RSQ DimStar'
	p[0] = NODE('dim',[p[3]]);

def p_dimstar_null(p):
	'DimStar : '
	p[0] = NODE('epsilon',[])

def p_error(p):
	if(p == None):
		print "Syntax error: Reached end of file"
	else:
		print "Syntax error at Line: ", p.lineno, "Before Token: ", p.value
	exit(0)


def print_sequence(blk_map,block_no,printed):
	for items in blk_map[block_no]:
		print block_no, "  ", items
		if(items.dest and not str(items.dest).isdigit() and str(items.dest)[:2] == '_b'):
			if(str(items.dest) not in printed):
				printed.append(str(items.dest))
				print_sequence(blk_map,str(items.dest),printed)
		if(items.src1 and not str(items.src1).isdigit() and str(items.src1)[:2] == '_b'):
			if(str(items.src1) not in printed):
				printed.append(str(items.src1))
				print_sequence(blk_map,str(items.src1), printed)
		if(items.operatortype != 9 and items.src2 and not str(items.src2).isdigit() and str(items.src2)[:2] == '_b'):
			if(str(items.src2) not in printed):
				printed.append(str(items.src2))
				print_sequence(blk_map,str(items.src2),printed)


def parse(s):
	parser = yacc.yacc()
	result = parser.parse(s)
	if(result.wellformed([],[],0)):
		print result.get_type().ptype
		if(not fun_def.has_key('main')):
			print 'main() function not present'
			exit()
#
#	if(result.wellformed([],[],0)):
#		new_block = get_new_block()
#		if(block_map.has_key(new_block)):
#			print "No new block label found.. "
#			exit(0)
		result.gencode()
		#print_sequence(block_map, main_blk,[])

		#print "------ all functions ------"
		#for element in fun_blk_map.keys():
		#	print ' -- function ' + element + ' start -- '
		#	print_sequence(block_map, fun_blk_map[element], [])
		#	print ' -- function ' + element + ' finish -- '

		#print "\n\n\n"
		return block_map, main_blk, fun_blk_map
	else:
		print "Not a well-formed program."
		exit(0)

inputd = '''
class B {
int p;
int getp() {
return this.p;
}
}
class C extends B{
int a;
int fun2(bool x, bool y) {
int p;
//super.a = 32;
x = y;
y = x;
p = super.p;
print (p);
p = super.getp();
print (p);
return this.a;
}
}
void main() {
C obc;
B obb;
int r;
obc = new C();
obb = obc;
obb.p = 21;
r = obc.fun2(true,false);
print (r);
return;
} '''

inputc = '''
class A{
        int x;
        int get(){
                return this.x;
        }
        void set(int a){
                this.x = a;
		return;
        }
}

void main() {
    A a;
    a = new A();

    a.set(5);

        a.x = a.get()*5;
        print(a.get());
    return;
}
'''
inputb = '''
int x;

int foo() {
    x = 41;
    return x + 1;
}

void main() {
     int y;

     x = 42;
     y = foo();
     print(x);
     print(y);

     return;
}
'''
inputa = '''
int a,b,c;
class A {
	int x;
	void foo(int y) {
		int z,s,t;
		z=2;
		print(z);
		return;
	}
}
class B extends A {
	int y;
	void foo1(int z) {
		int z,s,t;
		z=2;
		this.y = 0;
		print(z);
		return;
	}
	void foo1(bool x) {
		int z,s,t;
		z=2;
		print(z);
		return;
	}
}

int foo(int g) {
		int z,s,t;
		z=2;
		print(z);
		return z;
}
int foo(int g, int k) {
		int z,s,t;
		z=2;
		print(z);
		return z;
}

void main() {
	int j,k,l;
	bool ft;
	B obj1;
	j=3;
	ft = true;
	print(j);
	obj1 = new B();
	obj1.foo(3);	
	obj1.foo1(3);	
	obj1.foo1(ft);	
	k = foo(3,4);
	print(k);
	return;
}
'''
			
inputm = '''
class A {
int x;
int y[];
}
int func[](A a[][], int b) {
int x[];
x = new int[10];
x[0] = 0;
return x;
}
bool neg(bool val) {
return !val;
}
void main() {
int x[];
bool y;
A m[][];
m = new A[10][];
x = func(m, 7);
y = neg(true);
// Now lets try some conditions
if(y) then {
print(x[0]);
}
return;
}
'''
inputx =''' int a,b,c;
class sample {
 int item1;
 bool item2;
}	
int foo(bool f) {
int x[];
sample obj;
obj = new sample();
obj.item1 = 4;
b =2;
x = new int [10];
x[4] = 3;
if(true) then {
 b = 1;
 return b;
} else {
 b = 10;
 //return;
}
return true;
}
'''
input0 = ''' int a,b;
            b = 4;
        for (a = 1; a < b; a++)
            b++;
        do {
              a++;
           } while(b > 3);
            '''
input1 = ''' int a,b;
            b = 4;
            if(b==5)
           then
          {
              if(b==5)
             then
            {
           int c;
              c=3;
                b=6;}
            
            }
        {
            int d;
            d=6;
        }
        //d=2;
        for (a = 1; a < b; a++)
            b++;
        //do {
        //      a++;
        //   } while(b > 3);
            '''
input2 = '''int a, b, c, s;
a = input();
if(3-4) then { 
if(a==10) then {
	  a = 5;}
	a = a + 5 * 2;}
b= 2+-4;
c= b+2;
s=3;
if (b < 3) then {
   // Comment 1
   c = a * 2;
}
else {
   c = a * 1;
}
while (c < 3) do
{
   b = b++ + 2;
   a = a-- + 3;
}'''
input3 = ''' int a,b;
int d,x;
int z[];
b = 4;
if(b==5)
then
{
if(b==5)
then
{
int c;
c=3;
b=6;}
}
for (a = 1; a < b; a++)
{
int x;
b++;
x=1;
x++;
}
do {
int e;
d = 1;
//a++;
e=1;
e++;
//l = 3;
} while(b > 3);
//x++;
//dfsdfs
d++;
z[2] = 3;
'''
input4 = '''
class A {
  int x;
  int y[];
}

int func[]() {
  int x[];
  x = new int[10];
  x[0] = 0;
  return x;
}

bool neg(bool val) {
  return !val;
}

void main() {
  int x[];
  bool y;
  A m[];
  m = new A[10];
  print(m[0].x);
  x = func();
  y = neg(true);
  // Now lets try some conditions
  if(y) then {
    print(x[0]);
  }
  return;
}
'''
input5 = '''
int fact(int n){
        int x,y;
        //print (n);
        if (n < 1) then {
                return -1;
        }
        if ((n == 1) || (n == 2)) then {
//              print (n);
                return n;
        }
        x = fact(n-1);
        //print (x);
        y = fact(n-2);
        return x + y;

}

void main(){
        int x;
        x = fact(5);
        print (x);
        return;
}
'''

input6 = '''
class A{
	int x;
	int y;
}

void main(){
        A x[];
        x = new A[5];
        x[0].x = 1;
        print (x[0].x);
        return;
}
'''
#parse(input5)
#parse(input4)
# parse(input6)
#parse(inputd)
#parse(inputc)
#parse(inputb)
#parse(input5)
#parse(input4)
#parse(inputm)
#parse(input4)
#parse(inputm)
#parse(inputx)
#parse(input0)
#parse(input2)
#parse(input3)
