import IR
import parser

def grapher(graph,ins):
	nodes = []
	for i in range(0, len(ins)):
		for j in range(0, len(ins[i])):
			if ins[i][j] not in nodes:
				nodes.append(ins[i][j])
			for k in range(0, len(ins[i])):
				if (ins[i][k] != ins[i][j]):
					if(graph.has_key(ins[i][k])):
						if(ins[i][j] not in graph[ins[i][k]]):
							graph[ins[i][k]].append(ins[i][j])
					else:
						graph[ins[i][k]] = [ins[i][j]] 
					if(graph.has_key(ins[i][j])):
						if(ins[i][k] not in graph[ins[i][j]]):
							graph[ins[i][j]].append(ins[i][k])
					else:
						graph[ins[i][j]] = [ins[i][k]] 
				else:
					if(not graph.has_key(ins[i][k])):
						graph[ins[i][k]] = []
	return graph

var_temp = 0

def set_var_temp():
	global var_temp

def get_var_temp(var):
	global var_temp
	var_temp += 1
	return var+str(var_temp)
	
def resolve_spill(adj, inter_code_list):

	spill_map = {}
	new_icode_list = []

	for iter in range(0, len(inter_code_list)):

		left_side = 0
		right_side = 0

		if(inter_code_list[iter].src1 != None):
			if(str(inter_code_list[iter].src1) == adj):
				right_side = 1

		if(inter_code_list[iter].src2 != None):
			if(str(inter_code_list[iter].src2) == adj):
				right_side = 1

		if(right_side == 1):
			new_spill_var = get_var_temp(adj)
			
			if(not spill_map.has_key(adj)):
				print "Some problem with spill map"
				exit(0)

			new_icode_list.append(IR.IR_STMT('mem_load', new_spill_var, 0, spill_map[adj], 10, None, None, 5))
			if(inter_code_list[iter].src1 != None):
				if(str(inter_code_list[iter].src1) == adj):
					inter_code_list[iter].src1.value = new_spill_var

			if(inter_code_list[iter].src2 != None):
				if(str(inter_code_list[iter].src2) == adj):
					inter_code_list[iter].src2.value = new_spill_var

		if(inter_code_list[iter].dest != None):
			if(str(inter_code_list[iter].dest) == adj):
				left_side = 1
				if(right_side == 1):
					print "left and right both are 1. see this case"
					exit(0)
				new_spill_var1 = get_var_temp(adj)
				if(not spill_map.has_key(adj)):
					spill_map[adj] = 'M'+adj	
			
				inter_code_list[iter].dest = new_spill_var1

		new_icode_list.append(inter_code_list[iter])

		if(left_side == 1):
			new_icode_list.append(IR.IR_STMT('store', spill_map[adj], 0, new_spill_var1, 0, None, None, 6))

	return new_icode_list

def color(element, k, spilled, cnodes):
	colors = []
	for i in range(0, k):
		colors.append(i)

	if(element[0] in spilled):
		return cnodes

	for adj in element[1]:
		if(adj not in spilled):
			if(not cnodes.has_key(adj)):
				print "adjacency not colored"
				exit(0)
			else:
				if(cnodes[adj] in colors):
					colors.remove(cnodes[adj])
	if(cnodes.has_key(element[0])):
		print 'element', element[0], cnodes[element[0]], "NODE ALREADY COLORED. see this case"
		exit(0)
	else:
		cnodes[element[0]] = colors[0]
	return cnodes

def kcolor(insoutsmaps, k, blk_map, blkid, func_map):
	graph = {}
	for key in insoutsmaps:
		graph = grapher(graph,insoutsmaps[key].ins)
	# for key in graph:
	#   print key, graph[key]
	stack = []
	spilled = []
	found = True
	while(found):
		found = False
		max_key = None	
		for key in graph.keys():
			if(len(graph[key]) < k):
				found = True
				for node in graph[key]:
					graph[node].remove(key)
				stack.append([key, graph[key]])
				graph.pop(key)
			else:
				if(max_key == None):
					max_key = key
				else:
					if(len(graph[key]) > len(graph[max_key])):
						max_key = key
		if(found == False and graph):
			# we need to take care of spill for max_key
			spilled.append(max_key)
			for xnode in graph[max_key]:
				graph[xnode].remove(max_key)
			stack.append([max_key, graph[max_key]])
			found = True
			graph.pop(max_key)
	if(not graph):
		cnodes = {}
		while (len(stack) != 0):
			element = stack.pop()
			cnodes = color(element, k, spilled, cnodes)
		# print "success"
		if(len(parser.global_table)):
			for iter in parser.global_table:
				spilled.append(iter)
			parser.global_table = []
		# if(len(spilled)):
		# 	print 'spilled', spilled
		for key in spilled:
			# print "spill key: ", key
			for iter in blk_map:
				blk_map[iter] = resolve_spill(key, blk_map[iter])
		if(len(spilled)):
			insoutsmaps = IR.dofuncliveness(blk_map, func_map)
			# insoutsmaps = IR.liveness(blk_map, blkid, [])
			return kcolor(insoutsmaps, k, blk_map, blkid, func_map)
		return cnodes , blk_map
	else:
		print "EXIT WITH FAILURE"
		exit()

