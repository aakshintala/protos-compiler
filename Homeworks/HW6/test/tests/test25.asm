	.data
newline:	.asciiz	"\n"
boundmsg:	.asciiz	"Array Index out of Bounds.\n"

	.text
main:
	addiu $sp, $sp, -48
	sw $s0,0($sp)
	sw $s1,4($sp)
	sw $s2,8($sp)
	sw $s3,12($sp)
	sw $s4,16($sp)
	sw $s5,20($sp)
	sw $s6,24($sp)
	sw $a0,32($sp)
	sw $a1,36($sp)
	sw $a2,40($sp)
	addiu $sp, $sp, -4
	sw $ra,($sp)
	li $t0,2

	li $v0,9
	add $t0,$t0,1
	mul $t0,$t0,4
	move $a0,$t0
	syscall
	move $t2,$v0
	div $t0,$t0,4
	sub $t0,$t0,1
	sw $t0, ($t2)
	move $t2,$t2
	li $t7,10
	li $t0,0
	add $t6,$t0,1
	mul $t6,$t6,4
	add $t6,$t2,$t6
	sw $t7, ($t6)
	li $t0,10

	li $v0,9
	add $t0,$t0,1
	mul $t0,$t0,4
	move $a0,$t0
	syscall
	move $t5,$v0
	div $t0,$t0,4
	sub $t0,$t0,1
	sw $t0, ($t5)
	li $t0,1
	add $t3,$t0,1
	mul $t3,$t3,4
	add $t3,$t2,$t3
	sw $t5, ($t3)
	li $t0,1
	add $t1,$t0,1
	mul $t1,$t1,4
	add $t1,$t2,$t1
	lw $t3, ($t1)
	li $t1,0
	lw $t0, ($t3)
	sle $t0,$t0,$t1

	bgtz $t0, boundfail
	li $t0,0
	slt $t0,$t1,$t0

	bgtz $t0, boundfail
	add $t0,$t1,1
	mul $t0,$t0,4
	add $t0,$t3,$t0
	lw $t0, ($t0)

	li $v0,1
	move $a0,$t0
	syscall
	li $v0,4
	la $a0,newline
	syscall
	addiu $sp, $sp, -40
	sw $t0,0($sp)
	sw $t1,4($sp)
	sw $t2,8($sp)
	sw $t3,12($sp)
	sw $t4,16($sp)
	sw $t5,20($sp)
	sw $t6,24($sp)
	sw $t7,28($sp)
	sw $t8,32($sp)

	jal _b2
	lw $t0,0($sp)
	lw $t1,4($sp)
	lw $t2,8($sp)
	lw $t3,12($sp)
	lw $t4,16($sp)
	lw $t5,20($sp)
	lw $t6,24($sp)
	lw $t7,28($sp)
	lw $t8,32($sp)
	addiu $sp, $sp, 40
	move $t0,$v0
	move $t0,$t0
	addiu $sp, $sp, -40
	sw $t0,0($sp)
	sw $t1,4($sp)
	sw $t2,8($sp)
	sw $t3,12($sp)
	sw $t4,16($sp)
	sw $t5,20($sp)
	sw $t6,24($sp)
	sw $t7,28($sp)
	sw $t8,32($sp)
	li $t1,1
	addiu $sp, $sp, -4
	sw $t1, ($sp)

	jal _b3
	lw $t0,0($sp)
	lw $t1,4($sp)
	lw $t2,8($sp)
	lw $t3,12($sp)
	lw $t4,16($sp)
	lw $t5,20($sp)
	lw $t6,24($sp)
	lw $t7,28($sp)
	lw $t8,32($sp)
	addiu $sp, $sp, 40
	li $t1,1
	move $t1,$t1
	move $t1,$t1

	beqz $t1,block1
_b5:
	li $t3,1
	lw $t1, ($t0)
	sle $t1,$t1,$t3

	bgtz $t1, boundfail
	li $t1,0
	slt $t1,$t3,$t1

	bgtz $t1, boundfail
	add $t1,$t3,1
	mul $t1,$t1,4
	add $t1,$t0,$t1
	lw $t0, ($t1)

	li $v0,1
	move $a0,$t0
	syscall
	li $v0,4
	la $a0,newline
	syscall
	li $t0,0
	add $t4,$t0,1
	mul $t4,$t4,4
	add $t4,$t2,$t4
	lw $t0, ($t4)

	li $v0,1
	move $a0,$t0
	syscall
	li $v0,4
	la $a0,newline
	syscall
block1:
	lw $ra,($sp)
	addiu $sp, $sp, 4
	lw $s0,0($sp)
	lw $s1,4($sp)
	lw $s2,8($sp)
	lw $s3,12($sp)
	lw $s4,16($sp)
	lw $s5,20($sp)
	lw $s6,24($sp)
	lw $a0,32($sp)
	lw $a1,36($sp)
	lw $a2,40($sp)
	addiu $sp, $sp, 48
	jr $ra
exit:	li $v0, 10
	syscall

boundfail:
	li $v0,4
	la $a0,boundmsg
	syscall
	b exit


_b3:
	lw $t0, ($sp)
	addiu $sp, $sp, 4
	addiu $sp, $sp, -48
	sw $s0,0($sp)
	sw $s1,4($sp)
	sw $s2,8($sp)
	sw $s3,12($sp)
	sw $s4,16($sp)
	sw $s5,20($sp)
	sw $s6,24($sp)
	sw $a0,32($sp)
	sw $a1,36($sp)
	sw $a2,40($sp)
	addiu $sp, $sp, -4
	sw $ra,($sp)
	move $t0,$t0

	beqz $t0,block2
_b4:
	li $t0,2
	move $t0,$t0
	move $t0,$t0

	li $v0,1
	move $a0,$t0
	syscall
	li $v0,4
	la $a0,newline
	syscall
block2:
	lw $ra,($sp)
	addiu $sp, $sp, 4
	lw $s0,0($sp)
	lw $s1,4($sp)
	lw $s2,8($sp)
	lw $s3,12($sp)
	lw $s4,16($sp)
	lw $s5,20($sp)
	lw $s6,24($sp)
	lw $a0,32($sp)
	lw $a1,36($sp)
	lw $a2,40($sp)
	addiu $sp, $sp, 48
	jr $ra


_b2:
	addiu $sp, $sp, -48
	sw $s0,0($sp)
	sw $s1,4($sp)
	sw $s2,8($sp)
	sw $s3,12($sp)
	sw $s4,16($sp)
	sw $s5,20($sp)
	sw $s6,24($sp)
	sw $a0,32($sp)
	sw $a1,36($sp)
	sw $a2,40($sp)
	addiu $sp, $sp, -4
	sw $ra,($sp)
	li $t1,10

	li $v0,9
	add $t1,$t1,1
	mul $t1,$t1,4
	move $a0,$t1
	syscall
	move $t0,$v0
	div $t1,$t1,4
	sub $t1,$t1,1
	sw $t1, ($t0)
	move $t0,$t0
	li $t3,0
	li $t1,0
	lw $t2, ($t0)
	sle $t2,$t2,$t1

	bgtz $t2, boundfail
	li $t2,0
	slt $t2,$t1,$t2

	bgtz $t2, boundfail
	add $t2,$t1,1
	mul $t2,$t2,4
	add $t2,$t0,$t2
	sw $t3, ($t2)
	move $t0,$t0
	lw $ra,($sp)
	addiu $sp, $sp, 4
	lw $s0,0($sp)
	lw $s1,4($sp)
	lw $s2,8($sp)
	lw $s3,12($sp)
	lw $s4,16($sp)
	lw $s5,20($sp)
	lw $s6,24($sp)
	lw $a0,32($sp)
	lw $a1,36($sp)
	lw $a2,40($sp)
	addiu $sp, $sp, 48
	move $v0,$t0
	jr $ra
