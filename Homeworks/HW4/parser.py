import ply.yacc as yacc
#import sys
import IR
import copy
from lexer import tokens
precedence = (('left','OR'),
		('left', 'AND'),
		('nonassoc', 'EQ', 'NEQ'),
		('nonassoc', 'GT','LT','GTEQ','LTEQ'),
		('left', 'ADD','SUB'),
		('left', 'MUL','DIV','MOD'),
		('right', 'NOT'),
		('right', 'UMINUS'))

block_map = {}
global_id =0
s_table = {}

def set_temp():
        global global_id
        global_id = 0

def get_temp():
        global global_id
        global_id = global_id + 1
        return '_t'+str(global_id-1)

def convert_addr(icode_list):
	ptr = icode_list[len(icode_list)-1].dest
	ptrtype = icode_list[len(icode_list)-1].desttype
	if(ptrtype == 2):
		op1 = get_temp()
		icode_list.append(IR.IR_STMT('deref',op1, 0, ptr, ptrtype, None, None, 90))

def getitem(iter):
	x = ''
	if(isinstance(iter, list)):
		for another in iter:
			x = x + getitem(another)
	else:
		x = x + str(iter)
	return x

bno = 0
def get_new_block():
	global bno
	bno = bno + 1
	return "_b"+str(bno)

cur_bno = ''
def get_cur_block():
	global cur_bno
	return str(cur_bno)

def set_cur_block(x):
	global cur_bno
	cur_bno = x
					
class NODE:
	def __init__(self,op,items):
		#print op, items
		self.items = items
		self.op = op

	def __str__(self):
		x = str(self.op) + " "
		for iter in self.items:
			x = x + getitem(iter)
		return x

	def defined(self):
		if(self.op == '='):
			if(self.items[0].op != 'id'):
				print "left hand side is not id.. error"
				exit(0)
			return [self.items[0].items[0]]

		if(self.op == 'id' or self.op == 'intconst'):
			return []

	def wellformed(self, declared, defined, lhs):

		if(self.op == '='):
			if(not self.items[1].wellformed(declared, defined, 0)):
				return False
			else:
				if(not self.items[0].wellformed(declared, defined, 1)):
					return False
				return True

		if(self.op == 'ife'):
			if(not self.items[0].wellformed(declared, defined, 0)):
				return False

			local_defined1 = defined[:]
			local_defined2 = defined[:]
			local_declared1 = declared[:]
			local_declared2 = declared[:]

			if(not self.items[1].wellformed(local_declared1, local_defined1,0) or not self.items[2].wellformed(local_declared2, local_defined2,0)):
				return False
		
			for iter1 in local_defined1:
				if iter1 in local_defined2:
					if (iter1 not in defined) and (iter1 in declared):
						defined.append(iter1)
			return True		
		
		if(self.op == 'if' or self.op == 'while'):
			local_defined = defined[:]
			local_declared = declared[:]
			if(not self.items[0].wellformed(declared, defined, 0) or not self.items[1].wellformed(local_declared, local_defined, 0)):
				return False
			else:
				return True

		if(self.op == 'dowhile'):
			local_defined = defined[:]
			local_declared = declared[:]
			if(not self.items[0].wellformed(local_declared, local_defined, 0) or not self.items[1].wellformed(declared, defined, 0)):
				return False
			for iter1 in local_defined:
				if (iter1 not in defined) and (iter1 in declared):
					defined.append(iter1)
			return True		

		if(self.op == 'Pgm'):

			local_defined = defined[:]
			local_declared = declared[:]
			for element in self.items:
				if(not element.wellformed(local_declared, local_defined, 0)):
					return False
			for iter1 in local_defined:
				if (iter1 not in defined) and (iter1 in declared):
					defined.append(iter1)
			return True

		if(self.op == 'for'):
			if(not self.items[0].wellformed(declared, defined, 0)):
				return False
			if(not self.items[1].wellformed(declared, defined, 0)):
				return False
			if(not self.items[2].wellformed(declared, defined, 0)):
				return False
	
			local_defined = defined[:]
			local_declared = declared[:]
			if(not self.items[3].wellformed(local_declared, local_defined, 0)):
				return False
			else:
				return True

		if(self.op == 'intconst' or self.op == 'bool'):
			return True

		if(self.op == 'id'):
			if(lhs == 2):
				if(self.items[0] not in declared):
					declared.append(self.items[0])
				return True

			if(lhs == 1):
				if(self.items[0] not in declared):
					print str(self.items[0]), "not declared" 
					return False
				else:
					if(self.items[0] not in defined):
						defined.append(self.items[0])
				return True
			else:
				if(self.items[0] not in declared):
					print str(self.items[0]), "not declared" 
					return False
				if(self.items[0] not in defined):
					print str(self.items[0]), "not defined" 
					return False
				else:
					return True
				
		if(self.op == 'varlist' or self.op == 'var'):
			for element in self.items:
				if(not element.wellformed(declared, defined, 2)):
					return False
			return True

		if(self.op == 'array'):
			if(not self.items[1].wellformed(declared, defined, 0)):
				return False
			if(not self.items[0].wellformed(declared, defined, 0)):
				return False
			return True

		for element in self.items:
			if(not element.wellformed(declared, defined, 0)):
				return False
		return True

	def get_declared(self, local_declared):
		if(self.op == 'declseq' or self.op == 'Decl' or self.op == 'varlist'):
			for element in self.items:
				element.get_declared(local_declared)
		if(self.op == 'var'):
			local_declared.append(self.items[0].items[0])

	def gencode(self):
		global s_table

		if(self.op == 'intconst'):
			temp = get_temp()
			return [IR.IR_STMT('loadi', temp, 0, self.items[0], 1, None, None, 1)]

		if(self.op == 'id'):
#			temp = get_temp()
			if(s_table[self.items[0]] == 0):
				temp = self.items[0]
			else:
				temp = self.items[0] + str(s_table[self.items[0]])
			return [IR.IR_STMT('move', temp, 0, temp, 0, None, None, 4)]

		if(self.op == 'epsilon'):
			return []

		if(self.op == 'Pgm'):
			#new_bno = get_new_block()
			#if(block_map.has_key(new_bno)):
			#	print "Couldn't find a new block name.. "
			#	exit(0)
			#block_map[new_bno] = self.items[1].gencode()
			#return [IR.IR_STMT('new block', new_bno, None, None, None, None, 50)]
			self.s_table = copy.deepcopy(s_table)
			local_declared = []
			self.items[0].get_declared(local_declared)
			for element in local_declared:
				if(s_table.has_key(element)):
					s_table[element] = s_table[element] + 1 
				else:
					s_table[element] = 0
			print "in pgm", local_declared, s_table
			list_icode = self.items[1].gencode()
			s_table = copy.deepcopy(self.s_table)
			return list_icode

		if(self.op == 'stmtseq'):
			list_icode = []
			for element in self.items:
				list_icode = list_icode + element.gencode()
			return list_icode
				
		if(self.op == 'print'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			opcode = 'print'
			icode_list = self.items[0].gencode()
			if(len(icode_list) == 0):
				print "SEE THIS CASE.." # an id.
				op1 = self.items[0].items[0]
			else:
				convert_addr(icode_list)
				op1 = icode_list[len(icode_list)-1].dest
				op1type = 0
			optype = 2
			x = IR.IR_STMT(opcode, dest, 0, op1, op1type, op2, op2type, optype)
			icode_list.append(x)
			return icode_list

		if(self.op == '='):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
                        if(self.items[0].op == 'id'):
				print s_table
				if(s_table[self.items[0].items[0]] == 0):
					dest = self.items[0].items[0]
				else:
                                	dest = self.items[0].items[0] + str(s_table[self.items[0].items[0]])
#                                if(self.items[1].op == 'new'):
#                                        opcode = 'alloc'
#                                        list_icode = self.items[1].gencode()
#				
#					convert_addr(list_icode)	
#                                        op1 = list_icode[len(list_icode)-1].dest
#                                        op1type = 0
#                                        list_icode.append(IR.IR_STMT(opcode, dest, 2, op1, op1type, None, None, 60));
#                                else:
				list_icode = self.items[1].gencode()
				opcode = '='
					
				convert_addr(list_icode)	
				op1 = list_icode[len(list_icode)-1].dest
				op1type = 0
				list_icode.append(IR.IR_STMT(opcode, dest, 0, op1, op1type, None, None, 4));
                        else:
#                                if(self.items[1].op == 'new'):
#                                        opcode = 'alloc'
#                                        list_icode = self.items[1].gencode()
#					
#					convert_addr(list_icode)	
#                                        op1 = list_icode[len(list_icode)-1].dest
#                                        op1type = 0
#                                	list_icode = list_icode + self.items[0].gencode()
#                                	dest = list_icode[len(list_icode)-1].dest
#					desttype = list_icode[len(list_icode)-1].desttype
#          
#					if(desttype != 2):
#						print "Something wrong with desttype"
#
#					temp_var = get_temp()
#					list_icode.append(IR.IR_STMT(opcode, temp_var, 2, op1, op1type, None, None, 60));
#					opcode = 'store'
#					list_icode.append(IR.IR_STMT(opcode, dest, desttype, temp_var, 2, None, None, 61));
#				else:
				list_icode = self.items[1].gencode()
				convert_addr(list_icode)  
				op1 = list_icode[len(list_icode)-1].dest
				op1type = 0
				list_icode = list_icode + self.items[0].gencode()
				dest = list_icode[len(list_icode)-1].dest
				desttype = list_icode[len(list_icode)-1].desttype
				if(desttype != 2):
					print "Something wrong with desttype"
				opcode = 'store'
				list_icode.append(IR.IR_STMT(opcode, dest, desttype, op1, op1type, None, None, 61));

			return list_icode
  
		if(self.op == 'array'):
                        opcode = dest = op1 = op1type = op2 = op2type = optype = None
                        list_icode = []
                        dest = get_temp()
                        if(self.items[0].op == 'id'):
				if(s_table[self.items[0].items[0]] == 0):
					op1 = self.items[0].items[0]
				else:
					op1 = self.items[0].items[0] + str(s_table[self.items[0].items[0]])
                                op1type = 2 
                                list_icode = self.items[1].gencode()
					
				convert_addr(list_icode)	
                                op2 = list_icode[len(list_icode)-1].dest;
                                op2type = 0
                                # load the size at op1 location
				#temp_size = get_temp()
				list_icode.append(IR.IR_STMT('deref',dest, 0, op1, op1type, None, None, 90))
				list_icode.append(IR.IR_STMT('<=', dest, 0, dest, 0, op2, 0, 0))
				list_icode.append(IR.IR_STMT('exit', None, 0, dest, 0, None, None, 81))
				list_icode.append(IR.IR_STMT('loadi', dest, 0, 0, 1, None, None, 1))
				list_icode.append(IR.IR_STMT('<', dest, 0, op2, 0, dest, 0, 0))
				list_icode.append(IR.IR_STMT('exit', None, 0, dest, 0, None, None, 81))
                                # load the addr in dest
                                list_icode.append(IR.IR_STMT('aload', dest, 2, op1, op1type, op2, op2type, 55))
                        else:
                                list_icode = self.items[0].gencode()
				convert_addr(list_icode)  
                                op1 = list_icode[len(list_icode)-1].dest
                                op1type = list_icode[len(list_icode)-1].desttype
 
				if(op1type != 0):
					print "Something wrong with desttype"
				# op1 has the addr
				list_icode = list_icode + self.items[1].gencode()
				convert_addr(list_icode)  
				op2 = list_icode[len(list_icode)-1].dest;
				op2type = 0
                                # load the size at op1 location
				#temp_size = get_temp()
				list_icode.append(IR.IR_STMT('deref',dest, 0, op1, op1type, None, None, 90))
				list_icode.append(IR.IR_STMT('<=', dest, 0, dest, 0, op2, 0, 0))
				list_icode.append(IR.IR_STMT('exit', None, 0, dest, 0, None, None, 81))
				list_icode.append(IR.IR_STMT('loadi', dest, 0, 0, 1, None, None, 1))
				list_icode.append(IR.IR_STMT('<', dest, 0, op2, 0, dest, 0, 0))
				list_icode.append(IR.IR_STMT('exit', None, 0, dest, 0, None, None, 81))
				# dest has the addr
				list_icode.append(IR.IR_STMT('aload', dest, 2, op1, op1type, op2, op2type, 55))
			return list_icode

		if(self.op == 'postinc'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			
			icode_list = self.items[0].gencode()
			if(len(icode_list) == 0):
				print "SEE THIS CASE.." # an id.
				op1 = self.items[0].items[0]
				op1type = 0
			else:
				op1 = icode_list[len(icode_list)-1].dest
				op1type = icode_list[len(icode_list)-1].desttype

			if(op1type != 2):
				temp = get_temp()
				icode_list.append(IR.IR_STMT('move', temp, 0, op1, op1type, None, None, 4))
				temp1 = get_temp()
				icode_list.append(IR.IR_STMT('loadi', temp1, 0, 1, 1, None, None, 1))
				opcode = '+'
				optype = 0 #arithmetic
				x = IR.IR_STMT(opcode, op1, 0, temp1, 0, op1, 0, optype)
				icode_list.append(x)
				icode_list.append(IR.IR_STMT('move', temp, 0, temp, 0, None, None, 4))
			else:
				temp = get_temp()
				icode_list.append(IR.IR_STMT('deref',temp, 0, op1, op1type, None, None, 90))
				temp1 = get_temp()
				icode_list.append(IR.IR_STMT('loadi', temp1, 0, 1, 1, None, None, 1))
				opcode = '+'
				optype = 0 #arithmetic
				x = IR.IR_STMT(opcode, temp1, 0, temp1, 0, temp, 0, optype)
				icode_list.append(x)
                                icode_list.append(IR.IR_STMT('store', op1, op1type, temp1, 0, None, None, 61));
				icode_list.append(IR.IR_STMT('move', temp, 0, temp, 0, None, None, 4))
				
			return icode_list
					
		if(self.op == 'preinc'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			
			icode_list = self.items[0].gencode()
			if(len(icode_list) == 0):
				print "SEE THIS CASE.." # an id.
				op1 = self.items[0].items[0]
				op1type = 0
			else:
				op1 = icode_list[len(icode_list)-1].dest
				op1type = icode_list[len(icode_list)-1].desttype

			if(op1type != 2):
				temp = get_temp()
				icode_list.append(IR.IR_STMT('loadi', temp, 0, 1, 1, None, None, 1))
				opcode = '+'
				optype = 0 #arithmetic
				x = IR.IR_STMT(opcode, op1, 0, temp, 0, op1, 0, optype)
				icode_list.append(x)
			else:
				temp = get_temp()
				icode_list.append(IR.IR_STMT('deref',temp, 0, op1, op1type, None, None, 90))
				temp1 = get_temp()
				icode_list.append(IR.IR_STMT('loadi', temp1, 0, 1, 1, None, None, 1))
				opcode = '+'
				optype = 0 #arithmetic
				x = IR.IR_STMT(opcode, temp, 0, temp1, 0, temp, 0, optype)
				icode_list.append(x)
                                icode_list.append(IR.IR_STMT('store', op1, op1type, temp, 0, None, None, 61));
				icode_list.append(IR.IR_STMT('move', temp, 0, temp, 0, None, None, 4))
			return icode_list
					
		if(self.op == 'postdec'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			
			icode_list = self.items[0].gencode()
			if(len(icode_list) == 0):
				print "SEE THIS CASE.." # an id.
				op1 = self.items[0].items[0]
				op1type = 0
			else:
				op1 = icode_list[len(icode_list)-1].dest
				op1type = icode_list[len(icode_list)-1].desttype

			if(op1type != 2):
				temp = get_temp()
				icode_list.append(IR.IR_STMT('move', temp, 0, op1, op1type, None, None, 4))
				temp1 = get_temp()
				icode_list.append(IR.IR_STMT('loadi', temp1, 0, 1, 1, None, None, 1))
				opcode = '-'
				optype = 0 #arithmetic
				x = IR.IR_STMT(opcode, op1, 0, op1, 0, temp1, 0, optype)
				icode_list.append(x)
				icode_list.append(IR.IR_STMT('move', temp, 0, temp, 0, None, None, 4))
			else:
				temp = get_temp()
				icode_list.append(IR.IR_STMT('deref',temp, 0, op1, op1type, None, None, 90))
				temp1 = get_temp()
				icode_list.append(IR.IR_STMT('loadi', temp1, 0, 1, 1, None, None, 1))
				opcode = '-'
				optype = 0 #arithmetic
				x = IR.IR_STMT(opcode, temp1, 0, temp, 0, temp1, 0, optype)
				icode_list.append(x)
                                icode_list.append(IR.IR_STMT('store', op1, op1type, temp1, 0, None, None, 61));
				icode_list.append(IR.IR_STMT('move', temp, 0, temp, 0, None, None, 4))

			return icode_list
					
		if(self.op == 'predec'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			
			icode_list = self.items[0].gencode()
			if(len(icode_list) == 0):
				print "SEE THIS CASE.." # an id.
				op1 = self.items[0].items[0]
				op1type = 0
			else:
				op1 = icode_list[len(icode_list)-1].dest
				op1type = icode_list[len(icode_list)-1].desttype

			if(op1type != 2):
				temp = get_temp()
				icode_list.append(IR.IR_STMT('loadi', temp, 0, 1, 1, None, None, 1))
				opcode = '-'
				optype = 0 #arithmetic
				x = IR.IR_STMT(opcode, op1, 0, op1, 0, temp, 0, optype)
				icode_list.append(x)
			else:
				temp = get_temp()
				icode_list.append(IR.IR_STMT('deref',temp, 0, op1, op1type, None, None, 90))
				temp1 = get_temp()
				icode_list.append(IR.IR_STMT('loadi', temp1, 0, 1, 1, None, None, 1))
				opcode = '-'
				optype = 0 #arithmetic
				x = IR.IR_STMT(opcode, temp, 0, temp, 0, temp1, 0, optype)
				icode_list.append(x)
                                icode_list.append(IR.IR_STMT('store', op1, op1type, temp, 0, None, None, 61));
				icode_list.append(IR.IR_STMT('move', temp, 0, temp, 0, None, None, 4))
			return icode_list
					
		if(self.op == 'input'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			opcode = 'input'
			dest = get_temp()
			optype = 3
			return [IR.IR_STMT(opcode, dest, 0, op1, op1type, op2, op2type, optype)]

		if(self.op == 'uminus' or self.op == 'not'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			opcode = self.op
			list_icode = self.items[0].gencode()
			if(len(list_icode) == 0):
				op1 = self.items[0].items[0]
			else:
				convert_addr(list_icode)
				op1 = list_icode[len(list_icode)-1].dest
			op1type = 0
			dest = get_temp()
			optype = 0
			x = IR.IR_STMT(opcode, dest, 0, op1, op1type, op2, op2type, optype)
			list_icode.append(x)
			return list_icode

		if(self.op == '+' or self.op == '-' or self.op == '*' or self.op == '/' or self.op == '%' or self.op == '==' or self.op == '!=' or self.op == '<' or self.op == '>' or self.op == '>=' or self.op == '<='):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			dest = get_temp()
			list_icode1 = self.items[1].gencode()
			if(len(list_icode1) == 0):
				op2 = self.items[1].items[0]
			else:
				convert_addr(list_icode1)
				op2 = list_icode1[len(list_icode1)-1].dest
			op2type = 0
			list_icode2 = self.items[0].gencode()
			if(len(list_icode2) == 0):
				op1 = self.items[0].items[0]
			else:
				convert_addr(list_icode2)
				op1 = list_icode2[len(list_icode2)-1].dest

			op1type = 0
			opcode = self.op
			optype = 0 #arithmetic

			list_icode1 = list_icode1 + list_icode2
			x = IR.IR_STMT(opcode, dest, 0, op1, op1type, op2, op2type, optype)
			list_icode1.append(x)
			return list_icode1

		if(self.op == 'bool'):
			if(self.items[0] == 'true'):	
				temp = get_temp()
				list_icode = [IR.IR_STMT('loadi', temp, 0, 1, 1, None, None, 1)]
			else:
				temp = get_temp()
				list_icode = [IR.IR_STMT('loadi', temp, 0, 0, 1, None, None, 1)]
			return list_icode

		if(self.op == 'new'):
			list_icode = self.items[0].gencode()
			convert_addr(list_icode)
			op1 = list_icode[len(list_icode)-1].dest
			op1type = 0

			temp = self.items[1]
			dim = 0;
			while(temp.op != 'epsilon'):
				dim = dim + 1
				temp = temp.items[0]

			if(dim == 0):
				op2 = None
				op2type = None
			else:
				op2 = dim	
				op2type = 1

			dest = get_temp()
			opcode = 'alloc'
			optype = 60
			x = IR.IR_STMT(opcode, dest, 0, op1, op1type, op2, op2type, optype)
			list_icode.append(x)
			return list_icode

		if(self.op == '&&'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			list_icode = self.items[0].gencode()
			if(len(list_icode) == 0):
				op1 = self.items[0].items[0]
			else:
				convert_addr(list_icode)
				op1 = list_icode[len(list_icode)-1].dest
	
			op1type = 0

			dest = get_temp()
			list_icode.append(IR.IR_STMT('move', dest, 0, op1, op1type, None, None, 4))
	
			new_bno = get_new_block()
			if(block_map.has_key(new_bno)):
				print "Couldn't find a new block name.. "
				exit(0)
			block_map[new_bno] = self.items[1].gencode()
			if(len(block_map[new_bno]) == 0):
				block_map[new_bno] = [IR.IR_STMT('move', dest, 0, self.items[1][0], 0, None, None, 4)]
			else:
				block_map[new_bno].append(IR.IR_STMT('move', dest, 0, block_map[new_bno][len(block_map[new_bno])-1].dest, 0, None, None, 4))
			opcode = 'if'
			optype = 8 

			x = IR.IR_STMT(opcode, new_bno, 0, op1, op1type, None, None, optype)
			list_icode.append(x)
			list_icode.append(IR.IR_STMT('move', dest, 0, dest, 0, None, None, 4))
			return list_icode
				
		if(self.op == '||'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			list_icode = self.items[0].gencode()
			if(len(list_icode) == 0):
				op1 = self.items[0].items[0]
			else:
				convert_addr(list_icode)
				op1 = list_icode[len(list_icode)-1].dest
	
			op1type = 0
	
			dest = get_temp()

			new_bno = get_new_block()
			if(block_map.has_key(new_bno)):
				print "Couldn't find a new block name.. "
				exit(0)
			block_map[new_bno] = self.items[1].gencode()
			if(len(block_map[new_bno]) == 0):
				block_map[new_bno] = [IR.IR_STMT('move', dest, 0, self.items[1][0], 0, None, None, 4)]
			else:
				block_map[new_bno].append(IR.IR_STMT('move', dest, 0, block_map[new_bno][len(block_map[new_bno])-1].dest, 0, None, None, 4))

			op2 = new_bno
			op2type = 3 
			opcode = 'ife'
			optype = 7

			new_bno1 = get_new_block()
			if(block_map.has_key(new_bno1)):
				print "Couldn't find a new block name.. "
				exit(0)
			block_map[new_bno1] = [IR.IR_STMT('move', dest, 0, op1, 0, None, None, 4)]

			x = IR.IR_STMT(opcode, new_bno1, 0, op1, op1type, op2, op2type, optype)
			list_icode.append(x)
			list_icode.append(IR.IR_STMT('move', dest, 0, dest, 0, None, None, 4))
			return list_icode

					
		if(self.op == 'ife'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			list_icode = self.items[0].gencode()
			if(len(list_icode) == 0):
				op1 = self.items[0].items[0]
			else:
				convert_addr(list_icode)
				op1 = list_icode[len(list_icode)-1].dest

			op1type = 0
			op2 = None
			opcode = self.op
			optype = 7

			#hack: put else label in op2
			new_bno = get_new_block()
			if(block_map.has_key(new_bno)):
				print "Couldn't find a new block name.. "
				exit(0)

			block_map[new_bno] = self.items[1].gencode()
			#print new_bno, "   ", block_map[new_bno]
			dest = new_bno

			new_bno1 = get_new_block()
			if(block_map.has_key(new_bno1)):
				print "Couldn't find a new block name.. "
				exit(0)

			block_map[new_bno1] = self.items[2].gencode()
			#print new_bno1, "   ", block_map[new_bno1]
			op2 = new_bno1
			op2type = 3
			 
			x = IR.IR_STMT(opcode, dest, 0, op1, op1type, op2, op2type, optype)
			list_icode.append(x)
			return list_icode
								
		if(self.op == 'if'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			list_icode = self.items[0].gencode()
			if(len(list_icode) == 0):
				op1 = self.items[0].items[0]
			else:
				convert_addr(list_icode)
				op1 = list_icode[len(list_icode)-1].dest
			op2 = None
			opcode = self.op
			optype = 8
			op1type = 0

			new_bno = get_new_block()
			if(block_map.has_key(new_bno)):
				print "Couldn't find a new block name.. "
				exit(0)

			block_map[new_bno] = self.items[1].gencode()
			#print new_bno, "   ", block_map[new_bno]
			dest = new_bno

			x = IR.IR_STMT(opcode, dest, 0, op1, op1type, op2, op2type, optype)
			list_icode.append(x)
			return list_icode

		if(self.op == 'while'):
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			new_bno = get_new_block()
			if(block_map.has_key(new_bno)):
				print "Couldn't find a new block name.. "
				exit(0)

			block_map[new_bno] = self.items[0].gencode()
			if(len(block_map[new_bno]) == 0):
				op1 = self.items[0].items[0]
			else:
				convert_addr(block_map[new_bno])
				op1 = block_map[new_bno][len(block_map[new_bno]) - 1].dest
			op1type = 0
			list_icode = []
			op2 = new_bno 
			op2type = 3
			opcode = self.op
			optype = 12

			new_bno1 = get_new_block()
			if(block_map.has_key(new_bno1)):
				print "Couldn't find a new block name.. "
				exit(0)

			block_map[new_bno1] = self.items[1].gencode()
			block_map[new_bno1].append(IR.IR_STMT('loop_end',new_bno, 0, None,None,None,None,11))
			#block_map[new_bno].append(IR.IR_STMT(opcode, new_bno1, op1, op1type, op2, op2type, optype))
			block_map[new_bno].append(IR.IR_STMT(opcode, new_bno1, 0, op1, op1type, None, None, optype))

			list_icode.append(IR.IR_STMT('whilejmp',new_bno, 0, None,None,None,None,9))
			return list_icode

		if(self.op == 'for'):
			list_icode = []
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			new_bno = get_new_block()
			if(block_map.has_key(new_bno)):
				print "Couldn't find a new block name.. "
				exit(0)
			list_icode = self.items[0].gencode()

			block_map[new_bno] = self.items[1].gencode()
			if(len(block_map[new_bno]) == 0):
				op1 = self.items[0].items[0]
			else:
				convert_addr(block_map[new_bno])
				op1 = block_map[new_bno][len(block_map[new_bno]) - 1].dest
			op1type = 0
			opcode = self.op
			optype = 12
			op2 = new_bno 	
			op2type = 3
			
			new_bno1 = get_new_block()
			if(block_map.has_key(new_bno1)):
				print "Couldn't find a new block name.. "
				exit(0)

			block_map[new_bno1] = self.items[3].gencode()
			block_map[new_bno1] = block_map[new_bno1] + self.items[2].gencode()

			block_map[new_bno].append(IR.IR_STMT(opcode, new_bno1, 0, op1, op1type, op2, op2type, optype))

			block_map[new_bno1].append(IR.IR_STMT('loop_end',new_bno, 0,None,None,None,None,11))
			list_icode.append(IR.IR_STMT('forjmp',new_bno,0,None,None,None,None,9))
			return list_icode

		if(self.op == 'dowhile'):
			list_icode = []
			opcode = dest = op1 = op1type = op2 = op2type = optype = None
			
			new_bno = get_new_block()
			if(block_map.has_key(new_bno)):
				print "Couldn't find a new block name.. "
				exit(0)
			
			list_icode.append(IR.IR_STMT('jmp',new_bno,0,None,None,None,None,10))
			block_map[new_bno] = self.items[0].gencode()
			
			new_bno1 = get_new_block()
			if(block_map.has_key(new_bno1)):
				print "Couldn't find a new block name.. "
				exit(0)
			
			block_map[new_bno].append(IR.IR_STMT('whilejmp',new_bno1,0,None,None,None,None,9))
			block_map[new_bno1] = self.items[1].gencode()
			
			new_bno2 = get_new_block()
			if(block_map.has_key(new_bno2)):
				print "Couldn't find a new block name.. "
				exit(0)
			
			block_map[new_bno2] = self.items[0].gencode()
			block_map[new_bno2].append(IR.IR_STMT('loop_end',new_bno1,0,None,None,None,None,11))
			
			if(len(block_map[new_bno1]) == 0):
				op1 = self.items[0].items[0]
			else:
				convert_addr(block_map[new_bno1])
				op1 = block_map[new_bno1][len(block_map[new_bno1]) - 1].dest
			op1type = 0
			dest = new_bno2
			opcode = self.op
			optype = 12 
			block_map[new_bno1].append(IR.IR_STMT(opcode, dest, 0,op1, op1type, new_bno1, 3, optype))
			return list_icode

def p_pgm(p):
	'Pgm : DeclSeq StmtSeq'
	p[0] = NODE('Pgm',[p[1],p[2]])

def p_sseq_stmt_null(p):
	'StmtSeq : '
	p[0] = NODE('epsilon',[])

def p_sseq_stmt_sseq(p):
	'StmtSeq : Stmt StmtSeq'
	p[0] = NODE('stmtseq',[p[1],p[2]])

def p_stmt_se(p):
	'Stmt : SE SCOLON'
	p[0] = p[1]

def p_stmt_print(p):
	'Stmt : PRINT LOPEN AE ROPEN SCOLON'
	p[0] = NODE('print',[p[3]])

def p_stmt_blk(p):
	'Stmt : LCURL DeclSeq StmtSeq RCURL'
	p[0] = NODE('Pgm',[p[2],p[3]])

def p_stmt_ife(p):
	'Stmt : IF AE THEN Stmt ELSE Stmt'
	p[0] = NODE('ife', [p[2],p[4],p[6]])

def p_stmt_if(p):
	'Stmt : IF AE THEN Stmt'
	p[0] = NODE('if', [p[2],p[4]])

def p_stmt_while(p):
	'Stmt : WHILE AE DO Stmt'
	p[0] = NODE('while', [p[2],p[4]])

def p_stmt_dowhile(p):
	'Stmt : DO Stmt WHILE AE SCOLON'
	p[0] = NODE('dowhile', [p[2],p[4]])

def p_stmt_for(p):
	'Stmt : FOR LOPEN SEOpt SCOLON AEOpt SCOLON SEOpt ROPEN Stmt'
	p[0] = NODE('for', [p[3],p[5],p[7],p[9]])

def p_seopt_se(p):
	'SEOpt : SE'
	p[0] = p[1]

def p_seopt_null(p):
	'SEOpt : '
	p[0] = NODE('epsilon',[])

def p_aeopt_ae(p):
	'AEOpt : AE'
	p[0] = p[1]

def p_aeopt_null(p):
	'AEOpt : '
	p[0] = NODE('epsilon',[])

def p_declseq_decl(p):
	'DeclSeq : Decl DeclSeq'
	p[0] = NODE('declseq', [p[1],p[2]])

def p_declseq_null(p):
	'DeclSeq : '
	p[0] = NODE('epsilon', [])

def p_decl_type(p):
	'''Decl : INT Varlist SCOLON
		| BOOL Varlist SCOLON'''
	p[0] = NODE('Decl', [p[2]])

def p_varlist(p):
	'Varlist : Var COMA Varlist'
	p[0] = NODE('varlist',[p[1],p[3]])

def p_varlist_var(p):
	'Varlist : Var'
	p[0] = p[1]

def p_var(p):
	'Var : ID DimStar'
	p[0] = NODE('var', [NODE('id', [p[1]]),p[2]])
	
def p_se_assign(p):
	'SE : Lhs ASSIGN AE'
	p[0] = NODE('=', [p[1],p[3]])

def p_se_lhsp(p):
	'SE : Lhs INC'
	p[0] = NODE('postinc', [p[1]])
 
def p_se_lhsd(p):
	'SE : Lhs DEC'
	p[0] = NODE('postdec', [p[1]])

def p_se_lhsppre(p):
	'SE : INC Lhs'
	p[0] = NODE('preinc', [p[2]])
 
def p_se_lhsdpre(p):
	'SE : DEC Lhs'
	p[0] = NODE('predec', [p[2]])

def p_lhs_id(p):
	'Lhs : ID'
	p[0] = NODE('id', [p[1]])

def p_lhs_arr(p):
	'Lhs : Lhs LSQ AE RSQ'
	p[0] = NODE('array',[p[1],p[3]])
 	
def p_ae_bin(p):
	'''AE : AE ADD AE
	      | AE SUB AE
	      | AE MUL AE
	      | AE DIV AE
	      | AE MOD AE
	      | AE AND AE
	      | AE OR AE
	      | AE EQ AE
	      | AE NEQ AE
	      | AE LT AE
	      | AE GT AE
	      | AE GTEQ AE
	      | AE LTEQ AE'''
	p[0] = NODE(p[2], [p[1],p[3]])
	
def p_ae_un(p):
	'''AE : SUB AE %prec UMINUS
	      | NOT AE'''
	if(p[1] == '-'):
		p[0] = NODE('uminus', [p[2]])
	else:
		p[0] = NODE('not', [p[2]])
		
def p_ae_open(p):
	'AE : LOPEN AE ROPEN'
	p[0] = p[2]

def p_ae_int(p):
	'AE : INTCONST'
	p[0] = NODE('intconst',[p[1]])

def p_ae_id(p):
	'AE : Lhs'
	p[0] = p[1]

def p_ae_se(p):
	'AE : SE'
	p[0] = p[1]

def p_ae_input(p):
	'AE : INPUT LOPEN ROPEN'
	p[0] = NODE('input', []);

def p_ae_tf(p):
	'''AE : TRUE
	      | FALSE'''
	p[0] = NODE('bool',[p[1]])

def p_ae_new(p):
	'''AE : NEW INT DimExpr DimStar
	      | NEW BOOL DimExpr DimStar'''
	p[0] = 	NODE('new',[p[3],p[4]])

def p_dimexpr(p):
	'DimExpr : LSQ AE RSQ'
	p[0] = p[2]

def p_dimstar(p):
	'DimStar : LSQ RSQ DimStar'
	p[0] = NODE('dim',[p[3]]);

def p_dimstar_null(p):
	'DimStar : '
	p[0] = NODE('epsilon',[])

def p_error(p):
	if(p == None):
		print "Syntax error: Reached end of file"
	else:
		print "Syntax error at Line: ", p.lineno, "Before Token: ", p.value
	exit(0)


def print_sequence(blk_map,block_no,printed):
	for items in blk_map[block_no]:
		print block_no, "  ", items
		if(items.dest and not str(items.dest).isdigit() and str(items.dest)[:2] == '_b'):
			if(str(items.dest) not in printed):
				printed.append(str(items.dest))
				print_sequence(blk_map,str(items.dest),printed)
		if(items.src1 and not str(items.src1).isdigit() and str(items.src1)[:2] == '_b'):
			if(str(items.src1) not in printed):
				printed.append(str(items.src1))
				print_sequence(blk_map,str(items.src1), printed)
		if(items.operatortype != 9 and items.src2 and not str(items.src2).isdigit() and str(items.src2)[:2] == '_b'):
			if(str(items.src2) not in printed):
				printed.append(str(items.src2))
				print_sequence(blk_map,str(items.src2),printed)


def parse(s):
	parser = yacc.yacc()
	result = parser.parse(s)
#	print result.wellformed([],[],0)
#
	if(result.wellformed([],[],0)):
		new_block = get_new_block()
		if(block_map.has_key(new_block)):
			print "No new block label found.. "
			exit(0)
		block_map[new_block] = result.gencode()
		print_sequence(block_map, new_block,[])
		print "\n\n\n"
		return block_map, new_block
	else:
		print "Not a well-formed program."
		exit(0)

input ='''int a,b,c,d;
a = input();
b = a || !2;
c = a + b;
d = 2 -3 ;
print(d - 4 * a);'''
input0 = ''' int a,b;
            b = 4;
        for (a = 1; a < b; a++)
            b++;
        do {
              a++;
           } while(b > 3);
            '''
input1 = ''' int a,b;
            b = 4;
            if(b==5)
           then
          {
              if(b==5)
             then
            {
           int c;
              c=3;
                b=6;}
            
            }
        {
            int d;
            d=6;
        }
        //d=2;
        for (a = 1; a < b; a++)
            b++;
        //do {
        //      a++;
        //   } while(b > 3);
            '''
input2 = '''int a, b, c, s;
a = input();
if(3-4) then { 
if(a==10) then {
	  a = 5;}
	a = a + 5 * 2;}
b= 2+-4;
c= b+2;
s=3;
if (b < 3) then {
   // Comment 1
   c = a * 2;
}
else {
   c = a * 1;
}
while (c < 3) do
{
   b = b++ + 2;
   a = a-- + 3;
}'''
input3 = ''' int a,b;
int d,x;
int z[];
b = 4;
if(b==5)
then
{
if(b==5)
then
{
int c;
c=3;
b=6;}
}
for (a = 1; a < b; a++)
{
int x;
b++;
x=1;
x++;
}
do {
int e;
d = 1;
//a++;
e=1;
e++;
//l = 3;
} while(b > 3);
//x++;
//dfsdfs
d++;
z[2] = 3;
'''
#parse(input)
#parse(input0)
#parse(input2)
#parse(input3)
